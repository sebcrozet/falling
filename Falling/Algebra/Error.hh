#ifndef FALLING_ALGEBRA_ERROR
# define FALLING_ALGEBRA_ERROR

# include <limits>

namespace Falling
{
  namespace Algebra
  {
    template <typename N>
      struct Error
      {
        static constexpr N epsTol = 100.0 * std::numeric_limits<N>::min();
        static           N sqEpsRel;
        static constexpr N margin = 0.16;
      };

    template <typename N>
      N Error<N>::sqEpsRel = sqrt(Error<N>::epsTol);
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_ERROR
