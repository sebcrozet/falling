#ifndef FALLING_ALGEBRA_UNITSPHERESAMPLES
# define FALLING_ALGEBRA_UNITSPHERESAMPLES

namespace Falling
{
  namespace Algebra
  {
# define static_assert_UnitSphereSamples(TYPE, NUM)                                          \
         static_assert(Falling::Algebra::UnitSphereSamples<typename RMPAR(TYPE), NUM>::value \
                       , "Type parameter " #TYPE " must have " #NUM " samples on the unit sphere.")
    template <typename V, unsigned int N, typename Enabler = void>
      struct UnitSphereSamples
      {
        static const bool value = false;

        static const std::array<V, N> samples;
      };
  } // end Algebra
} // end Falling


#endif // FALLING_ALGEBRA_UNITSPHERESAMPLES
