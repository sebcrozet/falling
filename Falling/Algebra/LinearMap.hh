#ifndef FALLING_ALGEBRA_LINEARMAP
# define FALLING_ALGEBRA_LINEARMAP

# include <Falling/Algebra/Dimension.hh>
# include <Falling/Algebra/MultInversibleSemiGroup.hh>
# include <Falling/Algebra/Vect.hh>

namespace Falling
{
  namespace Algebra
  {
    template <unsigned int dim>
      class LinearMap
      {
        private:
          double _components[dim][dim];

        public:
          const double* begin() const
          { return &(_components[0][0]); }

          double* begin()
          { return &(_components[0][0]); }

          const double* end() const
          { return &(_components[dim][dim]); }

          double* end()
          { return &(_components[dim][dim]); }

          double at(unsigned i, unsigned j) const
          {
            assert(i < dim);
            assert(j < dim);

            return _components[i][j];
          }

          double& at(unsigned i, unsigned j)
          {
            assert(i < dim);
            assert(j < dim);

            return _components[i][j];
          }

          LinearMap()
          {
            for (unsigned i = 0; i < dim; ++i)
              for (unsigned j = 0; j < dim; ++j)
                at(i, j) = 0.0;
          }

          LinearMap& revertToIdentity()
          {
            *this = LinearMap();

            for (unsigned i = 0; i < dim; ++i)
              at(i, i) = 1.0;

            return *this;
          }
      };

    template <unsigned int d>
      struct DimensionTrait<LinearMap<d>>
      {
        static const bool         value = true;
        static const unsigned int dim   = d;
      };

    template <unsigned int dim>
      struct MultInversibleSemiGroupTrait<LinearMap<dim>>
      {
        static const bool value = true;

        static const LinearMap<dim> one;
        static LinearMap<dim> mult(const LinearMap<dim>& a, const LinearMap<dim>& b)
        {
          LinearMap<dim> res;

          for (unsigned i = 0; i < dim; ++i)
          {
            for (unsigned j = 0; j < dim; ++j)
            {
              double mult = 0.0;
              for (unsigned k = 0; k < dim; ++k)
                mult += a.at(i, k) * b.at(k, j);

              res.at(i, j) = mult;
            }
          }

          return res;
        }

        static LinearMap<dim>& invert(LinearMap<dim>& a)
        {
          LinearMap<dim> result;

          result.revertToIdentity();

          // inversion using Gauss-Jordan elimination
          for (unsigned k = 0; k < dim; ++k)
          {
            // search a non-zero value on the k-th column
            // FIXME: is it worth it to search for the max instead?
            unsigned i = k;
            for (; i < dim; ++i)
            {
              if (a.at(i, k) != 0.0)
                break;
            }

            assert(i != dim && "The input matrix must be invertible.");
            // swap pivot line
            for (unsigned j = 0; j < dim; ++j)
            {
              std::swap(a.at(i, j), a.at(k, j));
              std::swap(result.at(i, j), result.at(k, j));
            }

            double pivot = a.at(k, k);

            for (unsigned j = k; j < dim; ++j)
            {
              a.at(k, j)      /= pivot;
              result.at(k, j) /= pivot;
            }

            for (unsigned l = 0; l < dim; ++l)
            {
              if (l != k)
              {
                double normalizer = a.at(l, k) / pivot;

                for (unsigned j = k; j < dim; ++j)
                {
                  a.at(l, j)      -= a.at(k, j) * normalizer;
                  result.at(l, j) -= result.at(k, j) * normalizer;
                }
              }
            }
          }

          a = result;

          return a;
        }

        static LinearMap<dim> inverse(const LinearMap<dim>& a)
        {
          LinearMap<dim> res = a;

          return invert(res);
        }
      };

    template <unsigned int dim>
    const LinearMap<dim> MultInversibleSemiGroupTrait<LinearMap<dim>>
          ::one = LinearMap<dim>().revertToIdentity();

    template <unsigned int dim>
      struct TransformTrait<LinearMap<dim>, Vect<dim>>
      {
        static const bool value = true;

        static Vect<dim> transform(const LinearMap<dim>& t, const Vect<dim>& v)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
          {
            double currComp = 0.0;

            for (unsigned j = 0; j < dim; ++j)
              currComp += t.at(i, j) * v.at(j);

            res[i] = currComp;
          }

          return res;
        }
      };

    template <unsigned int dim>
      struct Eq<LinearMap<dim>>
      {
        static const bool value = true;

        static bool equal(const LinearMap<dim>& a, const LinearMap<dim>& b)
        {
          for (unsigned i = 0; i < dim; ++i)
          {
            for (unsigned j = 0; j < dim; ++j)
            {
              if (a.at(i, j) != b.at(i, j))
                return false;
            }
          }

          return true;
        }
      };

    template <unsigned int dim>
      std::ostream& operator<<(std::ostream& out, const LinearMap<dim>& mat)
      {
        out << "LinearMap" << dim << " {";

        for (unsigned i = 0; i < dim; ++i)
        {
          out << " { ";

          for (unsigned j = 0; j < dim; ++j)
            out << mat.at(i, j) << " ";

          out << "}";
        }

        out << " }";

        return out;
      }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_LINEARMAP
