#ifndef FALLING_ALGEBRA_TRANSFORM
# define FALLING_ALGEBRA_TRANSFORM

# include <cmath>
# include <Falling/Algebra/Translatable.hh>
# include <Falling/Algebra/DeltaTransform.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_Transform(TYPEP, TYPEV)            \
         static_assert(Falling::Algebra::TransformTrait<  \
             typename RMPAR(TYPEP)                        \
             , typename RMPAR(TYPEV)                      \
             >::value                                     \
             , "Type parameter " #TYPEP " must transform objects of type " #TYPEV ".")
    // FIXME: how to force the TranslatableTrait and DeltaTransformTrait?
    template <typename P, typename V, typename Enabler = void>
      struct TransformTrait
      {
        static const bool value = false;

        static V transform(const P&, const V&)
        { static_assert_Transform((P), (V)); }
      };

    template <typename P, typename V, BIND_CONCEPT(TT, (TransformTrait<P, V>))>
      V transform(const P& p, const V& v)
      { return TT::transform(p, v); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_TRANSFORM
