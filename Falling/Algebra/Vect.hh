#ifndef FALLING_ALGEBRA_VECT
# define FALLING_ALGEBRA_VECT

# include <random>
# include <cassert>
# include <iostream>
# include <array>
# include <initializer_list>
# include <Falling/Algebra/Eq.hh>
# include <Falling/Algebra/Transform.hh>
# include <Falling/Algebra/ScalarOp.hh>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/OrthonormalBasis.hh>
# include <Falling/Algebra/UnitSphereSamples.hh>

namespace Falling
{
  namespace Algebra
  {
    template <unsigned int dim>
      struct Vect
      {
        // NOTE: this is not a vector in homogeneous coordinates
        double components[dim]; // FIXME: make this private (to ensure bounds checking)?

        double operator[](unsigned i) const
        { return at(i); }

        double& operator[](unsigned i)
        { return at(i); }

        double at(unsigned i) const
        {
          assert(i < dim);

          return components[i];
        }

        double& at(unsigned i)
        {
          assert(i < dim);

          return components[i];
        }

        double* begin()
        { return &components[0]; }

        const double* begin() const
        { return &components[0]; }

        double* end()
        { return &components[dim]; }

        const double* end() const
        { return &components[dim]; }

        Vect()
        {
          for (unsigned i = 0; i < dim; ++i)
            components[i] = 0.0;
        }

        template<typename... L>
          explicit Vect(L... l)
          {
            static_assert(sizeof...(L) == dim
                          , "Number of initializer components not matching the vector dimension.");
            setTo(l...);
          }

        template<typename... L>
          void setTo(L... l)
          {
            static_assert(sizeof...(L) == dim
                          , "Number of initializer components not matching the vector dimension.");
            _setTo<0, L...>(l...);
          }

        private:
        template<unsigned curr, typename T>
          void _setTo(T e)
          {
            static_assert(std::is_convertible<T, double>::value
                          , "Components values must be convertible to double.");
            static_assert(curr == dim - 1,
                          "Internal problem: the caller forgot to check the dimension.");

            components[curr] = e;
          }

        template<unsigned curr, typename T, typename... L>
          void _setTo(T e, L... es)
          {
            static_assert(std::is_convertible<T, double>::value,
                          "Components values must be convertible to double.");

            components[curr] = e;
            _setTo<curr + 1, L...>(es...);
          }
      };

    template <unsigned int d>
      struct DimensionTrait<Vect<d>>
      {
        static const bool value = true;

        static const unsigned int dim = d;
      };

    template <unsigned int dim>
      struct ScalarOpTrait<Vect<dim>>
      {
        static const bool value = true;

        static Vect<dim>& multTo(double a, Vect<dim>& b)
        {
          for (unsigned i = 0; i < dim; ++i)
            b.components[i] *= a;

          return b;
        }

        static Vect<dim> mult(double a, const Vect<dim>& b)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
            res.components[i] = a * b.components[i];

          return res;
        }

        static Vect<dim>& addTo(double a, Vect<dim>& b)
        {
          for (unsigned i = 0; i < dim; ++i)
            b.components[i] += a;

          return b;
        }

        static Vect<dim> add(double a, const Vect<dim>& b)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
            res.components[i] = a + b.components[i];

          return res;
        }
      };

    template <unsigned int dim>
      struct DotProdTrait<Vect<dim>>
      {
        static const bool value = true;
        static double dot(const Vect<dim>& a, const Vect<dim>& b)
        {
          double res = 0.0;

          for (unsigned i = 0; i < dim; ++i)
            res += a.components[i] * b.components[i];

          return res;
        }

        static double sqlen(const Vect<dim>& v)
        { return dot(v, v); }

        static double len(const Vect<dim>& v)
        { return sqrt(sqlen(v)); }
      };

    template <unsigned int dim>
      struct  AbelianGroupTrait<Vect<dim>>
      {
        static const bool value = true;

        static const Vect<dim> zero;

        static Vect<dim>& addTo(Vect<dim>& a, const Vect<dim>& b)
        {
          for (unsigned i = 0; i < dim; ++i)
            a.components[i] += b.components[i];

          return a;
        }

        static Vect<dim>& subTo(Vect<dim>& a, const Vect<dim>& b)
        {
          for (unsigned i = 0; i < dim; ++i)
            a.components[i] -= b.components[i];

          return a;
        }

        static Vect<dim>& negate(Vect<dim>& a)
        {
          for (unsigned i = 0; i < dim; ++i)
            a.components[i] = -a.components[i];

          return a;
        }

        static Vect<dim> add(const Vect<dim>& a, const Vect<dim>& b)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
            res.components[i] = a.components[i] + b.components[i];

          return res;
        }

        static Vect<dim> sub(const Vect<dim>& a, const Vect<dim>& b)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
            res.components[i] = a.components[i] - b.components[i];

          return res;
        }

        static Vect<dim> neg(const Vect<dim>& a)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
            res.components[i] = -a.components[i];

          return res;
        }
      };

    template <unsigned int dim>
      const Vect<dim> AbelianGroupTrait<Vect<dim>>::zero = Vect<dim>();

    template <unsigned int dim>
      struct TranslatableTrait<Vect<dim>>
      {
        static const bool value = true;

        using TranslationType = Vect<dim>;

        static TranslationType translation(const Vect<dim>& v)
        { return v; }

        static Vect<dim>& translate(const TranslationType& t, Vect<dim>& v)
        {
          return addTo(t, v);
        }
      };

    template <unsigned int dim>
      struct Eq<Vect<dim>>
      {
        static const bool value = true;

        static bool equal(const Vect<dim>& a, const Vect<dim>& b)
        {
          for (unsigned i = 0; i < dim; ++i)
          {
            if (a[i] != b[i])
              return false;
          }

          return true;
        }
      };

    template <unsigned int dim>
      struct OrthonormalBasis<Vect<dim>>
      {
        private:
        static const std::array<Vect<dim>, dim> _generate()
        {
          std::array<Vect<dim>, dim> res;

          for (unsigned int i = 0; i < dim; ++i)
          {
            res[i]    = AbelianGroupTrait<Vect<dim>>::zero;
            res[i][i] = 1;
          }
          return res;
        }

        static bool                       _initialized;
        static std::array<Vect<dim>, dim> _basis;

        public:
        static const bool value = true;

        static const std::array<Vect<dim>, dim>& basis()
        {
          if (!_initialized)
          {
            _initialized = true;
            _basis       = _generate();
          }

          return _basis;
        }
      };

    template <unsigned int dim>
      std::array<Vect<dim>, dim>
      OrthonormalBasis<Vect<dim>>::_basis;

    template <unsigned int dim>
      bool OrthonormalBasis<Vect<dim>>::_initialized = false;

    template <unsigned int dim, unsigned int numSamples>
      struct UnitSphereSamples<Vect<dim>, numSamples>
      {
        private:
          static const std::array<Vect<dim>, numSamples> _generate()
          {
            // FIXME: do we need to set a seed?
            std::default_random_engine        generator;
            std::normal_distribution<double>  distribution;
            std::array<Vect<dim>, numSamples> res;

            for (unsigned int i = 0; i < numSamples; ++i)
            {
              for (unsigned int j = 0; j < dim; ++j)
                res[i][j] = distribution(generator);

              normalize(res[i]);
            }

            return res;
          }

          static bool                              _initialized;
          static std::array<Vect<dim>, numSamples> _samples;

        public:
          static const bool value = true;

          static const std::array<Vect<dim>, numSamples>& samples()
          {
            if (!_initialized)
            {
              _initialized = true;
              _samples = _generate();
            }

            return _samples;
          }
      };

    template <unsigned int dim, unsigned int numSamples>
      std::array<Vect<dim>, numSamples>
      UnitSphereSamples<Vect<dim>, numSamples>::_samples;

    template <unsigned int dim, unsigned int numSamples>
      bool UnitSphereSamples<Vect<dim>, numSamples>::_initialized = false;

    template <unsigned int dim>
      std::ostream& operator<<(std::ostream& out, const Falling::Algebra::Vect<dim>& v)
      {
        out << "Vect" << dim;

        for (unsigned int i = 0; i < dim; ++i)
          out << " " << v.components[i];

        return out;
      }
  } // end Algebra
} // end Falling

# endif // FALLING_ALGEBRA_VECT
