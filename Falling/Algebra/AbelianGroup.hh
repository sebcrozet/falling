#ifndef FALLING_ALGEBRA_ABELIANGROUP
# define FALLING_ALGEBRA_ABELIANGROUP

# include <type_traits>
# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_AbelianGroup(TYPE)                                               \
         static_assert(Falling::Algebra::AbelianGroupTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be an element of an abelian group.")
    template <class V, typename Enabler = void>
      struct AbelianGroupTrait
      {
        static const bool value = false;

        static const V zero;
        static V& addTo(V&, const V&)
        { static_assert_AbelianGroup((V)); }

        static V& subTo(V&, const V&)
        { static_assert_AbelianGroup((V)); }

        static V& negate(V&)
        { static_assert_AbelianGroup((V)); }

        static V add(const V&, const V&)
        { static_assert_AbelianGroup((V)); }

        static V sub(const V&, const V&)
        { static_assert_AbelianGroup((V)); }

        static V neg(const V&)
        { static_assert_AbelianGroup((V)); }
      };

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V& addTo(V& a, const V& b)
      {
        return AGT::addTo(a, b);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V& subTo(V& a, const V& b)
      {
        return AGT::subTo(a, b);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V& negate(V& v)
      {
        return AGT::negate(v);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V add(const V& a, const V& b)
      {
        return AGT::add(a, b);
      }

    template <class V, CHECK_CONCEPT((AbelianGroupTrait<V>))>
      V operator+(const V& a, const V& b)
      {
        return add(a, b);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V& operator+=(V& a, const V& b)
      {
        return AGT::addTo(a, b);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V sub(const V& a, const V& b)
      {
        return AGT::sub(a, b);
      }

    template <class V, CHECK_CONCEPT((AbelianGroupTrait<V>))>
      V operator-(const V& a, const V& b)
      {
        return sub(a, b);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V& operator-=(V& a, const V& b)
      {
        return AGT::subTo(a, b);
      }

    template <class V, BIND_CONCEPT(AGT, (AbelianGroupTrait<V>))>
      V neg(const V& v)
      {
        return AGT::neg(v);
      }

    template <class V, CHECK_CONCEPT((AbelianGroupTrait<V>))>
      V operator-(const V& v)
      {
        return neg(v);
      }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_ABELIANGROUP
