#ifndef FALLING_ALGEBRA_DELTA_TRANSFORM
# define FALLING_ALGEBRA_DELTA_TRANSFORM

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_DeltaTransform(TYPET, TYPEV)                                       \
         static_assert(Falling::Algebra::DeltaTransformTrait<                             \
              typename RMPAR(TYPET), typename RMPAR(TYPEV)>::value                        \
              , "Transform type parameter " #TYPET " and vector type parameter " # TYPEV " must be able to make delta-transformations.")
    template <class T, class V, typename Enabler = void>
      struct DeltaTransformTrait
      {
        static const bool value = false;

        static V deltaTransform(const T&, const V&)
        { static_assert_DeltaTransform((T), (V)); }

        static V deltaTransformTranspose(const T&, const V&)
        { static_assert_DeltaTransform((T), (V)); }
      };

    template <class T, class V, BIND_CONCEPT(DTT, (DeltaTransformTrait<T, V>))>
      V deltaTransform(const T& p, const V& v)
      { return DTT::deltaTransform(p, v); }

    template <class T, class V, BIND_CONCEPT(DTT, (DeltaTransformTrait<T, V>))>
      V deltaTransformTranspose(const T& p, const V& v)
      { return DTT::deltaTransformTranspose(p, v); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_DELTA_TRANSFORM
