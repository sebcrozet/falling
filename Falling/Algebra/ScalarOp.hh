#ifndef FALLING_ALGEBRA_SCALAROP
# define FALLING_ALGEBRA_SCALAROP

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_ScalarOp(TYPE)                                               \
         static_assert(Falling::Algebra::ScalarOpTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have common arithmetic operations with a scalar.")
    template <class V, typename Enabler = void>
      struct ScalarOpTrait
      {
        static const bool value = false;

        static V& multTo(double, V&)
        { static_assert_ScalarOp((V)); }

        static V  mult(double, const V&)
        { static_assert_ScalarOp((V)); }

        static V& addTo(double, V&)
        { static_assert_ScalarOp((V)); }

        static V  add(double, const V&)
        { static_assert_ScalarOp((V)); }

      };

    template <typename V, BIND_CONCEPT(SMT, (ScalarOpTrait<V>))>
      static V& multTo(double a, V& b)
      { return SMT::multTo(a, b); }

    template <typename V, BIND_CONCEPT(SMT, (ScalarOpTrait<V>))>
      static V mult(double a, const V& b)
      { return SMT::mult(a, b); }

    template <typename V, BIND_CONCEPT(SMT, (ScalarOpTrait<V>))>
      static V& addTo(double a, V& b)
      { return SMT::addTo(a, b); }

    template <typename V, BIND_CONCEPT(SMT, (ScalarOpTrait<V>))>
      static V add(double a, const V& b)
      { return SMT::add(a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static V operator*(double a, const V& b)
      { return mult(a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static V operator*(const V& b, double a)
      { return mult(a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static void operator*=(V& b, double a)
      { multTo(a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static V operator/(const V& b, double a)
      { return mult(1.0 / a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static void operator/=(V& b, double a)
      { multTo(1.0 / a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static V operator+(const V& b, double a)
      { return add(a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static void operator+=(V& b, double a)
      { addTo(a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static V operator-(const V& b, double a)
      { return add(-a, b); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      static void operator-=(V& b, double a)
      { addTo(-a, b); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_SCALAROP
