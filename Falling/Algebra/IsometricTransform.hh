#ifndef FALLING_ALGEBRA_ISOMETRICTRANSFORM
# define FALLING_ALGEBRA_ISOMETRICTRANSFORM

# include <cassert>

# include <Falling/Algebra/Eq.hh>
# include <Falling/Algebra/TransformSystem.hh>
# include <Falling/Algebra/Vect.hh>
# include <Falling/Algebra/RotationMatrix.hh>
# include <Falling/Algebra/Dimension.hh>

namespace Falling
{
  namespace Algebra
  {
    // this is a verry naive implementation.
    // for matrix on higher dimensions, using
    // some specialized libraries could be more efficient
    template <unsigned int dim>
      class IsometricTransform
      {
        private:
          RotationMatrix<dim> _rotation;
          Vect<dim>           _translation;

        public:
          IsometricTransform()
          { }

          IsometricTransform(const RotationMatrix<dim>& rot,  const Vect<dim>& t)
            : _rotation(rot), _translation(t)
          { }

          const RotationMatrix<dim>& rotation() const
          { return _rotation; }

          RotationMatrix<dim>& rotation()
          { return _rotation; }

          const Vect<dim>& translation() const
          { return _translation; }

          Vect<dim>& translation()
          { return _translation; }

          IsometricTransform& revertToIdentity()
          {
            _rotation.revertToIdentity();

            for (unsigned i = 0; i < dim; ++i)
              _translation[i] = 0.0;

            return *this;
          }

      };

    template <unsigned int d>
      struct DimensionTrait<IsometricTransform<d>>
      {
        static const bool         value = true;
        static const unsigned int dim   = d;
      };

    template <unsigned int dim>
      struct MultInversibleSemiGroupTrait<IsometricTransform<dim>>
      {
        static const bool value = true;

        static const IsometricTransform<dim> one;
        static IsometricTransform<dim> mult(const IsometricTransform<dim>& a
                                            , const IsometricTransform<dim>& b)
        {
          return IsometricTransform<dim>(
                  MultInversibleSemiGroupTrait<RotationMatrix<dim>>
                  ::mult(a.rotation() , b.rotation())
                  , a.translation() + transform(a.rotation(), b.translation())
                  );
        }

        static IsometricTransform<dim>& invert(IsometricTransform<dim>& a)
        {
          MultInversibleSemiGroupTrait<RotationMatrix<dim>>::invert(a.rotation());
          a.translation() = transform(a.rotation(), a.translation());

          return a;
        }

        static IsometricTransform<dim> inverse(const IsometricTransform<dim>& a)
        {
          IsometricTransform<dim> res = a;

          return invert(res);
        }
      };

    template <unsigned int dim>
    const IsometricTransform<dim> MultInversibleSemiGroupTrait<IsometricTransform<dim>>::one =
                                IsometricTransform<dim>().revertToIdentity();

    template <unsigned int dim>
      struct TranslatableTrait<IsometricTransform<dim>>
      {
        static const bool value = true;

        using TranslationType = Vect<dim>;

        static Vect<dim> translation(const IsometricTransform<dim>& mat)
        { return mat.translation(); }

        static IsometricTransform<dim>& translate(
            const Vect<dim>&           v
            , IsometricTransform<dim>& mat)
        {
          mat.translation() += v;

          return mat;
        }
      };

    template <unsigned int dim>
      struct DeltaTransformTrait<IsometricTransform<dim>, Vect<dim>>
      {
        static const bool value = true;

        static Vect<dim> deltaTransform(const IsometricTransform<dim>& t
                                        , const Vect<dim>&             v)
        {
          return DeltaTransformTrait<RotationMatrix<dim>, Vect<dim>>
                 ::deltaTransform(t.rotation(), v);
        }

        static Vect<dim> deltaTransformTranspose(const IsometricTransform<dim>& t
                                                 , const Vect<dim>&             v)
        {
          return DeltaTransformTrait<RotationMatrix<dim>, Vect<dim>>
                 ::deltaTransformTranspose(t.rotation(), v);
        }
      };


    template <unsigned int dim>
      struct TransformTrait<IsometricTransform<dim>, Vect<dim>>
      {
        static const bool value = true;

        static Vect<dim> transform(const IsometricTransform<dim>& t
                                   , const Vect<dim>&             v)
        {
          return TransformTrait<RotationMatrix<dim>, Vect<dim>>::transform(t.rotation(), v)
                 + t.translation();
        }
      };

    template <unsigned int dim>
      struct Eq<IsometricTransform<dim>>
      {
        static const bool value = true;

        static bool equal(const IsometricTransform<dim>& a, const IsometricTransform<dim>& b)
        { return a.translation() == b.translation() && a.rotation() == b.rotation(); }
      };

    template <unsigned int dim>
      std::ostream& operator<<(std::ostream& out, const IsometricTransform<dim>& mat)
      {
        out << "Mat" << dim     << " { "
            << "Rotation ="     << mat.rotation()
            << "Translation = " << mat.translation()
            << "}";

        return out;
      }
  } // end Algebra
} // end Falling
#endif // FALLING_ALGEBRA_ISOMETRICTRANSFORM
