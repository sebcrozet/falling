#ifndef FALLING_ALGEBRA_ORTHONORMALBASIS
# define FALLING_ALGEBRA_ORTHONORMALBASIS

namespace Falling
{
  namespace Algebra
  {
    template <typename V, typename Enabler = void>
      struct OrthonormalBasis
      {
        static const bool value = false;

        static const std::array<Metal::FIXME, 0> basis[];
        // FIXME: and add something else to complete a basis from a vector
      };
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_ORTHONORMALBASIS
