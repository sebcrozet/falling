#ifndef FALLING_ALGEBRA_DIMENSION
# define FALLING_ALGEBRA_DIMENSION

namespace Falling
{
  namespace Algebra
  {
# define static_assert_Dimension(TYPE)                                               \
         static_assert(Falling::Algebra::DimensionTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have a dimension.")
    template <typename V, typename Enabler = void>
      struct DimensionTrait
      {
        static const bool value = false;

        static const unsigned int dim = 0;
      };
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_DIMENSION
