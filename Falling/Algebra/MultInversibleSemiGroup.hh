#ifndef FALLING_ALGEBRA_MULTSEMIGROUP
# define FALLING_ALGEBRA_MULTSEMIGROUP

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_MultInversibleSemiGroup(TYPE)                                               \
         static_assert(Falling::Algebra::MultInversibleSemiGroupTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be an element of an multiplicative inversible semi group.")
    template <class M, typename Enabler = void>
      struct MultInversibleSemiGroupTrait
      {
        static const bool value = false;

        static const M one;

        static M  mult(const M&, const M&)
        { static_assert_MultInversibleSemiGroup((M)); }

        static M  inverse(const M&)
        { static_assert_MultInversibleSemiGroup((M)); }

        static M& invert(M&)
        { static_assert_MultInversibleSemiGroup((M)); }

      };

    template <class M, BIND_CONCEPT(MST, (MultInversibleSemiGroupTrait<M>))>
      M mult(const M& a, const M& b)
      { return MST::mult(a, b); }

    template <class M, CHECK_CONCEPT((MultInversibleSemiGroupTrait<M>))>
      M operator*(const M& a, const M& b)
      { return mult(a, b); }

    template <class M, CHECK_CONCEPT((MultInversibleSemiGroupTrait<M>))>
      M operator/(const M& a, const M& b)
      { return mult(a, invert(b)); }

    template <class M, BIND_CONCEPT(MST, (MultInversibleSemiGroupTrait<M>))>
      M inverse(const M& a)
      { return MST::inverse(a); }

    template <class M, BIND_CONCEPT(MST, (MultInversibleSemiGroupTrait<M>))>
      M& invert(M& a)
      { return MST::invert(a); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_MULTSEMIGROUP
