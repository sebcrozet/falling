#ifndef FALLING_ALGEBRA_ROTATABLE
# define FALLING_ALGEBRA_ROTATABLE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/Fixme.hh>

# include <Falling/Algebra/Translatable.hh>
# include <Falling/Algebra/AbelianGroup.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_Rotatable(TYPE)                                               \
         static_assert(Falling::Algebra::RotatableTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be rotatable and have a rotation.")
    template <typename P, typename Enabler = void>
      struct RotatableTrait
      {
        static const bool value = false;

        using RotationType = Metal::FIXME;

        static RotationType rotation(const P&)
        { static_assert_Rotatable((P)); }

        static P&           rotate(const RotationType&, P&)
        { static_assert_Rotatable((P)); }

      };

    template <typename P, BIND_CONCEPT(R, (RotatableTrait<P>))>
      typename R::RotationType rotation(const P& p)
      { return R::rotation(p); }

    template <typename P, BIND_CONCEPT(R, (RotatableTrait<P>))>
      P& rotate(const typename R::RotationType& r, P& p)
      { return R::rotate(r, p); }

    template <
       typename P
       , BIND_CONCEPT(T, (TranslatableTrait<P>))
       , BIND_CONCEPT(R, (RotatableTrait<P>))
       , CHECK_CONCEPT((AbelianGroupTrait<typename T::TranslationType>))
       >
        P& rotateWrtPoint(const typename R::RotationType&    rotation,
                          const typename T::TranslationType& point,
                          P&                                 transform)
        { return translate(point, rotate(rotation, translate(-point, transform))); }

    template <
       typename P
       , BIND_CONCEPT(T, (TranslatableTrait<P>))
       , BIND_CONCEPT(R, (RotatableTrait<P>))
       , CHECK_CONCEPT((AbelianGroupTrait<typename T::TranslationType>))
       >
        P& rotateWrtCenter(const typename R::RotationType&    rotation,
                           P&                                 transform)
        { return rotateWrtPoint(rotation, translation(transform), transform); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_ROTATABLE
