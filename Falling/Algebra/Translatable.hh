#ifndef FALLING_ALGEBRA_TRANSLATABLE
# define FALLING_ALGEBRA_TRANSLATABLE

# include <type_traits>
# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/Fixme.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_Translatable(TYPE)                                               \
         static_assert(Falling::Algebra::TranslatableTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be translatable and have a translation.")
    template <typename P, typename Enabler = void>
      struct TranslatableTrait
      {
        static const bool value = false;

        using TranslationType = Metal::FIXME;

        static TranslationType translation(const P&)
        { static_assert_Translatable((P)); }

        static P&              translate(const TranslationType&, P&)
        { static_assert_Translatable((P)); }

      };

    template <typename P, BIND_CONCEPT(TT, (TranslatableTrait<P>))>
      typename TT::TranslationType translation(const P& p)
      { return TT::translation(p); }

    template <typename P, BIND_CONCEPT(TT, (TranslatableTrait<P>))>
      P& translate(const typename TT::TranslationType& r, P& p)
      { return TT::translate(r, p); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_TRANSLATABLE
