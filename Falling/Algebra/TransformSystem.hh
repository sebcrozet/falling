#ifndef FALLING_ALGEBRA_TRANSFORMSYSTEM
# define FALLING_ALGEBRA_TRANSFORMSYSTEM

# include <Falling/Algebra/Transform.hh>
# include <Falling/Algebra/MultInversibleSemiGroup.hh>
# include <Falling/Algebra/Rotatable.hh>
# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/CrossProd.hh>
# include <Falling/Algebra/ScalarOp.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_TransformSystem(TYPE)                                               \
         static_assert(Falling::Algebra::TransformSystemTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must form a transform system.")
    template <class P, typename Enabler = void>
      struct TransformSystemTrait
      {
        using T = typename TranslatableTrait<P>::TranslationType;
        using R = typename RotatableTrait<P>::RotationType;

        using AsTranslatable            = TranslatableTrait<P>;
        using AsAbelianV                = AbelianGroupTrait<T>;
        using AsAbelianA                = AbelianGroupTrait<R>;
        using AsScalarOpV               = ScalarOpTrait<T>;
        using AsScalarOpA               = ScalarOpTrait<R>;
        using AsMultInversibleSemiGroup = MultInversibleSemiGroupTrait<P>;
        using AsTransform               = TransformTrait<P, T>;
        using AsRotatable               = RotatableTrait<P>;
        using AsDotProd                 = DotProdTrait<T>;
        using AsCrossProd               = CrossProdTrait<T>;

        static const bool value = AsAbelianV::value
                                  && AsAbelianA::value
                                  && AsScalarOpV::value
                                  && AsScalarOpA::value
                                  && AsMultInversibleSemiGroup::value
                                  && AsTransform::value
                                  && AsRotatable::value
                                  && AsTranslatable::value
                                  && AsDotProd::value
                                  && AsCrossProd::value
                                  && std::is_same<R, typename AsCrossProd::ResultType>::value;
      };
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_TRANSFORMSYSTEM
