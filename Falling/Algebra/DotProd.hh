#ifndef FALLING_ALGEBRA_DOTPROD
# define FALLING_ALGEBRA_DOTPROD

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Algebra/ScalarOp.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_DotProd(TYPE)                                               \
         static_assert(Falling::Algebra::DotProdTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have a dot product.")
    template <typename V, typename Enabler = void>
      struct DotProdTrait
      {
        static const bool value = false;

        static double dot(const V&, const V&)
        { static_assert_DotProd((V)); }

        static double sqlen(const V& a)
        { static_assert_DotProd((V)); }

        static double len(const V& a)
        { static_assert_DotProd((V)); }
      };

    template <typename V, BIND_CONCEPT(DPT, (DotProdTrait<V>))>
      double dot(const V& a, const V&b)
      { return DPT::dot(a, b); }

    template <typename V, BIND_CONCEPT(DPT, (DotProdTrait<V>))>
      double sqlen(const V& v)
      { return DPT::sqlen(v); }

    template <typename V, BIND_CONCEPT(DPT, (DotProdTrait<V>))>
      double len(const V& v)
      { return DPT::len(v); }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      double normalize(V& v)
      {
        auto lenv = len(v);

        assert(lenv != 0.0);

        v /= lenv;

        return lenv;
      }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      V normalized(const V& v)
      {
        auto lenv = len(v);

        assert(lenv != 0.0);

        return v / lenv;
      }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_DOTPROD
