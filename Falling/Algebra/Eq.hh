#ifndef FALLING_ALGEBRA_EQ
# define FALLING_ALGEBRA_EQ

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_Eq(TYPE)                                          \
         static_assert(Falling::Algebra::Eq<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have an equality.")
    template <typename T, typename Enabler = void>
      struct Eq
      {
        static const bool value = false;

        static const bool equal(const T& a, const T& b)
        { static_assert_Eq((T)); }
      };

    template <typename T, BIND_CONCEPT(E, (Eq<T>))>
      bool equal(const T& a, const T& b)
      { return E::equal(a, b); }

    template <typename T, BIND_CONCEPT(E, (Eq<T>))>
      bool operator==(const T& a, const T& b)
      { return E::equal(a, b); }

    template <typename T>
      bool operator!=(const T& a, const T& b)
      { return !(a == b); }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_EQ
