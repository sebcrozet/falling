#ifndef FALLING_ALGEBRA_ROTATIONMATRIX
# define FALLING_ALGEBRA_ROTATIONMATRIX

# include <cassert>

# include <Falling/Algebra/Eq.hh>
# include <Falling/Algebra/TransformSystem.hh>
# include <Falling/Algebra/Vect.hh>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/Algebra/LinearMap.hh>

namespace Falling
{
  namespace Algebra
  {
    // this is a verry naive implementation.
    // for matrix on higher dimensions, using
    // some specialized libraries could be more efficient
    template <unsigned int dim>
      class RotationMatrix
      {
        private:
          LinearMap<dim> _matrix;

        public:
          RotationMatrix()
          { }

          explicit RotationMatrix(const LinearMap<dim>& mapping)
            : _matrix(mapping)
          { }

          const LinearMap<dim>& subMatrix() const
          { return _matrix; }

          LinearMap<dim>& subMatrix()
          { return _matrix; }

          const double* rotation() const
          { return _matrix.begin(); }

          double* rotation()
          { return _matrix.begin(); }

          double at(unsigned i, unsigned j) const
          { return _matrix.at(i, j); }

          double& at(unsigned i, unsigned j)
          { return _matrix.at(i, j); }

          RotationMatrix& revertToIdentity()
          {
            _matrix.revertToIdentity();

            return *this;
          }
      };

    template <unsigned int d>
      struct DimensionTrait<RotationMatrix<d>>
      {
        static const bool         value = true;
        static const unsigned int dim   = d;
      };

    template <unsigned int dim>
      struct MultInversibleSemiGroupTrait<RotationMatrix<dim>>
      {
        static const bool value = true;

        static const RotationMatrix<dim> one;
        static RotationMatrix<dim> mult(const RotationMatrix<dim>&   a
                                        , const RotationMatrix<dim>& b)
        {
          return RotationMatrix<dim>(
                   MultInversibleSemiGroupTrait<LinearMap<dim>>
                   ::mult(a.subMatrix(), b.subMatrix()));
        }

        static RotationMatrix<dim>& invert(RotationMatrix<dim>& a)
        {
          // transpose
          for (unsigned i = 0; i < dim; ++i)
            for (unsigned j = i; j < dim; ++j)
              std::swap(a.at(i, j), a.at(j, i));

          return a;
        }

        static RotationMatrix<dim> inverse(const RotationMatrix<dim>& a)
        {
          RotationMatrix<dim> res = a;

          return invert(res);
        }
      };

    template <unsigned int dim>
    const RotationMatrix<dim> MultInversibleSemiGroupTrait<RotationMatrix<dim>>
          ::one = RotationMatrix<dim>().revertToIdentity();

    template <unsigned int dim>
      struct DeltaTransformTrait<RotationMatrix<dim>, Vect<dim>>
      {
        static const bool value = true;

        static Vect<dim> deltaTransform(const RotationMatrix<dim>& t
                                        , const Vect<dim>&         v)
        { return TransformTrait<LinearMap<dim>, Vect<dim>>::transform(t.subMatrix(), v); }

        static Vect<dim> deltaTransformTranspose(const RotationMatrix<dim>& t
                                                 , const Vect<dim>&         v)
        {
          Vect<dim> res;

          for (unsigned i = 0; i < dim; ++i)
          {
            double currComp = 0.0;

            for (unsigned j = 0; j < dim; ++j)
              currComp += t.at(j, i) * v.components[j];

            res.components[i] = currComp;
          }

          return res;
        }
      };


    template <unsigned int dim>
      struct TransformTrait<RotationMatrix<dim>, Vect<dim>>
      {
        static const bool value = true;

        static Vect<dim> transform(const RotationMatrix<dim>& t
                                   , const Vect<dim>&         v)
        { return TransformTrait<LinearMap<dim>, Vect<dim>>::transform(t.subMatrix(), v); }
      };

    template <unsigned int dim>
      struct Eq<RotationMatrix<dim>>
      {
        static const bool value = true;

        static bool equal(const RotationMatrix<dim>& a, const RotationMatrix<dim>& b)
        { return a.subMatrix() == b.subMatrix(); }
      };

    template <unsigned int dim>
      std::ostream& operator<<(std::ostream& out, const RotationMatrix<dim>& mat)
      {
        out << "RotationMatrix" << dim << " {";

        for (unsigned i = 0; i < dim; ++i)
        {
          out << " { ";

          for (unsigned j = 0; j < dim; ++j)
            out << mat.rotation().at(i, j) << " ";

          out << "}";
        }

        out << " }";

        return out;
      }
  } // end Algebra
} // end Falling
#endif // FALLING_ALGEBRA_ROTATIONMATRIX
