#ifndef FALLING_ALGEBRA_CROSSPROD
# define FALLING_ALGEBRA_CROSSPROD

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/Fixme.hh>

namespace Falling
{
  namespace Algebra
  {
# define static_assert_CrossProd(TYPE)                                               \
         static_assert(Falling::Algebra::CrossProdTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have a cross product.")
    template <class V, typename Enabler = void>
      struct CrossProdTrait
      {
        static const bool value = false;

        using ResultType = Metal::FIXME;

        static ResultType cross(const V&, const V&)
        { static_assert_CrossProd((V)); }
      };

    template <typename V , BIND_CONCEPT(CPT, (CrossProdTrait<V>))>
      typename CPT::ResultType cross(const V& a, const V& b)
      {
        return CPT::cross(a, b);
      }
  } // end Algebra
} // end Falling

#endif // FALLING_ALGEBRA_CROSSPROD
