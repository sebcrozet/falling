#ifndef FALLING_ALLOCATOR_RECYCLER
# define FALLING_ALLOCATOR_RECYCLER

# include <vector>

namespace Falling
{
  namespace Allocator
  {
    template <typename E>
    class Recycler
    {
      private:
        std::vector<E*> _cache;

      public:
        void freeCacheMemory()
        {
          for (auto e : _cache)
            delete e;

          _cache.clear();
        }

        void free(E* e)
        {
          assert(std::find(_cache.begin(), _cache.end(), e) == _cache.end() && "Double free.");

          _cache.push_back(e);
        }

        E* alloc()
        {
          if (_cache.size())
          {
            auto res = _cache.back();
            _cache.pop_back();

            return res;
          }
          else
            return new E();
        }
    };
  }
}

#endif // end FALLING_ALLOCATOR_RECYCLER
