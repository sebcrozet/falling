#ifndef FALLING_BODY_TRANSFORMABLE
# define FALLING_BODY_TRANSFORMABLE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Algebra/Transform.hh>

namespace Falling
{
  namespace Body
  {
    using namespace Algebra;

# define static_assert_Transformable(TYPE)                                               \
         static_assert(Falling::Body::TransformableTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be transformable.")
    template <class P, typename Enabler = void>
      struct TransformableTrait
      {
        static const bool value = false;

        using T = typename P::TransformType;

        static const T& localToWorld(const P&);
        static const T& worldToLocal(const P&);
        static void append(P&, const T&);
      };

    template <class P, BIND_CONCEPT(TT, (TransformableTrait<P>))>
    auto localToWorld(const P& p) -> decltype(TT::localToWorld(p))
    { return TT::localToWorld(p); }

    template <class P, BIND_CONCEPT(TT, (TransformableTrait<P>))>
    auto worldToLocal(const P& p) -> decltype(TT::worldToLocal(p))
    { return TT::worldToLocal(p); }

    template <class P, BIND_CONCEPT(TT, (TransformableTrait<P>))>
    void append(P& p, const typename P::TransformType& t)
    { TT::append(p, t); }
  } // end Body
} // end Falling

#endif // FALLING_BODY_TRANSFORMABLE
