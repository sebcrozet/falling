#ifndef FALLING_BODY_HASSHAPE
# define FALLING_BODY_HASSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/Fixme.hh>
// FIXME: should go on the Shape subfolder?

namespace Falling
{
  namespace Body
  {
# define static_assert_HasShape(TYPE)                                            \
         static_assert(Falling::Body::HasShapeTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have a shape.")
    template <typename RB, typename Enabler = void>
      struct HasShapeTrait
      {
        static const bool value = false;

        using ShapeType = Metal::FIXME;

        static const ShapeType* shape(const RB&);
      };

    template <typename RB, BIND_CONCEPT(HST, (HasShapeTrait<RB>))>
      auto shape(const RB& rb) -> decltype(HST::shape(rb))
      {
        return HST::shape(rb);
      }
  } // end Body
} // end Falling

#endif // FALLING_BODY_HASSHAPE
