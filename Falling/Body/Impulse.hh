#ifndef FALLING_BODY_IMPULSE
# define FALLING_BODY_IMPULSE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/ScalarMult.hh>
# include <Falling/Body/Dynamic.hh>
# include <Falling/Body/Transformable.hh>

namespace Falling
{
  namespace Body
  {
    // FIXME: this file is largely incomplete
    using namespace Falling::Algebra;

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      void applyLinearVelocityImpulse(B& body, const typename DT::LinearVelocityType& imp)
      {
        static_assert_ScalarMult((typename DT::LinearVelocityType));
        static_assert_AbelianGroup((typename DT::LinearVelocityType));

        typename DT::LinearVelocityType res = linearVelocity(body);

        setLinearVelocity(body, addTo(inverseMass(body) * imp, res));
      }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      void applyAngularVelocityImpulse(B&, const typename DT::AngularVelocityType&)
      {
      }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      void applyVelocityImpulses(B&                                        body
                                 , const typename DT::LinearVelocityType&  linearImpulse
                                 , const typename DT::AngularVelocityType& angularImpulse)
      {
        applyLinearVelocityImpulse(body, linearImpulse);
        applyAngularVelocityImpulse(body, angularImpulse);
      }

    template <typename B
              , BIND_CONCEPT(DT, (DynamicTrait<B>))
              , BIND_CONCEPT(TT, (TransformableTrait<B>))>
      void applyLinearPositionImpulse(B&, const typename DT::LinearPositionType&)
      {
      }

    template <typename B
              , BIND_CONCEPT(DT, (DynamicTrait<B>))
              , BIND_CONCEPT(TT, (TransformableTrait<B>))>
      void applyAngularPositionImpulse(B&, const typename DT::AngularPositionType&)
      {
      }

    template <typename B
              , BIND_CONCEPT(DT, (DynamicTrait<B>))
              , BIND_CONCEPT(TT, (TransformableTrait<B>))>
      void applyPositionImpulses(B&
                                 , const typename DT::LinearPositionType&
                                 , const typename DT::AngularPositionType&)
      {
      }
  } // end Body
} // end falling

#endif // FALLING_BODY_IMPULSE
