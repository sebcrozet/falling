#ifndef FALLING_BODY_BODYCOLLISIONDISPATCHER
# define FALLING_BODY_BODYCOLLISIONDISPATCHER

# include <Falling/Body/RigidBody.hh>

namespace Falling
{
  namespace Body
  {
    template <typename SD, typename RB>
      class RigidBodyCollisionDispatcher;

    template <typename      SD // shape dispatcher
              , typename    S
              , typename    T
              , typename    II
              , typename    IIT
              , typename... Proxys>
      class RigidBodyCollisionDispatcher<SD, RigidBody<S, T, II, IIT, Proxys...>>
      {
        private:
          SD _shapeDispatcher;

        public:
          using RB = RigidBody<S, T, II, IIT, Proxys...>;


        SD& shapeDispatcher() const
        { return _shapeDispatcher; }

        typename SD::NarrowPhaseType* operator()(const RB* a, const RB* b)
        {
          if (a->isStatic() && b->isStatic())
            return nullptr;

          auto dispatcher = _shapeDispatcher(shape(*a), shape(*b));
          assert(dispatcher != nullptr && "No dispatcher registered.");

          return dispatcher;
        }
      };
  } // end Body
} // end Falling

#endif // FALLING_BODY_BODYCOLLISIONDISPATCHER
