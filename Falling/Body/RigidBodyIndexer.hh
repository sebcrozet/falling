#ifndef FALLING_BODY_BODYINDEXER
# define FALLING_BODY_BODYINDEXER

# include <Falling/Constraint/Solver/IndexProxy.hh>
# include <Falling/Body/RigidBody.hh>

namespace Falling
{
  namespace Body
  {
    template <typename RB>
      class RigidBodyIndexer;

    template <typename S, typename T, typename II, typename IIT, typename... Proxys>
      class RigidBodyIndexer<RigidBody<S, T, II, IIT, Proxys...>>
      {

        using IP = Falling::Constraint::Solver::IndexProxy;
        using RB = RigidBody<S, T, II, IIT, Proxys...>;

        static_assert_HasProxy((IP), (RB));

        public:
        unsigned operator()(std::vector<RB*>& bodies)
        {
          unsigned i = 0;

          for (auto body : bodies)
          {
            if (body->isStatic())
              Metal::proxy<IP>(*body).index = -1;
            else
              Metal::proxy<IP>(*body).index = i++;
          }

          return i;
        }
      };
  }
} // end Falling

#endif // FALLING_BODY_BODYINDEXER
