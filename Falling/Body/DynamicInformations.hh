#ifndef FALLING_BODY_DYNAMICINFORMATIONS
# define FALLING_BODY_DYNAMICINFORMATIONS

namespace Falling
{
  namespace Body
  {
    template <typename LV, typename AV, typename IIT>
    struct DynamicInformations
    {
      double volume;
      double inverseMass;
      IIT    inverseInertiaTensor;
      IIT    worldSpaceInverseInertiaTensor;

      LV     linearVelocity;
      AV     angularVelocity;
      LV     externalLinearForces;
      AV     externalAngularForces;
    };
  } // end Body
} // end Falling

#endif // FALLING_BODY_DYNAMICINFORMATIONS

