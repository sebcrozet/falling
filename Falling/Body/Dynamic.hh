#ifndef FALLING_BODY_DYNAMIC
# define FALLING_BODY_DYNAMIC

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Body
  {
# define static_assert_Dynamic(TYPE)                                            \
         static_assert(Falling::Body::DynamicTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have the properties of a dynamic physics object.")
    template <typename B, typename Enabler = void>
    struct DynamicTrait
    {
      static const bool value = false;

      using T  = typename B::TransformType;
      using LV = typename B::LinearVelocityType;
      using AV = typename B::AngularVelocityType;
      using II = typename B::InverseInertiaTensorType;

      // velocity properties
      static void setLinearVelocity(B&, const LV&);
      static const LV& linearVelocity(const B&);
      // performances could be improved if we had something like:
      // static LV& linearVelocity(const B&);

      static void setAngularVelocity(B&, const AV&);
      static const AV& angularVelocity(const B&);

      // mass properties
      static void setInverseMass(B&, double);
      static double inverseMass(const B&);

      static void setWorldSpaceInverseInertiaTensor(B&, const II&);
      static const II& worldSpaceInverseInertiaTensor(const B&);

      static void setExternalLinearForce(B&, const LV&);
      static void setExternalAngularForce(B&, const AV&);
      static const LV& externalLinearForce(const B&);
      static const AV& externalAngularForce(const B&);
    };

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static void setLinearVelocity(B& b, const typename DT::LinearVelocityType& lv)
      { DT::setLinearVelocity(b, lv); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static auto linearVelocity(const B& b) -> decltype(DT::linearVelocity(b))
      { return DT::linearVelocity(b); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static void setAngularVelocity(B& b, const typename DT::AngularVelocityType& av)
      { DT::setAngularVelocity(b, av); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static auto angularVelocity(const B& b) -> decltype(DT::angularVelocity(b))
      { return DT::angularVelocity(b); }

    // mass properties
    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static void setInverseMass(B& b, double m)
      { DT::setInverseMass(b, m); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static double inverseMass(const B& b)
      { return DT::inverseMass(b); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static void setWorldSpaceInverseInertiaTensor(
          B&                                              b,
          decltype(DT::worldSpaceInverseInertiaTensor(b)) ii)
      { DT::setWorldSpaceInverseInertiaTensor(b, ii); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static const typename DT::InverseInertiaTensorType& worldSpaceInverseInertiaTensor(
          const B& b)
      { return DT::worldSpaceInverseInertiaTensor(b); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static void setExternalLinearForce(B& b, decltype(externalLinearForce(b)) lv)
      { DT::setExternalLinearForce(b, lv); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static void setExternalAngularForce(B& b, const typename DT::AngularVelocityType& av)
      { DT::setExternalAngularForce(b, av); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static auto externalLinearForce(const B& b) -> decltype(DT::externalLinearForce(b))
      { return DT::externalLinearForce(b); }

    template <typename B, BIND_CONCEPT(DT, (DynamicTrait<B>))>
      static auto externalAngularForce(const B& b) -> decltype(DT::externalAngularForce(b))
      { return DT::externalAngularForce(b); }
  } // end Body
} // end Falling

#endif // FALLING_BODY_DYNAMIC
