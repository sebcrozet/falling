#ifndef FALLING_BODY_BODY
# define FALLING_BODY_BODY

# include <tuple>

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/HasProxy.hh>
# include <Falling/Metal/FConvert.hh>
# include <Falling/Metal/Inherit.hh>
# include <Falling/Metal/Find.hh>
# include <Falling/Metal/List.hh>

# include <Falling/Algebra/TransformSystem.hh>
# include <Falling/Shape/TransformableShape.hh>
# include <Falling/Shape/VolumetricShape.hh>
# include <Falling/Shape/HasCenterOfMass.hh>
# include <Falling/BoundingVolume/HasBoundingVolume.hh>
# include <Falling/Body/Dynamic.hh>
# include <Falling/Body/Transformable.hh>
# include <Falling/Body/HasShape.hh>
# include <Falling/Body/DynamicInformations.hh>

namespace Falling
{
    namespace Body
    {
      using namespace Falling::Algebra;
      using namespace Falling::Shape;

      template <typename      S
                , typename    T // Must be a Lattice…
                , typename    I
                , typename... Proxys>
      class RigidBody
      {
        static_assert_TransformSystem((T));

        private:
          using TST  = TransformSystemTrait<T>;

          friend struct DynamicTrait<RigidBody<S, T, I, Proxys...>>;
          friend struct TransformableTrait<RigidBody<S, T, I, Proxys...>>;
          friend struct TranslatableTrait<RigidBody<S, T, I, Proxys...>>;
          friend struct RotatableTrait<RigidBody<S, T, I, Proxys...>>;

        public:
          using TransformType            = T;
          using LinearPositionType       = typename TST::AsTranslatable::TranslationType;
          using AngularPositionType      = typename TST::AsRotatable::RotationType;
          using LinearVelocityType       = typename TST::AsTranslatable::TranslationType;
          using AngularVelocityType      = typename TST::AsRotatable::RotationType;
          using ShapeType                = typename Metal::FConvert<S, Metal::InheritLattice>::type::type;
          using ConstShapeType           = typename Metal::ConstInheritLattice<Dynamic::TransformableShape<T>>::type;
          using InertiaTensorType        = I;
          using InverseInertiaTensorType = I;
          using DynamicInformationsType  = DynamicInformations<LinearVelocityType
                                                               , AngularVelocityType
                                                               , InertiaTensorType>;
        
        private:
          TransformType         _localToWorld;
          TransformType         _worldToLocal;

          const ConstShapeType* _shape;
          // those are lazyly updated values. Thus, we need the mutablility.
          mutable ShapeType*    _transformedShape;
          mutable bool          _movedShape = false;

          DynamicInformationsType* _dynamics;
          std::tuple<Proxys...>    _proxys;

        public:
            std::tuple<Proxys...>& proxys()
            { return _proxys; }

            const std::tuple<Proxys...>& proxys() const
            { return _proxys; }

            template <typename Proxy>
              Proxy& proxy()
              { return std::get<Metal::Find<Proxy, Metal::List<Proxys...>>::value>(_proxys); }

            template <typename Proxy>
              const Proxy& proxy() const
              { return std::get<Metal::Find<Proxy, Metal::List<Proxys...>>::value>(_proxys); }

            bool isStatic() const
            { return _dynamics == nullptr; }

            ~RigidBody()
            {
              delete _shape;

              if (_transformedShape)
              {
                delete _transformedShape->value().template valueAs<Metal::Empty>();

                delete _transformedShape;
              }
            }

            template <typename ST>
            RigidBody(const ST* shape)
            {
              assert(shape && "The shape must not be null.");

              using WS =
                typename Metal::FConvert<
                  typename Metal::ApplyToStaticBridge<
                    Metal::List<Dynamic::TransformableShape<T>>, ST>::type
                    , Metal::ConstDynamicValue
                  >::type;

              using MWS =
                typename Metal::FConvert<
                  typename Metal::ApplyToStaticBridge<
                    S
                    , typename Static::TransformableShapeTrait<ST, T>::TransformedShapeType
                  >::type
                  , Metal::DynamicValue
                  >::type;

              _shape            = new WS(*shape);
              _dynamics         = nullptr;
              _transformedShape = new MWS((ST*)nullptr);
              _localToWorld     = TST::AsMultInversibleSemiGroup::one;
              _bodyMoved();
            }

            template <typename ST, CHECK_CONCEPT((VolumetricShapeTrait<ST>))>
            RigidBody(const ST*                      shape,
                      const DynamicInformationsType& dynamics)
            : RigidBody(shape)
            {
              double density = 1.0; // FIXME: do not hard-code that
              double mass    = volume(*shape) * density;

              _initDynamicsInfos(dynamics, mass, inertiaTensor(*shape, mass));
            }

          template <typename ST, CHECK_CONCEPT((VolumetricShapeTrait<ST>))>
            RigidBody(const ST* shape, bool isDynamic)
            : RigidBody(shape)
            {
              if (isDynamic)
              {
                double density = 1.0; // FIXME: do not hard-code that
                double mass    = volume(*shape) * density;

                _initDynamicsInfos(DynamicInformationsType(), mass, inertiaTensor(*shape, mass));
              }
            }

          const ShapeType* shape() const
          { 
            if (_movedShape)
            {
              Static::transformShape(*_shape, _localToWorld, _transformedShape);

              assert(_transformedShape->value().template valueAs<Metal::Empty>());
              _movedShape = false;
            }

            return _transformedShape;
          }

        private:
          void _initDynamicsInfos(
              const DynamicInformationsType&    dynamics
              , double                          mass
              , const InverseInertiaTensorType& angularInertia)
          {
            if (!_dynamics)
              _dynamics  = new DynamicInformationsType();

            *_dynamics = dynamics;
            _dynamics->inverseMass          = 1.0 / mass;
            _dynamics->inverseInertiaTensor = angularInertia;
            invert(_dynamics->inverseInertiaTensor);

            // FIXME:
            _dynamics->worldSpaceInverseInertiaTensor = _dynamics->inverseInertiaTensor;
          }

          void _bodyMoved()
          {
            _movedShape = true;
          }
      };

      template <typename S, typename T, typename I, typename... Proxys>
        struct HasShapeTrait<RigidBody<S, T, I, Proxys...>>
        {
          static const bool value = true;

          using ShapeType = typename S::type;

          static const ShapeType* shape(const RigidBody<S, T, I, Proxys...>& body)
          { return body.shape(); }
        };

      template <typename S, typename T, typename I, typename... Proxys>
        struct DynamicTrait<RigidBody<S, T, I, Proxys...>>
        {
          static const bool value = true;

          using B = RigidBody<S, T, I, Proxys...>;

          using LinearVelocityType       = typename B::LinearPositionType;
          using AngularVelocityType      = typename B::AngularPositionType;
          using InverseInertiaTensorType = typename B::InverseInertiaTensorType;

          static void setLinearVelocity(B& body, const LinearVelocityType& linVel)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            body._dynamics->linearVelocity = linVel;
          }

          static const LinearVelocityType& linearVelocity(const B& body)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            return body._dynamics->linearVelocity;
          }

          static void setAngularVelocity(B& body, const AngularVelocityType& angVel)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            body._dynamics->angularVelocity = angVel;
          }

          static const AngularVelocityType& angularVelocity(const B& body)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            return body._dynamics->angularVelocity;
          }

          // mass properties
          static void setInverseMass(B& body, double m)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            body._dynamics->inverseMass = m;
          }

          static double inverseMass(const B& body)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            return body._dynamics->inverseMass;
          }

          static void setWorldSpaceInverseInertiaTensor(B& body, const I& tensor)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            body._dynamics->inverseInertiaTensor = tensor;
          }

          static const I& worldSpaceInverseInertiaTensor(const B& body)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            return body._dynamics->worldSpaceInverseInertiaTensor;
          }

          static void setExternalLinearForce(B& body, const LinearVelocityType& linVel)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            body._dynamics->externalLinearForces = linVel;
          }

          static void setExternalAngularForce(B& body, const AngularVelocityType& angVel)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            body._dynamics->externalAngularForces = angVel;
          }

          static const LinearVelocityType& externalLinearForce(const B& body)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            return body._dynamics->externalLinearForces;
          }

          static const AngularVelocityType& externalAngularForce(const B& body)
          {
            assert(body._dynamics && "This rigid body is not dynamic.");
            return body._dynamics->externalAngularForces;
          }
        };

      template <typename S, typename T, typename I, typename... Proxys>
        struct TransformableTrait<RigidBody<S, T, I, Proxys...>>
        {
          using RB = RigidBody<S, T, I, Proxys...>;

          static const bool value = true;

          static const T& localToWorld(const RB& b)
          {
            return b._localToWorld;
          }

          static const T& worldToLocal(const RB& b)
          {
            return b._worldToLocal;
          }

          static void append(RB& b, const T& t)
          {
            b._localToWorld = t * b._localToWorld;
            b._worldToLocal = inverse(b._localToWorld);
            b._bodyMoved();
            // b._inverseInertiaTensor =
            //   toWorldSpaceInverseInertiaTensor(
            //       b._localToWorld
            //       , b._worldToLocal
            //       , b._inverseInertiaTensor);
          }
        };
    } // end Body

    namespace Algebra
    {
      template <typename S, typename T, typename I, typename... Proxys>
        struct TranslatableTrait<Body::RigidBody<S, T, I, Proxys...>>
        {
          static const bool value = true;

          using RB = Body::RigidBody<S, T, I, Proxys...>;
          using TranslationType =
            typename TransformSystemTrait<T>::AsTranslatable::TranslationType;

          static TranslationType translation(const RB& b)
          { return TranslatableTrait<T>::translation(b._localToWorld); }

          static RB& translate(const TranslationType& t, RB& b)
          {
            TranslatableTrait<T>::translate(t, b._localToWorld);
            return b;
          }
        };

      template <typename S, typename T, typename I, typename... Proxys>
        struct RotatableTrait<Body::RigidBody<S, T, I, Proxys...>>
        {
          static const bool value = true;

          using RB = Body::RigidBody<S, T, I, Proxys...>;
          using RotationType =
            typename TransformSystemTrait<T>::AsRotatable::RotationType;

          static RotationType rotation(const RB& b)
          { return RotatableTrait<T>::rotation(b._localToWorld); }

          static RB& rotate(const RotationType& t, RB& b)
          {
            RotatableTrait<T>::rotate(t, b._localToWorld);
            return b;
          }
        };
    } // end Algebra

    namespace BoundingVolume
    {
      namespace Static
      {
        template <typename      S
                  , typename    T
                  , typename    I
                  , typename... Proxys
                  , typename    BV>
        struct HasBoundingVolumeTrait<
          Body::RigidBody<S, T, I, Proxys...>
          , BV
          , CHECK_ENABLER((
              HasBoundingVolumeTrait<
                typename Body::RigidBody<S, T, I, Proxys...>::ShapeType
                , BV
              >))>
        {
          static const bool value = true;

          using RB = Body::RigidBody<S, T, I, Proxys...>;

          static BV boundingVolume(const RB& rb)
          {
            using HBVT = HasBoundingVolumeTrait<typename RB::ShapeType, BV>;

            return HBVT::boundingVolume(*shape(rb));
          }

          static void boundingVolume(const RB& rb, BV& bv)
          {
            return boundingVolume(*shape(rb), bv);
          }
        };
      } // end Static
    } // end BoundingVolume

    namespace Shape
    {
      template <typename S, typename T, typename I, typename... Proxys>
        struct HasCenterOfMass<Body::RigidBody<S, T, I, Proxys...>>
        {
          static const bool value = true;

          using CenterOfMassType =
            typename Body::RigidBody<S, T, I, Proxys...>::LinearPositionType;

          static CenterOfMassType centerOfMass(const Body::RigidBody<S, T, I, Proxys...>& rb)
          { return translation(localToWorld(rb)); }

        };
    }

    namespace Metal
    {
      template <typename S, typename T, typename I, typename... Proxys , typename P>
      struct HasProxyTrait<P
                           , Body::RigidBody<S, T, I, Proxys...>
                           , typename std::enable_if<Metal::Find<P, List<Proxys...>>::value != -1, void>::type>
      {
        static const bool value = true;

        static const P& proxy(const Body::RigidBody<S, T, I, Proxys...>& b)
        { return b.template proxy<P>(); }

        static P& proxy(Body::RigidBody<S, T, I, Proxys...>& b)
        { return b.template proxy<P>(); }
      };
    } // end Metal
} // end Falling

#endif // FALLING_BODY_BODY
