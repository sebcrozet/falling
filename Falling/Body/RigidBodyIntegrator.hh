#ifndef FALLING_BODY_BODYINTEGRATOR
# define FALLING_BODY_BODYINTEGRATOR

# include <Falling/Integrator/Integrator.hh>
# include <Falling/Body/RigidBody.hh>

namespace Falling
{
  namespace Body
  {
    template <template <typename RB> class SI, typename RB>
      class RigidBodyIntegrator;

    template <template <typename RB> class SI
              , typename    S
              , typename    T
              , typename    II
              , typename    IIT //  = typename II::InverseInertiaTensorType
              , typename... Proxys>
      class RigidBodyIntegrator<SI, RigidBody<S, T, II, IIT, Proxys...>>
      {
        private:
          using B = RigidBody<S, T, II, IIT, Proxys...>;

        static_assert_Integrator((SI<B>), (B));

        private:
          SI<B>* _subIntegrator;

        public:
          SI<B>& subIntegrator()
          { return *_subIntegrator; }

          const SI<B>& subIntegrator() const
          { return *_subIntegrator; }

          explicit RigidBodyIntegrator(SI<B>* subIntegrator)
          {
            assert(subIntegrator != nullptr);

            _subIntegrator = subIntegrator;
          }

          ~RigidBodyIntegrator()
          {
            delete _subIntegrator;
          }
      };
  } // end Body

  namespace Integrator
  {
    template <template <typename RB> class SI, typename B>
    struct IntegratorTrait<RigidBodyIntegrator<SI, B>, B>
    {
      private:
          using IT = RigidBodyIntegrator<SI, B>;

      public:
        static const bool value = true;

        static void integrate(IT& integrator, double dt, B& body)
        {
          if (!body.isStatic())
            IntegratorTrait<SI<B>, B>::integrate(integrator.subIntegrator(), dt, body);
        }
    };
  } // end Integrator
}; // end Falling

#endif // FALLING_BODY_BODYINTEGRATOR
