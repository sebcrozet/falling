#ifndef FALLING_SHAPE_TRANSFORMEDSHAPE_HASCENTEROFMASS
# define FALLING_SHAPE_TRANSFORMEDSHAPE_HASCENTEROFMASS

# include <Falling/Shape/HasCenterOfMass.hh>
# include <Falling/Shape/TransformedShape/TransformedShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename T>
      struct HasCenterOfMass<TransformedShape<S, T>, CHECK_ENABLER((HasCenterOfMass<S>))>
      {
        static const bool value = true;

        using CenterOfMassType = typename HasCenterOfMass<S>::CenterOfMassType;

        static CenterOfMassType centerOfMass(const TransformedShape<S, T>& shape)
        {
          assert(shape.subShape() && "The shape must not be null.");

          return transform(shape.transform(), HasCenterOfMass<S>::centerOfMass(*shape.subShape()));
        }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_TRANSFORMEDSHAPE_HASCENTEROFMASS
