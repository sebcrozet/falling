#ifndef FALLING_SHAPE_TRANSFORMEDSHAPE_IMPLICITSHAPE
# define FALLING_SHAPE_TRANSFORMEDSHAPE_IMPLICITSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Shape/TransformedShape/TransformedShape.hh>
# include <Falling/Shape/ImplicitShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename T, typename V>
      struct ImplicitShapeTrait<TransformedShape<S, T>
                                 , V
                                 , CHECK_ENABLER((ImplicitShapeTrait<S, V>))>
      {
        static const bool value = true;

        static const V supportPoint(const TransformedShape<S, T>& s, const V& v)
        {
          return supportPointWithTransform(*s.subShape(), v, s.transform());
        }
      };
  } // end TransformedShape
} // end Falling

#endif // FALLING_SHAPE_TRANSFORMEDSHAPE_IMPLICITSHAPE
