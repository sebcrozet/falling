#ifndef FALLING_SHAPE_TRANSFORMEDSHAPE_TRANSFORMABLESHAPE
# define FALLING_SHAPE_TRANSFORMEDSHAPE_TRANSFORMABLESHAPE

# include <Falling/Algebra/MultInversibleSemiGroup.hh>
# include <Falling/Shape/TransformableShape.hh>
# include <Falling/Shape/TransformedShape/TransformedShape.hh>

namespace Falling
{
  namespace Shape
  {
    namespace Static
    {
      template <typename S, typename T>
        struct TransformableShapeTrait<TransformedShape<S, T>
                                        , T
                                        , CHECK_ENABLER((TransformableShapeTrait<S, T>))>
        {
          static const bool value = true;

          static_assert_MultInversibleSemiGroup((T));

          using TransformedShapeType = typename TransformableShapeTrait<S, T>::TransformedShapeType;

          static TransformedShapeType* transformShape(const TransformedShape<S, T>& in
                                                      , const T&                    t
                                                      , TransformedShapeType*       out)
          {
            return TransformableShapeTrait<S, T>::transformShape(
                *in.subShape()
                , t * in.transform()
                , out);
          }
        };
    } // end Static
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_TRANSFORMEDSHAPE_TRANSFORMABLESHAPE
