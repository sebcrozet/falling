#ifndef FALLING_SHAPE_TRANSFORMEDSHAPE_VOLUMETRICSHAPE
# define FALLING_SHAPE_TRANSFORMEDSHAPE_VOLUMETRICSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Shape/VolumetricShape.hh>
# include <Falling/Shape/TransformedShape/TransformedShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename T>
      struct VolumetricShapeTrait<TransformedShape<S, T>, CHECK_ENABLER((VolumetricShapeTrait<S>))>
      {
        static const bool value = true;

        using InertiaTensorType = typename VolumetricShapeTrait<S>::InertiaTensorType;

        static double volume(const TransformedShape<S, T>& shape)
        { return VolumetricShapeTrait<S>::volume(shape.subShape()); }

        static double inertiaTensor(const TransformedShape<S, T>& shape)
        {
          return shape.transform()
                 * VolumetricShapeTrait<S>::inertiaTensor(shape.subShape())
                 * inverse(shape.transform());
        }
      };
  } // end Shape
} // end Falling

#endif // end FALLING_SHAPE_TRANSFORMEDSHAPE_VOLUMETRICSHAPE
