#ifndef FALLING_SHAPE_TRANSFORMEDSHAPE_TRANSFORMEDSHAPE
# define FALLING_SHAPE_TRANSFORMEDSHAPE_TRANSFORMEDSHAPE

# include <Falling/Metal/Empty.hh>
# include <Falling/Algebra/MultInversibleSemiGroup.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename T>
      class TransformedShape : public Metal::Empty
      {
        private:
          const S* _subShape;
          T        _transform;

        public:
          TransformedShape(const S* subShape, const T& transform)
            : _subShape(subShape), _transform(transform)
          { }

          TransformedShape()
            : TransformedShape(nullptr, Algebra::MultInversibleSemiGroupTrait<T>::one)
          { }

          void setSubShape(S* subShape)
          {
            assert(subShape != nullptr);
            _subShape = subShape;
          }

          const S* subShape() const
          { return _subShape; }

          T& transform()
          { return _transform; }

          const T& transform() const
          { return _transform; }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_TRANSFORMEDSHAPE_TRANSFORMEDSHAPE
