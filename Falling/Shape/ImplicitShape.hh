#ifndef FALLING_SHAPE_IMPLICITSHAPE
# define FALLING_SHAPE_IMPLICITSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Algebra/Transform.hh>
# include <Falling/Algebra/DeltaTransform.hh>

namespace Falling
{
  namespace Shape
  {
    using namespace Algebra;

# define static_assert_ImplicitShape(TYPEG, TYPEV)                                                \
         static_assert(Falling::Shape::ImplicitShapeTrait<                                        \
             typename RMPAR(TYPEG)                                                                \
             , typename RMPAR(TYPEV)>::value                                                      \
             , "Type parameter " #TYPEG " must be an implicit shape with support points of type " \
               #TYPEV ".")
    template <typename G, typename V, typename Enabler = void>
      struct ImplicitShapeTrait
      {
        static const bool value = false;

        static const V supportPoint(const G&, const V&);
      };

    template <typename G, typename V, BIND_CONCEPT(IST, (ImplicitShapeTrait<G, V>))>
    const V supportPoint(const G& g, const V& v)
    { return IST::supportPoint(g, v); }

    template <
      typename G, typename T, typename V
      , CHECK_CONCEPT((TransformTrait<T, V>))
      , CHECK_CONCEPT((DeltaTransformTrait<T, V>))
      , CHECK_CONCEPT((ImplicitShapeTrait<G, V>))
      >
      const V supportPointWithTransform(const G& geometry, const V& dir, const T& t)
      { return transform(t, supportPoint(geometry, deltaTransformTranspose(t, dir))); }
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_IMPLICITSHAPE
