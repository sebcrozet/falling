#ifndef FALLING_SHAPE_TRANSLATEDSHAPE_VOLUMETRICSHAPE
# define FALLING_SHAPE_TRANSLATEDSHAPE_VOLUMETRICSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Shape/VolumetricShape.hh>
# include <Falling/Shape/TranslatedShape/TranslatedShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename T>
      struct VolumetricShapeTrait<TranslatedShape<S, T>, CHECK_ENABLER((VolumetricShapeTrait<S>))>
      {
        static const bool value = true;

        using InertiaTensorType = typename VolumetricShapeTrait<S>::InertiaTensorType;

        static double volume(const TranslatedShape<S, T>& shape)
        { return VolumetricShapeTrait<S>::volume(shape.subShape()); }

        static double inertiaTensor(const TranslatedShape<S, T>& shape)
        {
          // FIXME: take the translation in account?
          return VolumetricShapeTrait<S>::inertiaTensor(shape.subShape());
        }
      };
  } // end Shape
} // end Falling

#endif // end FALLING_SHAPE_TRANSLATEDSHAPE_VOLUMETRICSHAPE
