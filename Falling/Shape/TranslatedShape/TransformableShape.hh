#ifndef FALLING_SHAPE_TRANSLATEDSHAPE_TRANSFORMABLESHAPE
# define FALLING_SHAPE_TRANSLATEDSHAPE_TRANSFORMABLESHAPE

# include <Falling/Algebra/MultInversibleSemiGroup.hh>
# include <Falling/Shape/TransformableShape.hh>
# include <Falling/Shape/TranslatedShape/TranslatedShape.hh>

namespace Falling
{
  namespace Shape
  {
    namespace Static
    {
      template <typename S, typename T>
        struct TransformableShapeTrait<TranslatedShape<S, T>
                                        , T
                                        , CHECK_ENABLER((TransformableShapeTrait<S, T>))>
        {
          static const bool value = true;

          using TranslatedShapeType = typename TransformableShapeTrait<S, T>::TranslatedShapeType;

          static TranslatedShapeType* transformShape(const TranslatedShape<S, T>& in
                                                     , const T&                   t
                                                     , TranslatedShapeType*       out)
          {
            T translated = t;

            return TransformableShapeTrait<S, T>::transformShape(
                in.subShape()
                , translate(in.translation(), t)
                , out);
          }
        };
    } // end Static
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_TRANSLATEDSHAPE_TRANSFORMABLESHAPE
