#ifndef FALLING_SHAPE_TRANSLATEDSHAPE_TRANSLATEDSHAPE
# define FALLING_SHAPE_TRANSLATEDSHAPE_TRANSLATEDSHAPE

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename V>
      class TranslatedShape
      {
        private:
          const S& _s;
          const V  _translation;

        public:
          TranslatedShape(const S& s, const V& translation)
            : _s(s), _translation(translation)
          { }

          const S& subShape() const
          { return _s; }

          const V& translation() const
          { return _translation; }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_TRANSLATEDSHAPE_TRANSLATEDSHAPE
