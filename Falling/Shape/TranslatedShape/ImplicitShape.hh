#ifndef FALLING_SHAPE_TRANSLATEDSHAPE_IMPLICITSHAPE
# define FALLING_SHAPE_TRANSLATEDSHAPE_IMPLICITSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Shape/TranslatedShape/TranslatedShape.hh>
# include <Falling/Shape/ImplicitShape.hh>
# include <Falling/Algebra/AbelianGroup.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename V>
      struct ImplicitShapeTrait<TranslatedShape<S, V>
                                 , V
                                 , CHECK_ENABLER((ImplicitShapeTrait<S, V>))>
      {
        static const bool value = true;

        static const V supportPoint(const TranslatedShape<S, V>& s, const V& v)
        {
          static_assert_AbelianGroup((V));

          return ImplicitShapeTrait<S, V>::supportPoint(s.subShape(), v) + s.translation();
        }
      };
  } // end TranslatedShape
} // end Falling

#endif // FALLING_SHAPE_TRANSLATEDSHAPE_IMPLICITSHAPE
