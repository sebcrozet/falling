#ifndef FALLING_SHAPE_TRANSLATEDSHAPE_HASCENTEROFMASS
# define FALLING_SHAPE_TRANSLATEDSHAPE_HASCENTEROFMASS

# include <Falling/Shape/HasCenterOfMass.hh>
# include <Falling/Shape/TranslatedShape/TranslatedShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename V>
      struct HasCenterOfMass<TranslatedShape<S, V>, CHECK_ENABLER((HasCenterOfMass<S>))>
      {
        static const bool value = true;

        using CenterOfMassType = V;

        static CenterOfMassType centerOfMass(const TranslatedShape<S, V>& shape)
        {
          return V(HasCenterOfMass<S>::centerOfMass(shape.subShape())) + shape.translation();
        }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_TRANSLATEDSHAPE_HASCENTEROFMASS
