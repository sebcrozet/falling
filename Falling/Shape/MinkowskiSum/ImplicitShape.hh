#ifndef FALLING_SHAPE_MINKOWSKISUM_IMPLICITSHAPE
# define FALLING_SHAPE_MINKOWSKISUM_IMPLICITSHAPE

# include <Falling/Shape/MinkowskiSum/MinkowskiSum.hh>
# include <Falling/Shape/ImplicitShape.hh>
# include <Falling/Shape/MinkowskiSum/AnnotatedSupportPoint.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename SA, typename SB, typename V>
      struct ImplicitShapeTrait<
        MinkowskiSum<SA, SB>
        , V
        , typename std::enable_if<ImplicitShapeTrait<SA, V>::value
                                  && ImplicitShapeTrait<SB, V>::value
                                  , void>::type>
    {
      private:
        using _ISTA = ImplicitShapeTrait<SA, V>;
        using _ISTB = ImplicitShapeTrait<SB, V>;

      public:
        static const bool value = true;

        static const V supportPoint(const MinkowskiSum<SA, SB>& sum, const V& direction)
        {
          using namespace Algebra;

          return _ISTA::supportPoint(sum.sa(), direction)
                 + _ISTB::supportPoint(sum.sb(), direction);
        }
    };
    
    template <typename SA, typename SB, typename V>
      struct ImplicitShapeTrait<
        MinkowskiSum<SA, SB>
        , AnnotatedSupportPoint<V>
        , typename std::enable_if<ImplicitShapeTrait<SA, V>::value
                                  && ImplicitShapeTrait<SB, V>::value
                                  , void>::type>
    {
      private:
        using _ISTA = ImplicitShapeTrait<SA, V>;
        using _ISTB = ImplicitShapeTrait<SB, V>;

      public:
        static const bool value = true;

        static const AnnotatedSupportPoint<V>
          supportPoint(const MinkowskiSum<SA, SB>&       sum
                       , const AnnotatedSupportPoint<V>& direction)
        {
          using namespace Algebra;

          AnnotatedSupportPoint<V> res;

          res.sourceA = _ISTA::supportPoint(sum.sa(), direction.point);
          res.sourceB = _ISTB::supportPoint(sum.sb(), direction.point);
          res.point   = res.sourceA + res.sourceB;

          return res;
        }
    };
  } // end Shape

} // end Falling

#endif // FALLING_SHAPE_MINKOWSKISUM_IMPLICITSHAPE
