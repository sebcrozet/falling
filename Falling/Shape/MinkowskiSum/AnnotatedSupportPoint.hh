#ifndef FALLING_SHAPE_MINKOWSKISUM_ANNOTATEDSUPPORTPOINT
# define FALLING_SHAPE_MINKOWSKISUM_ANNOTATEDSUPPORTPOINT

# include <array>
# include <Falling/Metal/EnableIf.hh>
# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/ScalarOp.hh>
# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/Algebra/UnitSphereSamples.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
    struct AnnotatedSupportPoint
    {
      V point;
      V sourceA;
      V sourceB;

      AnnotatedSupportPoint()
      { }

      explicit AnnotatedSupportPoint(const V& pt, const V& sa, const V& sb)
      {
        point   = pt;
        sourceA = sa;
        sourceB = sb;
      }

      explicit AnnotatedSupportPoint(const V& pt)
      {
        point = pt;
      }
    };

    template <typename V>
      std::ostream& operator<<(std::ostream&                     out
                               , const AnnotatedSupportPoint<V>& pt)
      {
        out << "AnnotatedSupportPoint { " 
            << "point: "    << pt.point
            << " sourceA: " << pt.sourceA
            << " sourceB: " << pt.sourceB
            << " }";
        return out;
      }
  } // end Shape

  namespace Algebra
  {
    template <typename V>
      struct AbelianGroupTrait<Shape::AnnotatedSupportPoint<V>, CHECK_ENABLER((AbelianGroupTrait<V>))>
      {
        static const bool value = true;

        using This       = Shape::AnnotatedSupportPoint<V>;
        using SubConcept = AbelianGroupTrait<V>;

        static const This zero;

        static This& addTo(This& in, const This& out)
        {
          SubConcept::addTo(in.point, out.point);
          SubConcept::addTo(in.sourceA, out.sourceA);
          SubConcept::addTo(in.sourceB, out.sourceB);

          return in;
        }

        static This& subTo(This& in, const This& out)
        {
          SubConcept::subTo(in.point, out.point);
          SubConcept::subTo(in.sourceA, out.sourceA);
          SubConcept::subTo(in.sourceB, out.sourceB);
          return in;
        }

        static This& negate(This& in)
        {
          SubConcept::negate(in.point);
          SubConcept::negate(in.sourceA);
          SubConcept::negate(in.sourceB);
          return in;
        }

        static This add(const This& in, const This& out)
        {
          return This(SubConcept::add(in.point, out.point)
                      , SubConcept::add(in.sourceA, out.sourceA)
                      , SubConcept::add(in.sourceB, out.sourceB));
        }

        static This sub(const This& in, const This& out)
        {
          return This(SubConcept::sub(in.point, out.point)
                      , SubConcept::sub(in.sourceA, out.sourceA)
                      , SubConcept::sub(in.sourceB, out.sourceB));
        }

        static This neg(const This& in)
        {
          return This(SubConcept::neg(in.point)
                      , SubConcept::neg(in.sourceA)
                      , SubConcept::neg(in.sourceB));
        }
      };

    template <typename V>
      const Shape::AnnotatedSupportPoint<V>
      AbelianGroupTrait<Shape::AnnotatedSupportPoint<V>, CHECK_ENABLER((AbelianGroupTrait<V>))>
      ::zero = Shape::AnnotatedSupportPoint<V>(AbelianGroupTrait<V>::zero
                                               , AbelianGroupTrait<V>::zero
                                               , AbelianGroupTrait<V>::zero);

    template <typename V>
      struct ScalarOpTrait<Shape::AnnotatedSupportPoint<V>, CHECK_ENABLER((ScalarOpTrait<V>))>
      {
        static const bool value = true;

        using This       = Shape::AnnotatedSupportPoint<V>;
        using SubConcept = ScalarOpTrait<V>;

        static This& multTo(double s, This& out)
        {
          SubConcept::multTo(s, out.point);
          SubConcept::multTo(s, out.sourceA);
          SubConcept::multTo(s, out.sourceB);
          return out;
        }

        static Shape::AnnotatedSupportPoint<V> mult(double s, const This& in)
        {
          return Shape::AnnotatedSupportPoint<V>(SubConcept::mult(s, in.point)
                                                 , SubConcept::mult(s, in.sourceA)
                                                 , SubConcept::mult(s, in.sourceB));
        }
      };

    template <typename V>
      struct DotProdTrait<Shape::AnnotatedSupportPoint<V>, CHECK_ENABLER((DotProdTrait<V>))>
      {
        static const bool value = true;

        using This       = Shape::AnnotatedSupportPoint<V>;
        using SubConcept = DotProdTrait<V>;

        static double dot(const This& a, const This& b)
        { return SubConcept::dot(a.point, b.point); }

        static double sqlen(const This& a)
        { return SubConcept::sqlen(a.point); }

        static double len(const This& a)
        { return SubConcept::len(a.point); }
      };

    // we need to redefine those too
    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      double normalize(Shape::AnnotatedSupportPoint<V>& v)
      {
        normalize(v.sourceA);
        normalize(v.sourceB);
        return normalize(v.point);
      }

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      Shape::AnnotatedSupportPoint<V> normalized(const Shape::AnnotatedSupportPoint<V>& v)
      {
        auto lenv = len(v);

        assert(lenv != 0.0);

        return Shape::AnnotatedSupportPoint<V>(v.point / lenv
                                               , normalized(v.sourceA)
                                               , normalized(v.sourceB));
      }

    template <typename V>
      struct DimensionTrait<Shape::AnnotatedSupportPoint<V>, CHECK_ENABLER((DimensionTrait<V>))>
      {
        static const bool value = true;

        static const unsigned int dim = DimensionTrait<V>::dim;
      };

    template <typename V>
      struct Eq<Shape::AnnotatedSupportPoint<V>, CHECK_ENABLER((Eq<V>))>
      {
        static const bool value = true;

        using This = Shape::AnnotatedSupportPoint<V>;

        static const bool equal(const This& a ,const This& b)
        { return Eq<V>::equal(a.point, b.point); }
      };

    template <typename V, unsigned int numSamples>
      struct UnitSphereSamples<Shape::AnnotatedSupportPoint<V>
                               , numSamples
                               , CHECK_ENABLER((UnitSphereSamples<V, numSamples>))>
      {
        private:
          static std::array<Shape::AnnotatedSupportPoint<V>, numSamples>
            _generate()
            {
              std::array<Shape::AnnotatedSupportPoint<V>, numSamples> res;

              auto refSamples = UnitSphereSamples<V, numSamples>::samples();

              for (unsigned i = 0; i < numSamples; ++i)
                res[i].point = refSamples[i];

              return res;
            }

          static bool                                                    _initialized;
          static std::array<Shape::AnnotatedSupportPoint<V>, numSamples> _samples;
        public:
          static const bool value = true;

          static const std::array<Shape::AnnotatedSupportPoint<V>, numSamples>& samples()
          {
            if (!_initialized)
            {
              _initialized = true;
              _samples     = _generate();
            }

            return _samples;
          }
      };

    template <typename V, unsigned int numSamples>
      std::array<Shape::AnnotatedSupportPoint<V>, numSamples>
      UnitSphereSamples<Shape::AnnotatedSupportPoint<V>
                        , numSamples
                        , CHECK_ENABLER((UnitSphereSamples<V, numSamples>))>::_samples;

    template <typename V, unsigned int numSamples>
      bool UnitSphereSamples<Shape::AnnotatedSupportPoint<V>
                               , numSamples
                               , CHECK_ENABLER((UnitSphereSamples<V, numSamples>))>
                               ::_initialized = false;

  } // end Algebra
} // end Falling

#endif // FALLING_SHAPE_MINKOWSKISUM_ANNOTATEDSUPPORTPOINT
