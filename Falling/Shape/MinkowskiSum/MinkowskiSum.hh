#ifndef FALLING_SHAPE_MINKOWSKISUM_MIKOWSKISUM
# define FALLING_SHAPE_MINKOWSKISUM_MIKOWSKISUM

namespace Falling
{
  namespace Shape
  {
    template <typename SA, typename SB>
      class MinkowskiSum
      {
        private:
          const SA& _sa;
          const SB& _sb;

        public:
          MinkowskiSum(const SA& sa, const SB& sb)
            : _sa(sa), _sb(sb)
          { }

          const SA& sa() const
          { return _sa; }

          const SB& sb() const
          { return _sb; }
      };

    template <typename SA, typename SB>
      MinkowskiSum<SA, SB> makeMinkowskiSum(const SA& sa, const SB& sb)
      { return MinkowskiSum<SA, SB>(sa, sb); }

  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_MINKOWSKISUM_MIKOWSKISUM
