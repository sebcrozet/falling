#ifndef FALLING_SHAPE_MINKOWSKISUM_HASCENTEROFMASS
# define FALLING_SHAPE_MINKOWSKISUM_HASCENTEROFMASS

# include <Falling/Shape/MinkowskiSum/MinkowskiSum.hh>
# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename SA, typename SB>
      struct HasCenterOfMass<MinkowskiSum<SA, SB>
                             , ENABLE_IF((HasCenterOfMass<SA>::value && HasCenterOfMass<SB>::value))>
      {
        private:
          using _HCOMA = HasCenterOfMass<SA>;
          using _HCOMB = HasCenterOfMass<SB>;

        public:
          static const bool value = true;

          using CenterOfMassType = typename HasCenterOfMass<SA>::CenterOfMassType;
          static_assert(std::is_same<typename HasCenterOfMass<SA>::CenterOfMassType
              , typename HasCenterOfMass<SB>::CenterOfMassType>::value
              , "Center of mass type missmatch.");

          static_assert_ScalarOp((CenterOfMassType));

          // NOTE: this is not really the center of mass. I really dont know the rigth formula, but
          // it seems not *that* bad.
          static CenterOfMassType centerOfMass(const MinkowskiSum<SA, SB>& cso)
          { return (_HCOMA::centerOfMass(cso.sa()) + _HCOMB::centerOfMass(cso.sb())) / 2.0; }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_MINKOWSKISUM_HASCENTEROFMASS
