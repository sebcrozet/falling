#ifndef FALLING_SHAPE_IMPLICITSHAPE_HASBOUNDINGVOLUMEAABB
# define FALLING_SHAPE_IMPLICITSHAPE_HASBOUNDINGVOLUMEAABB

# include <Falling/BoundingVolume/AABB/AABB.hh>
# include <Falling/Shape/ImplicitShape.hh>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/OrthonormalBasis.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    namespace Static
    {
      template <typename S, typename V>
        struct HasBoundingVolumeTrait<
          S
          , BoundingVolume::AABB<V>
          , typename std::enable_if<Shape::ImplicitShapeTrait<S, V>::value
                                    && Algebra::DimensionTrait<V>::value
                                    && Algebra::DotProdTrait<V>::value
                                    && Algebra::OrthonormalBasis<V>::value, void>::type>
                                    // FIXME: and another constraint to say it must be traversable?
        {
          static const bool value = true;

          static AABB<V> boundingVolume(const S& shape)
          {
            AABB<V> out;

            boundingVolume(shape, out);

            return out;
          }

          static void boundingVolume(const S& shape, AABB<V>& out)
          {
            auto minsp = std::begin(out.mins);
            auto maxsp = std::begin(out.maxs);

            auto basis = OrthonormalBasis<V>::basis();
            for (unsigned i = 0; i < DimensionTrait<V>::dim; ++i)
            {
              const V& dir = basis[i];
              *(minsp++) = dot(dir, supportPoint(shape, -dir));
              *(maxsp++) = dot(dir, supportPoint(shape, dir));
            }

          }
        };
    } // end Static
  } // end BoundingVolume
} // end Falling

#endif // end FALLING_SHAPE_IMPLICITSHAPE_HASBOUNDINGVOLUMEAABB
