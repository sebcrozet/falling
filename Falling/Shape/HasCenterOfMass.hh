#ifndef FALLING_SHAPE_HASCENTEROFMASS
# define FALLING_SHAPE_HASCENTEROFMASS

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/Fixme.hh>

namespace Falling
{
  namespace Shape
  {
# define static_assert_HasCenterOfMass(TYPE)                                        \
         static_assert(Falling::Shape::HasCenterOfMass<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must have a center of mass.")
    template <typename S, typename Enabler = void>
      struct HasCenterOfMass
      {
        static const bool value = false;

        using CenterOfMassType = Metal::FIXME;
        static CenterOfMassType centerOfMass(const S&);
      };

    template <typename S, BIND_CONCEPT(HCOM, (HasCenterOfMass<S>))>
    typename HasCenterOfMass<S>::CenterOfMassType centerOfMass(const S& s)
    { return HCOM::centerOfMass(s); }
  } // end Shape
} // end Falling

#endif // end FALLING_SHAPE_HASCENTEROFMASS
