#ifndef FALLING_SHAPE_BOX
# define FALLING_SHAPE_BOX

# include <Falling/Metal/Empty.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
      class Box : public Metal::Empty
      {
        private:
          V _halfExtents;

        public:
          explicit Box(const V& halfExtents)
            : _halfExtents(halfExtents)
          { }

          const V& halfExtents() const
          { return _halfExtents; }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_BOX
