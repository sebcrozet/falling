#ifndef FALLING_SHAPE_BOX_TRANSFORMABLESHAPE
# define FALLING_SHAPE_BOX_TRANSFORMABLESHAPE

# include <Falling/Shape/Box/Box.hh>
# include <Falling/Shape/TransformableShape.hh>
# include <Falling/Shape/TransformedShape/TransformedShape.hh>

namespace Falling
{
  namespace Shape
  {
    namespace Static
    {
      template <typename V, typename T>
        struct TransformableShapeTrait<Box<V>, T>
        {
          using TransformedShapeType = TransformedShape<Box<V>, T>;

          static const bool value = true;

          static TransformedShapeType* transformShape(const Box<V>&           in
                                                      , const T&              t
                                                      , TransformedShapeType* out)
          {
            if (!out)
              out = new TransformedShape<Box<V>, T>(&in, t);
            else
              out->transform() = t;

            return out;
          }
        };
    }
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_BOX_TRANSFORMABLESHAPE
