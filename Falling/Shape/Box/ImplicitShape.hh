#ifndef FALLING_SHAPE_BOX_IMPLICITSHAPE
# define FALLING_SHAPE_BOX_IMPLICITSHAPE

# include <iterator>
# include <Falling/Shape/Box/Box.hh>
# include <Falling/Shape/ImplicitShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
      struct ImplicitShapeTrait<Box<V>, V>
      {
        static const bool value = true;

        static const V supportPoint(const Box<V>& box, const V& direction)
        {
          V res = direction;

          auto halfExtents = std::begin(box.halfExtents());

          for (double& component : res)
          {
            if (component < 0.0)
              component = -*halfExtents;
            else
              component = *halfExtents;

            ++halfExtents;
          }

          return res;
        }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_BOX_IMPLICITSHAPE
