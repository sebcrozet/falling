#ifndef FALLING_SHAPE_BOX_HASCENTEROFMASS
# define FALLING_SHAPE_BOX_HASCENTEROFMASS

# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Shape/HasCenterOfMass.hh>
# include <Falling/Shape/Box/Box.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
      struct HasCenterOfMass<Box<V>, CHECK_ENABLER((AbelianGroupTrait<V>))>
      {
        static const bool value = true;

        using CenterOfMassType = V;

        static CenterOfMassType centerOfMass(const Box<V>& box)
        { return AbelianGroupTrait<V>::zero; }
      };
  } // end shape
} // end Falling

#endif // FALLING_SHAPE_BOX_HASCENTEROFMASS
