#ifndef FALLING_SHAPE_TRANSFORMABLESHAPE
# define FALLING_SHAPE_TRANSFORMABLESHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/DynamicVal.hh>
# include <Falling/Metal/TypeToId.hh>
# include <Falling/Metal/Inherit.hh>
# include <Falling/Metal/StaticDynamicBridge.hh>
# include <Falling/Metal/Fixme.hh>

namespace Falling
{
  namespace Shape
  {
    namespace Static
    {
# define static_assert_TransformableShape(TYPES, TYPET)                                         \
         static_assert(Falling::Shape::Static::TransformableShape<                              \
             typename RMPAR(TYPES)                                                              \
             , typename RMPAR(TYPET)>::value                                                    \
             , "Type parameter " #TYPES " must be a shape transformable by transforms of type " \
               #TYPET ".")
      /**
       * \brief Concept to be specialized for transformable objects.
       * A transformable object is an object to which a transform can be applied.
       * \param S The type of the transformable object.
       * \param T The type of the transformation.
       * \param Enabler Extra parameter allowing more powerful conditional specializations.
       */
      template <typename S, typename T, typename Enabler = void>
        struct TransformableShapeTrait
        {
          /**
           * \brief Indicates whether this concept is enabled for its parameter.
           * This must be defined as true by specializer. Check this value to determine whether
           * this concept has been specialized.
           */
          static const bool value = false;

          /**
           * \brief Type of the object obtained after the transformation.
           * This must be defined by the specializer.
           */
          using TransformedShapeType = Metal::FIXME;

          /**
           * \anchor _transformShape
           * \brief Transforms an object.
           * This must be defined by the specializer.
           * \param[in] s The shape to be transformed. 
           * \param[in] t The transformation.
           * \param[out] ts The transformed shape will be written on this pointer. If the \a
           * nullptr pointer is passed, a new transformed shape is allocated.
           * \return Returns the newly allocated transformed shape or \a ts if it was not \a
           * nullptr.
           */
          static TransformedShapeType* transfomShape(const S&                s
                                                     , const T&              t
                                                     , TransformedShapeType* ts);
        };

      /**
       * \brief Same as \ref _transformShape "transformShape" but with automatic concept deduction.
       * @see TransformableShapeTrait
       */
      template <typename S, typename T, BIND_CONCEPT(TST, (TransformableShapeTrait<S, T>))>
        typename TST::TransformedShapeType* transformShape(
            const S&                              s
            , const T&                            t
            , typename TST::TransformedShapeType* ts)
        {
          return TST::transformShape(s, t, ts);
        }
    } // end Static

    namespace Dynamic
    {
      template <typename T>
        struct TransformableShape
        {
          virtual Metal::DynamicVal&
            transformShape(const Metal::ConstDynamicVal&, const T&, Metal::DynamicVal& out) const = 0;
        };

      template <typename S, typename T, BIND_CONCEPT(TST, (Static::TransformableShapeTrait<S, T>))>
        struct TransformableShapeTrait : virtual TransformableShape<T>
        {
          virtual Metal::DynamicVal&
            transformShape(const Metal::ConstDynamicVal& in, const T& t, Metal::DynamicVal& out) const
            {
              out.setVal(Static::transformShape(
                         *in.valueAs<S>()
                         , t
                         , out.valueAs<typename TST::TransformedShapeType>()));

              return out;
            }
        };
    } // end Dynamic

    namespace Static
    {
      template <typename K, typename T>
        struct TransformableShapeTrait
        <K, T
         , typename std::enable_if<std::is_base_of<Dynamic::TransformableShape<T>, K>::value, void>::type>
      {
        static const bool value = true;

        using TransformedShapeType = Metal::Inherit<>;

        static Metal::Inherit<>* transformShape(const K& in, const T& t, Metal::Inherit<>* out)
        {
          assert(out && "Out must not be null.");
          in.transformShape(in.constValue(), t, out->value());

          return out;
        }
      };
    } // end Static
  } // end Shape

  namespace Metal
  {
    // tie the knot
    template <typename S, typename T>
      struct DynamicBridge<Shape::Dynamic::TransformableShapeTrait<S, T>>
      {
        using type = Shape::Dynamic::TransformableShape<T>;
      };

    template <typename T>
      struct StaticBridge<Shape::Dynamic::TransformableShape<T>>
      {
        template <typename S>
          using type = Shape::Dynamic::TransformableShapeTrait<S, T>;
      };

    // register identifier
    template <typename S, typename T>
      struct TypeToId<Shape::Dynamic::TransformableShapeTrait<S, T>> : register_id<int, 10000>
      { };

    template <typename T>
      struct TypeToId<Shape::Dynamic::TransformableShape<T>> : register_id<int, 1000>
      { };
  } // end Metal
} // end Falling

#endif // FALLING_SHAPE_TRANSFORMABLESHAPE
