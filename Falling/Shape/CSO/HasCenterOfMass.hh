#ifndef FALLING_SHAPE_CSO_HASCENTEROFMASS
# define FALLING_SHAPE_CSO_HASCENTEROFMASS

# include <Falling/Shape/CSO/CSO.hh>
# include <Falling/Shape/MinkowskiSum/HasCenterOfMass.hh>
# include <Falling/Shape/Reflexion/HasCenterOfMass.hh>
# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename SA, typename SB>
    struct HasCenterOfMass<CSO<SA, SB>
                           , CHECK_ENABLER((HasCenterOfMass<MinkowskiSum<SA, Reflexion<SB>>>))>
    {
      private:
        using _HasCenterOfMass= HasCenterOfMass<MinkowskiSum<SA, Reflexion<SB>>>;

      public:
        static const bool value = true;

        using CenterOfMassType =
          typename HasCenterOfMass<MinkowskiSum<SA, Reflexion<SB>>>::CenterOfMassType;

        static CenterOfMassType centerOfMass(const CSO<SA, SB>& cso)
        { return _HasCenterOfMass::centerOfMass(cso.s()); }
    };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_CSO_HASCENTEROFMASS
