#ifndef FALLING_SHAPE_CSO
# define FALLING_SHAPE_CSO

# include <Falling/Shape/Reflexion/Reflexion.hh>
# include <Falling/Shape/MinkowskiSum/MinkowskiSum.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename SA, typename SB>
    class CSO
    {
      private:
        Reflexion<SB>                   _reflexion;
        MinkowskiSum<SA, Reflexion<SB>> _s;

      public:
        CSO(const SA& sa, const SB& sb)
          : _reflexion(makeReflexion(sb))
            , _s(makeMinkowskiSum(sa, _reflexion))
        { }

        const MinkowskiSum<SA, Reflexion<SB>>& s() const
        { return _s; }
    };

    template <typename SA, typename SB>
      CSO<SA, SB> makeCSO(const SA& sa, const SB& sb)
      { return CSO<SA, SB>(sa, sb); }
  }
}

#endif // end FALLING_SHAPE_CSO
