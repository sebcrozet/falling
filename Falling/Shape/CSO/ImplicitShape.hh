#ifndef FALLING_SHAPE_CSO_IMPLICITSHAPE
# define FALLING_SHAPE_CSO_IMPLICITSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Shape/CSO/CSO.hh>
# include <Falling/Shape/Reflexion/ImplicitShape.hh>
# include <Falling/Shape/MinkowskiSum/ImplicitShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename SA, typename SB, typename V>
      struct ImplicitShapeTrait<
        CSO<SA, SB>, V, CHECK_ENABLER((ImplicitShapeTrait<MinkowskiSum<SA, Reflexion<SB>>, V>))>
    {
      private:
        using _ImplicitShapeTrait = ImplicitShapeTrait<MinkowskiSum<SA, Reflexion<SB>>, V>;

      public:
        static const bool value = true;

        static const V supportPoint(const CSO<SA, SB>& cso, const V& direction)
        {
          using namespace Algebra;

          return _ImplicitShapeTrait::supportPoint(cso.s(), direction);
        }
    };
  } // end Shape

} // end Falling

#endif // FALLING_SHAPE_CSO_IMPLICITSHAPE
