#ifndef FALLING_SHAPE_BALL_TRANSFORMABLESHAPE
# define FALLING_SHAPE_BALL_TRANSFORMABLESHAPE

# include <Falling/Shape/TransformableShape.hh>
# include <Falling/Shape/Ball/Ball.hh>

namespace Falling
{
  namespace Shape
  {
    namespace Static
    {
      template<typename V, typename T>
        struct TransformableShapeTrait<Ball<V>, T>
        {
          using TransformedShapeType = Ball<V>;

          static const bool value = true;

          static Ball<V>* transformShape(const Ball<V>& in, const T& t, Ball<V>* out)
          {
            if (!out)
              out = new Ball<V>(in.radius());

            out->setCenter(TransformTrait<T, V>::transform(t, in.center()));

            return out;
          }
        };
    } // end Static
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_BALL_TRANSFORMABLESHAPE
