#ifndef FALLING_SHAPE_BALL_HASCENTEROFMASS
# define FALLING_SHAPE_BALL_HASCENTEROFMASS

# include <Falling/Shape/Ball/Ball.hh>
# include <Falling/Shape/HasCenterOfMass.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
    struct HasCenterOfMass<Ball<V>>
    {
      static const bool value = true;

      using CenterOfMassType = V;

      static CenterOfMassType centerOfMass(const Ball<V>& b)
      { return b.center(); }
    };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_BALL_HASCENTEROFMASS
