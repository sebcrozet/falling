#ifndef FALLING_SHAPE_BALL_HASBOUNDINGVOLUMEAABB
# define FALLING_SHAPE_BALL_HASBOUNDINGVOLUMEAABB

# include <Falling/Algebra/Dimension.hh>
# include <Falling/BoundingVolume/HasBoundingVolume.hh>
# include <Falling/BoundingVolume/AABB/AABB.hh>
# include <Falling/Shape/Ball/Ball.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    namespace Static
    {
      template <typename V>
        struct HasBoundingVolumeTrait<Shape::Ball<V>, AABB<V>
        // We must add those constraints to avoid specialization overlapping with ImplicitShape
        // This makes this specialization more specific than the general one for implicit shapes.
        , typename std::enable_if<Shape::ImplicitShapeTrait<Shape::Ball<V>, V>::value
                                  && Algebra::DimensionTrait<V>::value
                                  && Algebra::DotProdTrait<V>::value
                                  && Algebra::OrthonormalBasis<V>::value, void>::type>
        {
          static const bool value = true;

          static AABB<V> boundingVolume(const Shape::Ball<V>& b)
          {
            AABB<V> res;

            boundingVolume(b, res);

            return res;
          }

          static void boundingVolume(const Shape::Ball<V>& b, AABB<V>& out)
          {
            out.mins = b.center() - b.radius();
            out.maxs = b.center() + b.radius();

            out.invariant();
          }
        };
    }
  } // end Shape
}// end Falling

#endif // FALLING_SHAPE_BALL_HASBOUNDINGVOLUMEAABB
