#ifndef FALLING_SHAPE_BALL_BALL
# define FALLING_SHAPE_BALL_BALL

# include <iostream>

// FIXME: cleanup unused includes
# include <Falling/Metal/Empty.hh>
# include <Falling/Algebra/Transform.hh>
# include <Falling/Algebra/ScalarOp.hh>
# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/DotProd.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
      class Ball : public Metal::Empty
      {
        private:
          double _radius;
          V      _center;

        public:
          explicit Ball(double radius)
            : _radius(radius)
          { }

          Ball(double radius, const V& center)
            : _radius(radius), _center(center)
          { }

          double   radius() const
          { return  _radius; }

          const V& center() const
          { return  _center; }

          void setCenter(const V& c)
          { _center = c; }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_BALL_BALL
