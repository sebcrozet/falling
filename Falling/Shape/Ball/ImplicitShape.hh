#ifndef FALLING_SHAPE_BALL_IMPLICITSHAPE
# define FALLING_SHAPE_BALL_IMPLICITSHAPE

# include <Falling/Shape/Ball/Ball.hh>
# include <Falling/Shape/ImplicitShape.hh>

namespace Falling
{
  namespace Shape
  {
    template<typename V>
      struct ImplicitShapeTrait<
        Ball<V>
        , V
        , typename std::enable_if<Falling::Algebra::DotProdTrait<V>::value
                                  && Falling::Algebra::ScalarOpTrait<V>::value
                                  && Falling::Algebra::AbelianGroupTrait<V>::value
                                  , void>::type>
      {
        static const bool value = true;

        static const V supportPoint(const Ball<V>& ball, const V& direction)
        {
          using namespace Algebra;

          return ball.center() + direction * ball.radius() / len(direction);
        }
      };
  } // end Shape
} // end Falling


#endif // FALLING_SHAPE_BALL_IMPLICITSHAPE
