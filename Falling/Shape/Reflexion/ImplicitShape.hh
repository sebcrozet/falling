#ifndef FALLING_SHAPE_REFLEXION_IMPLICITSHAPE
# define FALLING_SHAPE_REFLEXION_IMPLICITSHAPE

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Shape/Reflexion/Reflexion.hh>
# include <Falling/Shape/ImplicitShape.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S, typename V>
      struct ImplicitShapeTrait<Reflexion<S>, V, CHECK_ENABLER((ImplicitShapeTrait<S, V>))>
    {
      static const bool value = true;

      static const V supportPoint(const Reflexion<S>& ref, const V& direction)
      {
        using namespace Algebra;

        return -ImplicitShapeTrait<S, V>::supportPoint(ref.s(), -direction);
      }
    };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_REFLEXION_IMPLICITSHAPE
