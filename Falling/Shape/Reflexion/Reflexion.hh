#ifndef FALLING_SHAPE_REFLEXION_REFLEXION
# define FALLING_SHAPE_REFLEXION_REFLEXION

namespace Falling
{
  namespace Shape
  {
    template <typename S>
      class Reflexion
      {
        private:
          const S& _s;

        public:
          explicit Reflexion(const S& s)
            : _s(s)
          { }

          const S& s() const
          { return _s; }
      };

    template <typename S>
      Reflexion<S> makeReflexion(const S& s)
      { return Reflexion<S>(s); }
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_REFLEXION_REFLEXION
