#ifndef FALLING_SHAPE_REFLEXION_HASCENTEROFMASS
# define FALLING_SHAPE_REFLEXION_HASCENTEROFMASS

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename S>
    struct HasCenterOfMass<Reflexion<S>, CHECK_ENABLER((HasCenterOfMass<S>))>
    {
      static const bool value = true;

      using CenterOfMassType = typename HasCenterOfMass<S>::CenterOfMassType;

      static CenterOfMassType centerOfMass(const Reflexion<S>& ref)
      { return HasCenterOfMass<S>::centerOfMass(ref.s()); }
    };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_REFLEXION_HASCENTEROFMASS
