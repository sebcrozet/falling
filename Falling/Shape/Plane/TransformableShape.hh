#ifndef FALLING_SHAPE_PLANE_TRANSFORMABLESHAPE
# define FALLING_SHAPE_PLANE_TRANSFORMABLESHAPE

# include <Falling/Shape/TransformableShape.hh>
# include <Falling/Shape/Plane/Plane.hh>

namespace Falling
{
  namespace Shape
  {

    namespace Static
    {
      template <typename V, typename T>
        struct TransformableShapeTrait<Plane<V>, T>
        {
          using TransformedShapeType = Plane<V>;

          static const bool value = true;

          static Plane<V>* transformShape(const Plane<V>& in, const T& t, Plane<V>* out)
          {
            if (!out)
              out = new Plane<V>(in.normal(), in.center());

            out->setCenter(Algebra::TransformTrait<T, V>::transform(t, out->center()));
            out->setNormal(deltaTransform(t, out->normal()));

            return out;
          }
        };
    } // end Static
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_PLANE_TRANSFORMABLESHAPE
