#ifndef FALLING_SHAPE_PLANE_HASBOUNDINGVOLUMEAABB
# define FALLING_SHAPE_PLANE_HASBOUNDINGVOLUMEAABB

# include <limits>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/BoundingVolume/HasBoundingVolume.hh>
# include <Falling/BoundingVolume/AABB/AABB.hh>
# include <Falling/Shape/Plane/Plane.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    namespace Static
    {
      template <typename V>
        struct HasBoundingVolumeTrait<Shape::Plane<V>, AABB<V>>
        {
          static const unsigned int dim = Algebra::DimensionTrait<V>::dim;

          static const bool value = true;

          static AABB<V> boundingVolume(const Shape::Plane<V>& b)
          {
            AABB<V> res;

            boundingVolume(b, res);

            return res;
          }

          static void boundingVolume(const Shape::Plane<V>& b, AABB<V>& out)
          {
            out.mins = AbelianGroupTrait<V>::zero + std::numeric_limits<double>::lowest() / 2.0;
            out.maxs = AbelianGroupTrait<V>::zero + std::numeric_limits<double>::max() / 2.0;

            out.invariant();
          }
        };
    }
  } // end Shape
}// end Falling

#endif // FALLING_SHAPE_PLANE_HASBOUNDINGVOLUMEAABB
