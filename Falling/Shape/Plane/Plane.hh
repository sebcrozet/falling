#ifndef FALLING_SHAPE_PLANE_PLANE
# define FALLING_SHAPE_PLANE_PLANE

# include <Falling/Algebra/Transform.hh>
# include <Falling/Metal/Empty.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
      class Plane : public Metal::Empty
      {
        private:
          V _normal;
          V _center;

        public:
          explicit Plane(const V& normal)
            : _normal(normal)
          { }

          Plane(const V& normal, const V& center)
            : _normal(normal), _center(center)
          { }

          const V& normal() const
          { return _normal; }

          const V& center() const
          { return _center; }

          void setNormal(const V& n)
          { _normal = n; }

          void setCenter(const V& c)
          { _center = c; }
      };
  } // end Shape
} // end Falling

#endif // FALLING_SHAPE_PLANE_PLANE
