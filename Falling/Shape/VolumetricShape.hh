#ifndef FALLING_SHAPE_VOLUMETRICSHAPE
# define FALLING_SHAPE_VOLUMETRICSHAPE

# include <Falling/Metal/Fixme.hh>

namespace Falling
{
  namespace Shape
  {
  # define static_assert_VolumetricShape(TYPE)                                      \
         static_assert(Falling::Shape::VolumetricShape<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be a shape with volume and inertia informations.")
    template <typename VS, typename Enabler = void>
      struct VolumetricShapeTrait
      {
        static const bool value = false;

        using InertiaTensorType = Metal::FIXME;

        static double            volume(const VS&);
        static InertiaTensorType inertiaTensor(const VS&, double);
      };

    template <typename S, BIND_CONCEPT(VS, (VolumetricShapeTrait<S>))>
    double volume(const S& s)
    { return VS::volume(s); }

    template <typename S, BIND_CONCEPT(VS, (VolumetricShapeTrait<S>))>
    typename VS::InertiaTensorType inertiaTensor(const S& s, double m)
    { return VS::inertiaTensor(s, m); }
  } // end Shape
} // end Falling

#endif // end FALLING_SHAPE_VOLUMETRICSHAPE
