#ifndef FALLING_BOUNDINGVOLUME_BOUNDINGVOLUME
# define FALLING_BOUNDINGVOLUME_BOUNDINGVOLUME

# include <type_traits>
# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace BoundingVolume
  {
# define static_assert_BoundingVolume(TYPE)                                                      \
         static_assert(Falling::BoundingVolume::BoundingVolumeTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be a bounding volume.")
    template <typename BV, typename Enabler = void>
      struct BoundingVolumeTrait
      {
        static const bool value = false;

        static bool intersect(const BV&, const BV&);
        static bool contain(const BV&, const BV&);
        static BV   merge(const BV&, const BV&);
        static void merge(const BV&, const BV&, BV&);
        static void merge(const BV&, BV&);
    };

    template <typename BV, BIND_CONCEPT(BVT, (BoundingVolumeTrait<BV>))>
      bool intersect(const BV& v1, const BV& v2)
      { return BVT::intersect(v1, v2); }

    template <typename BV, BIND_CONCEPT(BVT, (BoundingVolumeTrait<BV>))>
      bool contain(const BV& v1, const BV& v2)
      { return BVT::contain(v1, v2); }

    template <typename BV, BIND_CONCEPT(BVT, (BoundingVolumeTrait<BV>))>
      BV merge(const BV& v1, const BV& v2)
      { return BVT::merge(v1, v2); }

    template <typename BV, BIND_CONCEPT(BVT, (BoundingVolumeTrait<BV>))>
      void merge(const BV& v1, const BV& v2, BV& out)
      { return BVT::merge(v1, v2, out); }

    template <typename BV, BIND_CONCEPT(BVT, (BoundingVolumeTrait<BV>))>
      void merge(const BV& v1, BV& v2)
      { return BVT::merge(v1, v2); }
  } // end BoundingVolume
} // end Falling

#endif // FALLING_BOUNDINGVOLUME_BOUNDINGVOLUME
