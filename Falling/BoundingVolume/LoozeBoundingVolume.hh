#ifndef FALLING_COLLISION_DETECTION_BROADPHASE_LOOZEBOUNDINGVOLUME
# define FALLING_COLLISION_DETECTION_BROADPHASE_LOOZEBOUNDINGVOLUME

# include <Falling/Metal/EnableIf.hh>
# include <Falling/BoundingVolume/BoundingVolume.hh>

namespace Falling
{
  namespace BoundingVolume
  {
# define static_assert_LoozeBoundingVolume(TYPE)                                                      \
         static_assert(Falling::BoundingVolume::LoozeBoundingVolumeTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must a looze bounding volume.")
    template <typename LBV, typename Enabler = void>
      struct LoozeBoundingVolumeTrait
      {
        static const bool value = false;

        using AsBoundingVolume = BoundingVolumeTrait<LBV>;
        static LBV  loozen(double margin, const LBV& vol);
        static void loozen(double margin, LBV& vol);
        static void loozen(double margin, const LBV& vol, LBV& out);
      };

    template <typename LBV, BIND_CONCEPT(LBVT, (LoozeBoundingVolumeTrait<LBV>))>
      LBV loozen(double margin, const LBV& vol)
      { return LBVT::loozen(margin, vol); }

    template <typename LBV, BIND_CONCEPT(LBVT, (LoozeBoundingVolumeTrait<LBV>))>
      void loozen(double margin, LBV& vol)
      { LBVT::loozen(margin, vol); }

    template <typename LBV, BIND_CONCEPT(LBVT, (LoozeBoundingVolumeTrait<LBV>))>
    void loozen(double margin, const LBV& vol, LBV& out)
    { LBVT::loozen(margin, vol, out); }
  } // end BoundingVolume
} // end Falling

#endif // FALLING_COLLISION_DETECTION_BROADPHASE_LOOZEBOUNDINGVOLUME
