#ifndef FALLING_BOUNDINGVOLUME_HASBOUNDINGVOLUME
# define FALLING_BOUNDINGVOLUME_HASBOUNDINGVOLUME

# include <type_traits>
# include <Falling/Metal/EnableIf.hh>
# include <Falling/Metal/DynamicVal.hh>
# include <Falling/Metal/TypeToId.hh>
# include <Falling/Metal/StaticDynamicBridge.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    namespace Static
    {
# define static_assert_HasBoundingVolume(TYPERB, TYPEBV)                                     \
         static_assert(Falling::BoundingVolume::Static::HasBoundingVolumeTrait<              \
             typename RMPAR(TYPERB), typename RMPAR(TYPEBV)>::value                          \
                       , "Type parameter " #TYPERB " must have a bounding volume of type " #TYPEBV ".")
      /**
       * \brief Concept to be specialized for objects having a bounding volume.
       * A bounding volume is a geometric simplification of a complicated geometry. This is mainly
       * used to fasten geometric queries on complicated geometries. A bounding volume is typically
       * an axis-aligned bounding box, a bounding sphere, an oriented bounding box, etc.
       * This concept has dynamic dispatch capabilities.
       * \param RB The type of the object having a bounding volume.
       * \param BV The type of the bounding volume.
       * \param Enabler Extra parameter allowing more powerful conditional specializations.
       */
      template <typename RB, typename BV, typename Enabler = void>
        struct HasBoundingVolumeTrait
        {
          /**
           * \brief Indicates whether this concept is enabled for its parameter.
           * This must be defined as true by specializer. Check this value to determine whether
           * this concept has been specialized.
           */
          static const bool value = false;

          /**
           * \anchor _boundingVolume
           * \brief Computes the bounding volume of an object.
           * This must be defined by the specializer.
           * \param[in]  b Object from which the bounding volume must be computed.
           * \return A bounding volume.
           */
          static BV   boundingVolume(const RB& b);

          /**
           * \anchor _boundingVolume_inplace
           * \brief In-place computation of the bounding volume of an object. It is not guarenteed
           * that the bounding volume has already been properly initialized before.
           * This must be defined by the specializer.
           * \param[in] b Object from which the bounding volume must be computed.
           * \param[out] out The bounding volume will be written on this reference.
           */
          static void boundingVolume(const RB& b, BV& out);
        };

      /**
       * \brief Same as \ref _boundingVolume_inplace "boundingVolume" but with automatic concept
       * deduction.
       * @see HasBoundingVolumeTrait
       */
      template <typename RB, typename BV, BIND_CONCEPT(HBVT, (HasBoundingVolumeTrait<RB, BV>))>
        void boundingVolume(const RB& b, BV& bv)
        { HBVT::boundingVolume(b, bv); }
    } // end Static

    namespace Dynamic
    {
      template <typename BV>
      struct HasBoundingVolume
      {
        virtual BV   boundingVolume(const Metal::ConstDynamicVal&) const = 0;
        virtual void boundingVolume(const Metal::ConstDynamicVal&, BV&) const = 0;
      };

      template <typename RB
                , typename BV
                , BIND_CONCEPT(HBVT, (Static::HasBoundingVolumeTrait<RB, BV>))>
        struct HasBoundingVolumeTrait : virtual HasBoundingVolume<BV>
        {
          virtual BV boundingVolume(const Metal::ConstDynamicVal& rb) const
          {
            return HBVT::boundingVolume(*rb.valueAs<RB>());
          }

          virtual void boundingVolume(const Metal::ConstDynamicVal& rb
                                      , BV&                      out) const
          {
            Static::boundingVolume(*rb.valueAs<RB>(), out);
          }
        };
    } // end Dynamic

    namespace Static
    {
      template <typename RB, typename BV>
        struct HasBoundingVolumeTrait
        <RB, BV, CHECK_ENABLER((std::is_base_of<typename Dynamic::HasBoundingVolume<BV>, RB>))>
      {
        static const bool value = true;

        static BV   boundingVolume(const RB& rb)
        {
          return rb.boundingVolume(rb.constValue());
        }

        static void boundingVolume(const RB& rb, BV& bv)
        {
          rb.boundingVolume(rb.constValue(), bv);
        }
      };
    } // end Static
  } // end BoundingVolume

  namespace Metal
  {
    // tie the knot
    template <typename RB, typename BV>
      struct DynamicBridge<BoundingVolume::Dynamic::HasBoundingVolumeTrait<RB, BV>>
      {
        using type = BoundingVolume::Dynamic::HasBoundingVolume<BV>;
      };

    template <typename BV>
      struct StaticBridge<BoundingVolume::Dynamic::HasBoundingVolume<BV>>
      {
        template <typename RB>
          using type = BoundingVolume::Dynamic::HasBoundingVolumeTrait<RB, BV>;
      };


    template <typename RB, typename BV>
      struct TypeToId<BoundingVolume::Dynamic::HasBoundingVolumeTrait<RB, BV>> : register_id<int, 30000>
      { };

    template <typename BV>
      struct TypeToId<BoundingVolume::Dynamic::HasBoundingVolume<BV>> : register_id<int, 3000>
      { };
  } // end Metal
} // end Falling

#endif // FALLING_BOUNDINGVOLUME_HASBOUNDINGVOLUME
