#ifndef FALLING_BOUNDINGVOLUME_AABB_HASCENTEROFMASS
# define FALLING_BOUNDINGVOLUME_AABB_HASCENTEROFMASS

# include <Falling/BoundingVolume/AABB/AABB.hh>
# include <Falling/Shape/HasCenterOfMass.hh>
# include <Falling/Algebra/Vect.hh>

namespace Falling
{
  namespace Shape
  {
    template <typename V>
      struct HasCenterOfMass<BoundingVolume::AABB<V>>
      {
        static const bool value = true;
        using CenterOfMassType  = V;

        static CenterOfMassType centerOfMass(const BoundingVolume::AABB<V>& aabb)
        {
          aabb.invariant();

          // FIXME: impose AbelianGroupTrait et ScalarOpTrait
          return (aabb.mins + aabb.maxs) / 2.0;
        }
      };
  } // end Shape
} // end Falling

#endif // end FALLING_BOUNDINGVOLUME_AABB_HASCENTEROFMASS
