#ifndef FALLING_BOUNDINGVOLUME_AABB_AABB
# define FALLING_BOUNDINGVOLUME_AABB_AABB

# include <cassert>
# include <algorithm>

# include <Falling/Algebra/AbelianGroup.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    using namespace Algebra;

    template <typename V>
      struct AABB
      {
        static_assert_AbelianGroup((V));

        V mins = AbelianGroupTrait<V>::zero;
        V maxs = AbelianGroupTrait<V>::zero;

        void invariant() const
        {
          auto minsp = std::begin(mins);
#ifndef NDEBUG
          auto maxsp = std::begin(maxs);
#endif
          auto endp  = std::end(mins);

          while (minsp != endp)
            assert(*(minsp++) <= *(maxsp++) && "Invalid bounds.");
        }
      };

    template <typename V>
      std::ostream& operator<<(std::ostream& stream, const AABB<V>& aabb)
      {
        stream << "AABB { mins = " << aabb.mins << ", maxs = " << aabb.maxs << " }";
        return stream;
      }
  } // end BoundingVolume
} // end Falling

#endif // FALLING_BOUNDINGVOLUME_AABB_AABB
