#ifndef FALLING_BOUNDINGVOLUME_AABB_BOUNDINGVOLUME
# define FALLING_BOUNDINGVOLUME_AABB_BOUNDINGVOLUME

# include <Falling/BoundingVolume/AABB/AABB.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    template <typename V>
      struct BoundingVolumeTrait<AABB<V>>
      {
        static const bool value = true;

        static bool intersect(const AABB<V>& b1, const AABB<V>& b2)
        {
          b1.invariant();
          b2.invariant();

          auto minsp1 = std::begin(b1.mins);
          auto maxsp1 = std::begin(b1.maxs);
          auto minsp2 = std::begin(b2.mins);
          auto maxsp2 = std::begin(b2.maxs);

          auto endp   = std::end(b1.mins);

          while (minsp1 != endp)
          {
            if (*(minsp1++) > *(maxsp2++) || *(minsp2++) > *(maxsp1++))
              return false;
          }

          return true; // they intersect on every dimensions
        }

        static bool contain(const AABB<V>& b1, const AABB<V>& b2)
        {
          b1.invariant();
          b2.invariant();

          auto minsp1 = std::begin(b1.mins);
          auto maxsp1 = std::begin(b1.maxs);
          auto minsp2 = std::begin(b2.mins);
          auto maxsp2 = std::begin(b2.maxs);

          auto endp   = std::end(b1.mins);

          while (minsp1 != endp)
          {
            if (*(minsp2++) < *(minsp1++) || *(maxsp2++) > *(maxsp1++))
              return false;
          }

          return true;
        }

        static void merge(const AABB<V>& b1, const AABB<V>& b2, AABB<V>& out)
        {
          b1.invariant();
          b2.invariant();

          auto minsp1 = std::begin(b1.mins);
          auto maxsp1 = std::begin(b1.maxs);
          auto minsp2 = std::begin(b2.mins);
          auto maxsp2 = std::begin(b2.maxs);
          auto minspo = std::begin(out.mins);
          auto maxspo = std::begin(out.maxs);

          auto endp   = std::end(out.mins);

          while (minspo != endp)
          {
            *(minspo++) = std::min(*(minsp1++), *(minsp2++));
            *(maxspo++) = std::max(*(maxsp1++), *(maxsp2++));
          }

          out.invariant();
        }

        static AABB<V> merge(const AABB<V>& b1, const AABB<V>& b2)
        {
          AABB<V> result;
          merge(b1, b2, result);
          return result;
        }

        static void merge(const AABB<V>& b1, AABB<V>& b2)
        { merge(b1, b2, b2); }
      };
  } // end BoundingVolume
} // end Falling

#endif // FALLING_BOUNDINGVOLUME_AABB_BOUNDINGVOLUME
