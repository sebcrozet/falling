#ifndef FALLING_BOUNDINGVOLUME_AABB_LOOZEBOUNDINGVOLUME
# define FALLING_BOUNDINGVOLUME_AABB_LOOZEBOUNDINGVOLUME

# include <Falling/BoundingVolume/AABB/AABB.hh>

namespace Falling
{
  namespace BoundingVolume
  {
    template <typename V>
      struct LoozeBoundingVolumeTrait<AABB<V>>
      {
        static const bool value = true;

        using AsBoundingVolume = BoundingVolumeTrait<AABB<V>>;

        static void loozen(double margin, const AABB<V>& b, AABB<V>& out)
        {
          b.invariant();

          out.mins = b.mins - margin;
          out.maxs = b.maxs + margin;

          out.invariant();
        }

        static AABB<V> loozen(double margin, const AABB<V>& b)
        {
          b.invariant();

          AABB<V> res;
          loozen(margin, b, res);
          return res;
        }

        static void loozen(double margin, AABB<V>& b)
        {
          loozen(margin, b, b);
        }
      };
  } // end BoundingVolume
} // end Falling

#endif // FALLING_BOUNDINGVOLUME_AABB_LOOZEBOUNDINGVOLUME
