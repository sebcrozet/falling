#ifndef FALLING_CONSTRAINT_SOLVER_PROJECTEDGAUSSSEIDELSOLVER
# define FALLING_CONSTRAINT_SOLVER_PROJECTEDGAUSSSEIDELSOLVER

namespace Falling
{
  namespace Constraint
  {
    namespace Solver
    {

      template <unsigned int lvPlusAvDim>
        double* projectedGaussSeidelSolve(
            double*      J,
            double*      MJ,
            double*      b,
            double*      lambda,
            double*      bounds,
            int*         idx,
            unsigned int numEquations,
            unsigned int numBodies,
            unsigned int iterationsNumber)
        {

          double* d        = new double[numEquations]; // JMJ matrix’s diagonal.
          double* MJLambda = new double[lvPlusAvDim * numBodies]; // MJLambda = MJ * lambda

          /*
           * compute MJLambda = MJ * lambda
           */
          std::fill(MJLambda, MJLambda + lvPlusAvDim * numBodies, 0.0);

          /*
           * warm start
           */
          for (unsigned int i = 0; i < numEquations; ++i) 
          {
            int b1 = idx[i * 2];
            int b2 = idx[i * 2 + 1];

            if (b1 >= 0)
            {
              for (unsigned j = 0; j < lvPlusAvDim; ++j)
                MJLambda[b1 * lvPlusAvDim + j] += MJ[i * lvPlusAvDim * 2 + j] * lambda[i];
            }

            if (b2 >= 0)
            {
              for (unsigned j = 0; j < lvPlusAvDim; ++j)
                MJLambda[b2 * lvPlusAvDim + j] += MJ[i * lvPlusAvDim * 2 + lvPlusAvDim + j] * lambda[i];
            }
          }

          /*
           * init JB's diagonal
           */
          for (unsigned i = 0; i < numEquations; ++i)
          {
            /*
             * J and MJ^t have the same sparcity
             */
            d[i] = 0.0;

            for (unsigned k = 0; k < lvPlusAvDim; ++k)
            {
              d[i] += J[i * lvPlusAvDim * 2 + k] * MJ[i * lvPlusAvDim * 2 + k]
                      + J[i * lvPlusAvDim * 2 + k + lvPlusAvDim] * MJ[lvPlusAvDim + i * lvPlusAvDim * 2 + k];
            }
          }

          /*
           * solve the system
           */
          for (unsigned iter = 0; iter < iterationsNumber; ++iter)
          {
            for (unsigned i = 0; i < numEquations; ++i)
            {
              int b1 = idx[i * 2 + 0] * lvPlusAvDim;
              int b2 = idx[i * 2 + 1] * lvPlusAvDim;

              double d_lambda_i = b[i];

              if (b1 >= 0)
              {
                for (unsigned k = 0; k < lvPlusAvDim; ++k)
                  d_lambda_i -= J[2 * lvPlusAvDim * i + k] * MJLambda[b1 + k];
              }

              if (b2 >= 0)
              {
                for (unsigned k = 0; k < lvPlusAvDim; ++k)
                  d_lambda_i -= J[2 * lvPlusAvDim * i + lvPlusAvDim + k] * MJLambda[b2 + k];
              }

              d_lambda_i /= d[i];

              /*
               * clamp the value such that: lambda- <= lambda <= lambda+
               * (this is the ``projected'' flavour of Gauss-Seidel
               */
              double lambda_i_0 = lambda[i];

              lambda[i] =
                std::max(bounds[i * 2],
                    std::min(lambda_i_0 + d_lambda_i, bounds[i * 2 + 1]));

              d_lambda_i = lambda[i] - lambda_i_0;

              if (b1 >= 0)
              {
                for (unsigned j = 0; j < lvPlusAvDim; ++j)
                  MJLambda[b1 + j] += d_lambda_i * MJ[i * lvPlusAvDim * 2 + j];
              }

              if (b2 >= 0)
              {
                for (unsigned j = 0; j < lvPlusAvDim; ++j)
                  MJLambda[b2 + j] += d_lambda_i * MJ[i * lvPlusAvDim * 2 + lvPlusAvDim + j];
              }
            }
          }

          delete[] d;

          return MJLambda;
        }
    }; // end Solver
  }; // end Constraint
}; // end Falling

#endif // FALLING_CONSTRAINT_SOLVER_PROJECTEDGAUSSSEIDELSOLVER
