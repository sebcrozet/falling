#ifndef FALLING_CONSTRAINT_SOLVER_INDEXPROXY
# define FALLING_CONSTRAINT_SOLVER_INDEXPROXY

namespace Falling
{
  namespace Constraint
  {
    namespace Solver
    {
      struct IndexProxy
      { int index; };
    } // end Solver
  } // end Constraint
} // end Falling

#endif // end FALLING_CONSTRAINT_SOLVER_INDEXPROXY
