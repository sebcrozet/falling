#ifndef FALLING_CONSTRAINT_SOLVER_ACCUMULATEDIMPULSECONSTRAINTSOLVER
# define FALLING_CONSTRAINT_SOLVER_ACCUMULATEDIMPULSECONSTRAINTSOLVER

# include <cassert>
# include <tuple>

# include <Falling/Algebra/ScalarOp.hh>
# include <Falling/Algebra/MultInversibleSemiGroup.hh>
# include <Falling/Constraint/Solver/ContactEquation.hh>
# include <Falling/Constraint/Solver/ProjectedGaussSeidelSolver.hh>

namespace Falling
{
  namespace Constraint
  {
    namespace Solver
    {
      template <typename RB, typename LV, typename AV, typename IDX>
        class AccumulatedImpulseConstraintSolver
        {
          static_assert_Dimension((LV));
          static_assert_Dimension((AV));
          static_assert_HasProxy((IndexProxy), (RB));

          public:
          void operator()(double                                                        dt
                          , std::vector<std::tuple<RB*, RB*, Collision::Contact<LV>*>>& island
                          , std::vector<RB*>&                                           bodies)
          {
            if (!island.size())
              return; // no constraints to solve

            IDX indexer;

            unsigned numIndices = indexer(bodies); // set indices
            std::random_shuffle(island.begin(), island.end());
            _secondOrderSolve(dt, island, bodies, numIndices);
            _firstOrderSolve(dt, island, bodies, numIndices);
          }

          private:
          static const unsigned lvDim  = DimensionTrait<LV>::dim;
          static const unsigned avDim  = DimensionTrait<AV>::dim;
          static const unsigned maxDim = lvDim + avDim;

          void _firstOrderSolve(double dt
                                , std::vector<std::tuple<RB*, RB*, Collision::Contact<LV>*>>& island
                                , std::vector<RB*>&                                  bodies
                                , int                                                numIndices)
          {
            bool needsFirstOrder = false;

            for (auto coll : island)
            {
              if (needsFirstOrderResolution(*std::get<2>(coll)))
              {
                needsFirstOrder = true;
                break;
              }
            }

            if (!needsFirstOrder)
              return;

            constexpr unsigned equationsPerContacts = 1;

            auto numEquations = island.size() * equationsPerContacts;

            auto J      = new double[numEquations * 2 * maxDim];
            auto MJ     = new double[numEquations * 2 * maxDim];
            auto b      = new double[numEquations];
            auto lambda = new double[numEquations];
            auto bounds = new double[numEquations * 2];
            auto idx    = new int[numEquations * 2];

            std::fill(J, J + numEquations * 2 * maxDim, 0.0);
            std::fill(MJ, MJ + numEquations * 2 * maxDim, 0.0);
            std::fill(lambda, lambda + numEquations, 0.0);

            unsigned ididx    = 0;
            unsigned idJ      = 0;
            unsigned idb      = 0;
            unsigned idbounds = 0;

            for (auto coll : island)
            {
              auto rb1 = std::get<0>(coll);
              auto rb2 = std::get<1>(coll);

              fillFirstOrderContactEquation(
                  dt
                  , *std::get<2>(coll)
                  , *rb1, *rb2
                  , J, MJ, b, lambda, bounds, idx
                  , ididx, idJ, idb, idbounds);
            }

            // FIXME: should be generic wrt the LCP algorithm
            auto MJLambda = projectedGaussSeidelSolve<maxDim>(
                J
                , MJ
                , b
                , lambda
                , bounds
                , idx
                , numEquations
                , numIndices
                , 50);

            for (auto body : bodies)
            {
              int id = Metal::proxy<IndexProxy>(*body).index;

              if (id >= 0)
              {
                LV dp;
                AV da;

                unsigned offset = id * maxDim;

                std::copy(&MJLambda[offset], &MJLambda[offset + lvDim], dp.begin());

                offset += lvDim;
                std::copy(&MJLambda[offset], &MJLambda[offset + avDim], da.begin());

                dp *= dt;
                da *= dt;

                typename RB::TransformType t // FIXME: or TranformableTrait<RB>::TransformType ?
                  = MultInversibleSemiGroupTrait<typename RB::TransformType>::one;

                rotateWrtPoint(da, Shape::centerOfMass(*body), t);
                translate(dp, t); 

                append(*body, t);
              }
            }

            delete[] J;
            delete[] MJ;
            delete[] b;
            delete[] lambda;
            delete[] idx;
            delete[] MJLambda;
            delete[] bounds;
          }

          void _secondOrderSolve(double                                               dt
                                 , std::vector<std::tuple<RB*, RB*, Collision::Contact<LV>*>>& island
                                 , std::vector<RB*>&                                  bodies
                                 , unsigned                                           numIndices)
          {
            constexpr unsigned equationsPerContacts = 1; // FIXME: add friction

            auto numEquations = island.size() * equationsPerContacts;

            auto J      = new double[numEquations * 2 * maxDim];
            auto MJ     = new double[numEquations * 2 * maxDim];
            auto b      = new double[numEquations];
            auto lambda = new double[numEquations];
            auto bounds = new double[numEquations * 2];
            auto idx    = new int[numEquations * 2];

            std::fill(J, J + numEquations * 2 * maxDim, 0.0);
            std::fill(MJ, MJ + numEquations * 2 * maxDim, 0.0);
            std::fill(lambda, lambda + numEquations, 0.0);

            unsigned ididx    = 0;
            unsigned idJ      = 0;
            unsigned idb      = 0;
            unsigned idbounds = 0;

            for (auto coll : island)
            {
              auto rb1 = std::get<0>(coll);
              auto rb2 = std::get<1>(coll);

              fillSecondOrderContactEquation(
                  dt
                  , *std::get<2>(coll)
                  , *rb1, *rb2
                  , J, MJ, b, lambda, bounds, idx
                  , ididx, idJ, idb, idbounds);
            }

            // FIXME: should be generic wrt the LCP algorithm
            auto MJLambda = projectedGaussSeidelSolve<maxDim>(
                J
                , MJ
                , b
                , lambda
                , bounds
                , idx
                , numEquations
                , numIndices
                , 50);

            for (auto body : bodies)
            {
              int id = Metal::proxy<IndexProxy>(*body).index;

              if (id >= 0)
              {
                LV newLinearVelocity;
                AV newAngularVelocity;

                unsigned offset = id * maxDim;

                std::copy(&MJLambda[offset], &MJLambda[offset + lvDim]
                          , newLinearVelocity.begin());

                offset += lvDim;
                std::copy(&MJLambda[offset], &MJLambda[offset + avDim]
                          , newAngularVelocity.begin());

                setLinearVelocity(*body, newLinearVelocity + linearVelocity(*body));
                setAngularVelocity(*body, newAngularVelocity + angularVelocity(*body));
              }
            }

            // save cache for future warm-start
            for (double* l = lambda; l != lambda + numEquations; ++l)
              std::get<2>(island[l - lambda])->impulse = *l;

            delete[] J;
            delete[] MJ;
            delete[] b;
            delete[] lambda;
            delete[] idx;
            delete[] MJLambda;
            delete[] bounds;
          }
        };
    } // end Solver
  } // end Constraint
} // end Falling

#endif // FALLING_CONSTRAINT_SOLVER_ACCUMULATEDIMPULSECONSTRAINTSOLVER
