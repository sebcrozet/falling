#ifndef FALLING_CONSTRAINT_SOLVER_CONTACTEQUATION
# define FALLING_CONSTRAINT_SOLVER_CONTACTEQUATION

# include <cfloat>

# include <Falling/Metal/HasProxy.hh>

# include <Falling/Algebra/Dimension.hh>
# include <Falling/Collision/Contact.hh>
# include <Falling/Constraint/Solver/IndexProxy.hh>
# include <Falling/Body/Transformable.hh>
# include <Falling/Body/Dynamic.hh>
# include <Falling/Shape/HasCenterOfMass.hh>

namespace Falling
{
  namespace Constraint
  {
    namespace Solver
    {
      using namespace Falling::Algebra;
      using namespace Falling::Body;

# define FIRST_ORDER_MIN_PEN 0.08

      template <typename LV>
      bool needsFirstOrderResolution(const Collision::Contact<LV>& coll)
      {
        return coll.penetrationDepth > FIRST_ORDER_MIN_PEN;
      }

      template <typename RB
                , typename DT
                    = typename std::enable_if<DynamicTrait<RB>::value, DynamicTrait<RB>>::type>
        void fillFirstOrderContactEquation(double                 dt
                                           , const Collision::Contact<typename DT::LinearVelocityType>& coll
                                           , const RB&            rb1
                                           , const RB&            rb2
                                           , double*              J
                                           , double*              MJ
                                           , double*              b
                                           , double*              lambda
                                           , double*              bounds
                                           , int*                 idx
                                           , unsigned&            ididx
                                           , unsigned&            idJ
                                           , unsigned&            idb
                                           , unsigned&            idbounds)
        {
          using LV = typename DT::LinearVelocityType;
          using AV = typename DT::AngularVelocityType;

          static_assert_Transformable((RB));
          static_assert_HasProxy((IndexProxy), (RB));
          static_assert_AbelianGroup((LV));
          static_assert_HasCenterOfMass((RB));
          static_assert_DotProd((LV));
          static_assert_CrossProd((LV));
          static_assert_DotProd((AV));

          /*
           * Fill J and MJ
           */
          LV contactCenter = (coll.globalContact1 + coll.globalContact2) / 2.0;

          if (Metal::proxy<IndexProxy>(rb1).index >= 0)
          {
            unsigned idJa = idJ;

            // translation
            for (auto ne : coll.contactNormal)
            {
              J[idJa]  = -ne;
              MJ[idJa] = inverseMass(rb1) * -ne;
              ++idJa;
            }

            // rotation
            auto rotAxis = cross(contactCenter - Shape::centerOfMass(rb1), -coll.contactNormal);
            auto curr = idJa;

            for (auto re : rotAxis)
              J[curr++]  = re;

            auto weightedRotAxis = transform(worldSpaceInverseInertiaTensor(rb1)
                                             , rotAxis);
            for (auto wre : weightedRotAxis)
              MJ[idJa++] = wre;
          }

          if (Metal::proxy<IndexProxy>(rb2).index >= 0)
          {
            unsigned idJb = idJ + DimensionTrait<LV>::dim + DimensionTrait<AV>::dim;

            // translation
            for (auto ne : coll.contactNormal)
            {
              J[idJb]  = ne;
              MJ[idJb] = inverseMass(rb2) * ne;
              ++idJb;
            }

            // rotation
            auto rotAxis = cross(contactCenter - Shape::centerOfMass(rb2), coll.contactNormal);
            auto curr    = idJb;

            for (auto re : rotAxis)
              J[curr++]  = re;

            auto weightedRotAxis = transform(worldSpaceInverseInertiaTensor(rb2), rotAxis);

            for (auto wre : weightedRotAxis)
              MJ[idJb++] = wre;
          }

          idJ += 2.0 * (DimensionTrait<LV>::dim + DimensionTrait<AV>::dim);

          /*
           * Fill idx
           */
          idx[ididx++] = Metal::proxy<IndexProxy>(rb1).index;
          idx[ididx++] = Metal::proxy<IndexProxy>(rb2).index;

          /*
           * Fill b
           */
          b[idb] = 0.4 * std::max(0.0, coll.penetrationDepth - FIRST_ORDER_MIN_PEN) / dt;

          ++idb;

          /*
           * Fill bounds
           */
          bounds[idbounds++] = 0.0;
          bounds[idbounds++] = std::numeric_limits<double>::max();
        }

      template <typename RB
                , typename DT
                    = typename std::enable_if<DynamicTrait<RB>::value, DynamicTrait<RB>>::type>
        void fillSecondOrderContactEquation(double                 dt
                                            , const Collision::Contact<typename DT::LinearVelocityType>& coll
                                            , const RB&            rb1
                                            , const RB&            rb2
                                            , double*              J
                                            , double*              MJ
                                            , double*              b
                                            , double*              lambda
                                            , double*              bounds
                                            , int*                 idx
                                            , unsigned&            ididx
                                            , unsigned&            idJ
                                            , unsigned&            idb
                                            , unsigned&            idbounds)
        {
          using LV = typename DT::LinearVelocityType;
          using AV = typename DT::AngularVelocityType;

          static_assert_Transformable((RB));
          static_assert_HasProxy((IndexProxy), (RB));
          static_assert_AbelianGroup((LV));
          static_assert_HasCenterOfMass((RB));
          static_assert_DotProd((LV));
          static_assert_CrossProd((LV));
          static_assert_DotProd((AV));


          LV contactCenter = (coll.globalContact1 + coll.globalContact2) / 2.0;
          AV rotAxis1;
          AV rotAxis2;

          /*
           * Fill J and MJ
           */
          if (Metal::proxy<IndexProxy>(rb1).index >= 0)
          {
            unsigned idJa = idJ;

            // translation
            for (auto ne : coll.contactNormal)
            {
              J[idJa]  = -ne;
              MJ[idJa] = inverseMass(rb1) * -ne;
              ++idJa;
            }

            // rotation
            rotAxis1  = cross(contactCenter - Shape::centerOfMass(rb1), -coll.contactNormal);
            auto curr = idJa;

            for (auto re : rotAxis1)
              J[curr++]  = re;

            auto weightedRotAxis = transform(worldSpaceInverseInertiaTensor(rb1)
                                             , rotAxis1);
            for (auto wre : weightedRotAxis)
              MJ[idJa++] = wre;
          }

          if (Metal::proxy<IndexProxy>(rb2).index >= 0)
          {
            unsigned idJb = idJ + DimensionTrait<LV>::dim + DimensionTrait<AV>::dim;
            for (auto ne : coll.contactNormal)
            {
              J[idJb]  = ne;
              MJ[idJb] = inverseMass(rb2) * ne;
              ++idJb;
            }

            // rotation
            rotAxis2  = cross(contactCenter - Shape::centerOfMass(rb2), coll.contactNormal);
            auto curr = idJb;

            for (auto re : rotAxis2)
              J[curr++]  = re;

            auto weightedRotAxis = transform(worldSpaceInverseInertiaTensor(rb2)
                                             , rotAxis2);
            for (auto wre : weightedRotAxis)
              MJ[idJb++] = wre;
          }

          idJ += 2.0 * (DimensionTrait<LV>::dim + DimensionTrait<AV>::dim);

          /*
           * Fill idx
           */
          idx[ididx++] = Metal::proxy<IndexProxy>(rb1).index;
          idx[ididx++] = Metal::proxy<IndexProxy>(rb2).index;

          /*
           * Fill b and lambda
           */
          b[idb] = 0.0;

          if (Metal::proxy<IndexProxy>(rb1).index >= 0)
            b[idb] -= dot(linearVelocity(rb1) + dt * externalLinearForce(rb1)
                          , coll.contactNormal)
                      - dot(angularVelocity(rb1) + dt * externalAngularForce(rb1)
                            , rotAxis1);

          if (Metal::proxy<IndexProxy>(rb2).index >= 0)
            b[idb] += dot(linearVelocity(rb2) + dt * externalLinearForce(rb2)
                          , coll.contactNormal)
                      + dot(angularVelocity(rb2) + dt * externalAngularForce(rb2)
                            , rotAxis2);

          b[idb] -= 0.4 * std::max(0.0, std::min(FIRST_ORDER_MIN_PEN, coll.penetrationDepth) - 0.01) / dt;

          b[idb] = -b[idb];

          lambda[idb] = coll.impulse;

          ++idb;

          /*
           * Fill bounds
           */
          bounds[idbounds++] = 0.0;
          bounds[idbounds++] = std::numeric_limits<double>::max();
        }
    } // end Solver
  } // end Constraint
} // end Falling

#endif // FALLING_CONSTRAINT_SOLVER_CONTACTEQUATION
