#ifndef FALLING_METAL_REMOVE
# define FALLING_METAL_REMOVE

# include <type_traits>

# include <Falling/Metal/Cons.hh>

namespace Falling
{
  namespace Metal
  {
    // FIXME: implement Remove with Fold?
    template <template <typename> class P, typename F>
      struct RemoveBy;

    template <template <typename>      class P
              , template <typename...> class F>
      struct RemoveBy<P, F<>>
      {
        using type = F<>;
      };

    template <template <typename>      class P
              , template <typename...> class F
              , typename                     E
              , typename...                  ES>
      struct RemoveBy<P, F<E, ES...>>
      {
        using type =
          typename std::conditional<
            P<E>::value
            , F<ES...>
            , typename Cons<E, typename RemoveBy<P, F<ES...>>::type>::type
          >::type;
      };

    template <template <typename> class P, typename F>
      struct RemoveAllBy;

    template <template <typename>      class P
              , template <typename...> class F>
      struct RemoveAllBy<P, F<>>
      {
        using type = F<>;
      };

    template <template <typename>      class P
              , template <typename...> class F
              , typename                     E
              , typename...                  ES>
      struct RemoveAllBy<P, F<E, ES...>>
      {
        using type =
          typename std::conditional<
            P<E>::value
            , typename RemoveAllBy<P, ES...>::type
            , typename Cons<E, typename RemoveAllBy<P, F<ES...>>::type>::type
          >::type;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_REMOVE
