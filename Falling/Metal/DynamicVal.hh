#ifndef FALLING_METAL_DYNAMICVAL
# define FALLING_METAL_DYNAMICVAL

# include <cassert>
# include <Falling/Metal/Empty.hh>

namespace Falling
{
  namespace Metal
  {
    struct DynamicVal
    {
      private:
        Empty* _value = nullptr;

      public:
        template <typename T>
          explicit DynamicVal(T& v)
          : _value(&v)
          {
            static_assert(!std::is_same<T, DynamicVal>::value
                          , "DynamicVal and DynamicVal must not be nested.");
          }

        template <typename T>
          explicit DynamicVal(T* v)
          : _value(v)
          {
            static_assert(!std::is_same<T, DynamicVal>::value
                          , "DynamicVal and DynamicVal must not be nested.");
          }

        template <typename T>
          const T* valueAs() const
          {
            assert(!_value || (dynamic_cast<const T*>(_value) && "Invalid runtime type."));

            return static_cast<const T*>(_value);
          }

        template <typename T>
          T* valueAs()
          {
            assert(!_value || (dynamic_cast<T*>(_value) && "Invalid runtime type."));

            return static_cast<T*>(_value);
          }

        void setVal(Empty* v)
        { _value = v; }
    };

    struct ConstDynamicVal
    {
      private:
        const Empty* _value;

      public:
        template <typename T>
          explicit ConstDynamicVal(const T* v)
          : _value(v)
          {
            static_assert(!std::is_same<T, DynamicVal>::value
                          , "DynamicVal and ConstDynamicVal must not be nested.");
            static_assert(!std::is_same<T, ConstDynamicVal>::value
                          , "ConstDynamicVal and ConstDynamicVal must not be nested.");
          }

        template <typename T>
          const T* valueAs() const
          {
            assert(!_value || (dynamic_cast<const T*>(_value) && "Invalid runtime type."));

            return static_cast<const T*>(_value);
          }
    };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_DYNAMICVAL
