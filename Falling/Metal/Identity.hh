#ifndef FALLING_METAL_IDENTITY
# define FALLING_METAL_IDENTITY

namespace Falling
{
  namespace Metal
  {
    template <typename T>
      struct Identity
      {
        using type = T;
      };
  } // end Identity
} // end Falling

#endif // FALLING_METAL_IDENTITY
