#ifndef FALLING_METAL_FCONCAT
# define FALLING_METAL_FCONCAT

# include <Falling/Metal/Fold.hh>
# include <Falling/Metal/Concat.hh>

namespace Falling
{
  namespace Metal
  {
    template <typename FL>
      struct FConcat;

    template <template <typename...> class F>
      struct FConcat<F<>>
      {
        using type = F<>;
      };

    template <typename FL>
      struct FConcat
      {
        using type =
          typename Fold1<
            Concat
            , FL
          >::type;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_FCONCAT
