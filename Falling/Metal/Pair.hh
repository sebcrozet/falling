#ifndef FALLING_METAL_PAIR
# define FALLING_METAL_PAIR

namespace Falling
{
  namespace Metal
  {
    template <typename A, typename B>
      struct Pair
      { };

    template <typename P>
      struct First;

    template <typename A, typename B>
      struct First<Pair<A, B>>
      {
        using type = A;
      };

    template <typename P>
      struct Second;

    template <typename A, typename B>
      struct Second<Pair<A, B>>
      {
        using type = B;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_PAIR
