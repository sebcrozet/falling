#ifndef FALLING_METAL_FCONCATMAP
# define FALLING_METAL_FCONCATMAP

# include <Falling/Metal/FConcat.hh>
# include <Falling/Metal/FMap.hh>

namespace Falling
{
  namespace Metal
  {
    template <template   <typename> class Transformer
              , typename                  Transformee>
      struct FConcatMap
      {
        using type = typename FConcat<typename FMap<Transformer, Transformee>::type>::type;
      };


  } // end Metal
} // end Falling

#endif // FALLING_METAL_FCONCATMAP
