#ifndef FALLING_METAL_INHERIT
# define FALLING_METAL_INHERIT

# include <iostream>
# include <Falling/Metal/StaticDynamicBridge.hh>
# include <Falling/Metal/DynamicVal.hh>
# include <Falling/Metal/Sort.hh>
# include <Falling/Metal/Lattice.hh>
# include <Falling/Metal/Identity.hh>
# include <Falling/Metal/Apply.hh>
# include <Falling/Metal/Pair.hh>
# include <Falling/Metal/List.hh>

namespace Falling
{
  namespace Metal
  {
    template <typename... L>
      struct ConstInherit : public virtual L...
      {
        virtual const ConstDynamicVal& constValue() const = 0;

        virtual ~ConstInherit()
        { }
      };

    template <typename B>
      struct ConstInherit<List<>, B> : public virtual B // To avoid multiple ConstInherit<> base class
      {
        virtual const ConstDynamicVal& constValue() const = 0;
      };

    template <typename... A, typename B>
      struct ConstInherit<List<A...>, B> : public virtual ConstInherit<A...>
                                           , public virtual B
      {
        virtual const ConstDynamicVal& constValue() const = 0;
      };

    template <typename... L>
      struct ConstInheritLattice
      {
        using type = typename Lattice<ConstInherit, List, typename Sort<ConstInherit<L...>>::type>::type;
      };

    template <typename... L>
      struct Inherit : public virtual L...
      {
        virtual const ConstDynamicVal& constValue() const = 0;
        virtual DynamicVal&       value()                 = 0;
        virtual void           setValue(DynamicVal&)      = 0;

        virtual ~Inherit()
        { }
      };

    template <typename B>
      struct Inherit<List<>, B> : public virtual B // To avoid multiple Inherit<> base class
      {
        virtual const ConstDynamicVal& constValue() const = 0;
        virtual DynamicVal&       value()                 = 0;
        virtual void           setValue(DynamicVal&)      = 0;
      };

    template <typename... A, typename B>
      struct Inherit<List<A...>, B> : public virtual B
                                      , public virtual ConstInheritLattice<A...>::type
      {
        virtual const ConstDynamicVal& constValue() const = 0;
        virtual DynamicVal& value()                       = 0;
        virtual void setValue(DynamicVal&)                = 0;
      };

    template <typename... L>
      struct InheritLattice
      {
        using type = typename Lattice<Inherit, List, typename Sort<Inherit<L...>>::type>::type;
      };

    template <typename... DynamicTraits>
      struct ConstDynamicValue
        : public DynamicTraits...
          , public FMap<DynamicBridge, ConstInheritLattice<DynamicTraits...>>::type::type
      {
        private:
          ConstDynamicVal _value;

        public:
          template <typename T>
            explicit ConstDynamicValue(T& value)
            : _value(&value)
            { }

          template <typename T>
            explicit ConstDynamicValue(T* value)
            : _value(value)
            { }

          virtual void setValue(ConstDynamicVal& val)
          { _value = val; }

          virtual const ConstDynamicVal& constValue() const
          { return _value; }
      };

    // FIXME: move this on another file?
    template <typename... DynamicTraits>
      struct DynamicValue
      : public DynamicTraits...
        , public FMap<DynamicBridge, InheritLattice<DynamicTraits...>>::type::type
      {
        private:
          DynamicVal              _value;
          mutable ConstDynamicVal _constValue;

        public:
          using parent =
            typename FMap<DynamicBridge, InheritLattice<DynamicTraits...>>::type::type;

          template <typename T>
            explicit DynamicValue(T& value)
            : _value(value)
            { }

          template <typename T>
            explicit DynamicValue(T* value)
            : _value(value), _constValue(value)
            { }

          virtual void setValue(DynamicVal& val)
          { _value = val; }

          virtual DynamicVal& value()
          { return _value; }

          virtual const ConstDynamicVal& constValue() const
          {
            _constValue = ConstDynamicVal(_value.valueAs<Empty>());
            return _constValue;
          }
      };

    template <typename F, typename E>
      struct ApplyToStaticBridge
      {
        private:
          template <typename _SB>
            struct _ApplyStaticBridge
            {
              using type = typename StaticBridge<_SB>::template type<E>;
            };

        public:
          using type = typename FMap<_ApplyStaticBridge, F>::type;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_INHERIT
