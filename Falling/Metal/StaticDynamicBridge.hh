#ifndef FALLING_METAL_STATICDYNAMICBRIDGE
# define FALLING_METAL_STATICDYNAMICBRIDGE

namespace Falling
{
  namespace Metal
  {
    template <typename T>
      struct StaticBridge;

    template <typename T>
      struct DynamicBridge;
  };
} // end Falling

#endif // FALLING_METAL_STATICDYNAMICBRIDGE
