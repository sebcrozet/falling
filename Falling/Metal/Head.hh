#ifndef FALLING_METAL_HEAD
# define FALLING_METAL_HEAD

namespace Falling
{
  namespace Metal
  {
    template <typename L>
      struct Head;

    template <template <typename...> class F
              , typename    H
              , typename... L>
      struct Head<F<H, L...>>
      {
        using type = H;
      };
  }; // end Metal
}; // end Falling

#endif // FALLING_METAL_HEAD
