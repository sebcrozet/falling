#ifndef FALLING_METAL_EMPTY
# define FALLING_METAL_EMPTY

namespace Falling
{
  namespace Metal
  {
    struct Empty
    {
      virtual ~Empty()
      { }
    };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_EMPTY
