#ifndef FALLING_METAL_LATTICE
# define FALLING_METAL_LATTICE

// # include <Falling/Metal/Interleave.hh>
# include <Falling/Metal/Cons.hh>
# include <Falling/Metal/Tail.hh>
# include <Falling/Metal/FMap.hh>
# include <Falling/Metal/FConcatMap.hh>
# include <Falling/Metal/Remove.hh>
# include <Falling/Metal/Size.hh>

namespace Falling
{
  namespace Metal
  {
    /**
     * Template implementation of the Haskell function:
     *
     * removeOne :: [a] -> [[a]]
     * removeOne []            = []
     * removeOne (e:es)        = es : map (e:) (removeOne es)
     */
    // FIXME: could be put directly inside of the Lattice definition (to hide it)
    template <typename F>
      struct _RemoveOne;

    template <template <typename...> class F>
      struct _RemoveOne<F<>>
      {
        using type = F<>;
      };

    template <template <typename...> class F
              , typename                   E
              , typename...                ES>
      struct _RemoveOne<F<E, ES...>>
      {
        template <typename _F>
          using _ECons = Cons<E, _F>;

        using type =
          typename Cons<
            F<ES...>
            , typename FMap<_ECons, typename _RemoveOne<F<ES...>>::type>::type
          >::type;
      };

    /**
     * Template implementation of the Haskell function:
     *
     * data Lattice a = Lattice a [ Lattice a ]
     * 
     * mkLattice :: [a] -> Lattice [a]
     * mkLattice [] = Lattice [] []
     * mkLattice l  = Lattice l $ map mkLattice (removeOne l)
     */
    template <template <typename...>   class Lt // FIXME: use template <typename, typename> instead?
              , template <typename...> class St
              , typename F>
      struct Lattice;

    template <template <typename...>  class Lt
             , template <typename...> class St
             , template <typename...> class F>
      struct Lattice<Lt, St, F<>>
      {
        using type = Lt<St<>, F<>>;
      };

    template <template <typename...>   class Lt
              , template <typename...> class St
              , template <typename...> class F
              , typename...                  L>
      struct Lattice<Lt, St, F<L...>>
      {
        template <typename T>
          using _Lattice = Lattice<Lt, St, T>;

        using type =
          Lt<
            St<L...>
            , typename FMap<_Lattice, typename _RemoveOne<F<L...>>::type>::type
          >;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_LATTICE
