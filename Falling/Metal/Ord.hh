#ifndef FALLING_METAL_ORD
# define FALLING_METAL_ORD

namespace Falling
{
  namespace Metal
  {
    // FIXME: using Enabler is quite uggly…
    template <typename A, typename B, typename Enabler = void>
      struct Ord
      { };

    // $$ Error: template argument must be a type
    // How to make this work?
    //
    // template <int A, int B>
    //   struct Ord<A, B>
    //   {
    //     static const bool greater        = A >  B;
    //     static const bool smaller        = A <  B;
    //     static const bool equal          = A == B;
    //     static const bool greaterOrEqual = A >= B;
    //     static const bool smallerOrEqual = A <= B;
    //   };

  } // end Ord
} // end Falling

#endif // FALLING_METAL_ORD
