#ifndef FALLING_METAL_TAIL
# define FALLING_METAL_TAIL

namespace Falling
{
  namespace Metal
  {
    template <typename L>
      struct Tail;

    template <template <typename...> class F
              , typename    H
              , typename... L>
      struct Tail<F<H, L...>>
      {
        using type = F<L...>;
      };
  }; // end Metal
}; // end Falling

#endif // FALLING_METAL_TAIL
