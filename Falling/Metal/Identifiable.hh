#ifndef FALLING_METAL_IDENTIFIABLE
# define FALLING_METAL_IDENTIFIABLE

# include <cassert>
# include <Falling/Metal/StaticDynamicBridge.hh>
# include <Falling/Metal/TypeToId.hh>
# include <Falling/Metal/Empty.hh>

namespace Falling
{
  namespace Metal 
  {
    namespace Static
    {
# define static_assert_Identifiable(TYPE)                                                     \
         static_assert(Falling::Metal::Static::IdentifiableTrait<typename RMPAR(TYPE)>::value \
                       , "Type parameter " #TYPE " must be identifiable.")
      /**
       * \brief Concept to specialized for types having an identifier.
       * An identifier is an integer intended to be unique for each different type. The default
       * implementation already provide an unique identifier for each type.
       * It is not needed to specialize this concept as long as the default implementation suits
       * your need.
       * \param S The type having an identifier.
       * \param Enabler Extra parameter allowing more powerful conditional specializations.
       */
      template <typename S, typename Enabler = void>
        struct IdentifiableTrait
        {
          private:
            static Metal::Empty _unused;

          public:
            /**
             * \brief Indicates whether this concept is enabled for its parameter.
             * This must be defined as true by specializer.
             * This concept already has a default implementation.
             */
            static const bool value = true;

            /**
             * \brief Retrieves the unique identifier associated with this type.
             */
            static size_t identifier()
            { return reinterpret_cast<size_t>(&_unused); }
        };

      template <typename S, typename Enabler>
        Metal::Empty IdentifiableTrait<S, Enabler>::_unused;
    } // end Static

    namespace Dynamic
    {
      struct Identifiable
      {
        virtual int identifier() const = 0;
      };

      template <typename S>
        struct IdentifiableTrait : virtual Identifiable
        {
          static_assert_Identifiable((S));

          virtual int identifier() const
          {
            return Static::IdentifiableTrait<S>::identifier();
          }
        };
    } // end Dynamic

    // tie the knot
    template <typename S>
      struct DynamicBridge<Dynamic::IdentifiableTrait<S>>
      {
        using type = Dynamic::Identifiable;
      };

    template <>
      struct StaticBridge<Dynamic::Identifiable>
      {
        template <typename S>
          using type = Dynamic::IdentifiableTrait<S>;
      };

    // register identifier
    template<typename S>
      struct TypeToId<Dynamic::IdentifiableTrait<S>> : register_id<int, 20000> { };

    template <>
      struct TypeToId<Dynamic::Identifiable> : register_id<int, 2000> { };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_IDENTIFIABLE
