#ifndef FALLING_METAL_SIZE
# define FALLING_METAL_SIZE

namespace Falling
{
  namespace Metal
  {
    template <typename F>
      struct Size;

    template <template <typename...> class F
              , typename...                L>
      struct Size<F<L...>>
      {
        static const unsigned int value = sizeof...(L);
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_SIZE
