#ifndef FALLING_METAL_CONCAT
# define FALLING_METAL_CONCAT

namespace Falling
{
  namespace Metal
  {
    template <typename E, typename F>
      struct Concat;

    template <template <typename...> class F
              , typename... A
              , typename... B
              >
      struct Concat<F<A...>, F<B...>>
      {
        using type = F<A..., B...>;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_CONCAT
