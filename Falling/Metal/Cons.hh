#ifndef FALLING_METAL_CONS
# define FALLING_METAL_CONS

namespace Falling
{
  namespace Metal
  {
    template <typename A, typename F>
      struct ConsL
      { };

    template <typename                       A
              , template <typename...> class F
              , typename...                  L>
      struct ConsL<A, F<L...>>
      {
        using type = F<A, L...>;
      };

    template <typename A, typename F>
      struct ConsR
      { };

    template <typename                       A
              , template <typename...> class F
              , typename...                  L>
      struct ConsR<A, F<L...>>
      {
        using type = F<L..., A>;
      };

    template <typename A, typename F>
      struct Cons
      {
        using type = typename ConsL<A, F>::type;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_CONS
