#ifndef FALLING_METAL_HASPROXY
# define FALLING_METAL_HASPROXY

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Metal
  {
# define static_assert_HasProxy(TYPEP, TYPET)         \
         static_assert(Falling::Metal::HasProxyTrait< \
             typename RMPAR(TYPEP)                    \
             , typename RMPAR(TYPET)>::value          \
             , "Type parameter " #TYPET " must have a proxy of type " #TYPEP ".")
    template <typename Proxy, typename T, typename Enabler = void>
      struct HasProxyTrait
      {
        private:
          using T_NoConst = typename std::remove_const<T>::type;

        public:
          static const bool value = false;

          static const Proxy& proxy(const T_NoConst&);
          static Proxy& proxy(T_NoConst&);
      };

    template <typename Proxy, typename T, BIND_CONCEPT(HPT, (HasProxyTrait<Proxy, T>))>
    Proxy& proxy(T& t)
    { return HPT::proxy(t); }

    template <typename Proxy, typename T, BIND_CONCEPT(HPT, (HasProxyTrait<Proxy, T>))>
    const Proxy& proxy(const T& t)
    { return HPT::proxy(t); }
  } // end Metal
} // end Falling

#endif // FALLING_METAL_HASPROXY
