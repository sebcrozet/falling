#ifndef FALLING_METAL_SORT
# define FALLING_METAL_SORT

# include <type_traits>
# include <Falling/Metal/Ord.hh>
# include <Falling/Metal/Fold.hh>

namespace Falling
{
  namespace Metal
  {
    /**
     * Temlate implementation of the Haskell function:
     *
     * sortedInsert :: Ord a => a -> [a] -> [a]
     * sortedInsert a []    = [a]
     * sortedInsert a (e:es) = if a > e then (e:sortedInsert a es) else (a:sortedInsert e es)
     */
    template <typename A, typename F>
      struct SortedInsert;

    template <typename A
              , template <typename...> class F>
      struct SortedInsert<A, F<>>
      {
        using type = F<A>;
      };

    template <typename                       A
              , template <typename...> class F
              , typename                     E
              , typename...                  ES>
      struct SortedInsert<A, F<E, ES...>>
      {
        using type =
          typename std::conditional<
            Ord<A, E>::greater
            , typename Cons<
                E
                , typename SortedInsert<A, F<ES...>>::type
              >::type
            , typename Cons<
                A
                , typename SortedInsert<E, F<ES...>>::type
              >::type
          >::type;
      };


    /**
     * Template implementation of the Haskell function:
     * sort :: Ord a => [a] -> [a]
     * sort l = foldr sortedInsert [] l
     */
    template <typename F>
      struct Sort;

    template <template <typename...> class F
              , typename...                L>
      struct Sort<F<L...>>
      {
        // sort l = foldr sortedInsert [] l
        using type =
          typename Fold<
            SortedInsert
            , F<>
            , F<L...>
          >::type;
      };
  } // end Metal
} // end Falling


#endif // FALLING_METAL_SORT
