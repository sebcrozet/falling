#ifndef FALLING_METAL_FOLD
# define FALLING_METAL_FOLD

# include <Falling/Metal/Cons.hh>

namespace Falling
{
  namespace Metal
  {
    /**
     * Template implementation of the Haskell function:
     *
     * fold :: (a -> b -> b) -> b -> [a] -> b
     * fold f acc []     = acc
     * fold f acc (e:es) = fold f (f e acc) es
     *
     * This is a left fold.
     */
    template <template <typename, typename> class, typename, typename>
      struct Fold;

    template <template<typename, typename> class F
              , typename                         Acc
              , template <typename...>     class L>
      struct Fold<F, Acc, L<>>
      {
        // fold f acc []     = acc
        using type = Acc;
      };

    template <template<typename, typename> class F
              , typename                         Acc
              , template <typename...>     class L
              , typename                         E
              , typename...                      ES>
      struct Fold<F, Acc, L<E, ES...>>
      {
        // fold f acc (e:es) = fold f (f e acc) es
        using type =
          typename Fold<
            F
            , typename F<E, Acc>::type
            , L<ES...>
          >::type;
      };

    /**
     * Template implementation of the Haskell function:
     *
     * fold1 :: (a -> a -> a) -> [a] -> a
     * fold1 f (e:[])    = e
     * fold1 f (e1:e2:l) = fold1 f $ (f e1 e2 : l)
     */
    template <template <typename, typename> class, typename>
      struct Fold1;

    template <template <typename, typename> class F
              , template <typename...>      class L
              , typename                          Head>
      struct Fold1<F, L<Head>>
      {
        // fold1 f (e:[]) = e
        using type = Head;
      };

    template <template <typename, typename> class F
              , template <typename...>      class L
              , typename                          E1
              , typename                          E2
              , typename...                       Tail>
      struct Fold1<F, L<E1, E2, Tail...>>
      {
        // fold1 f (e1:e2:l) = fold1 f $ (f e1 e2 : l)
        using type = typename Fold1<F, typename Cons<typename F<E1, E2>::type, L<Tail...>>::type>::type;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_FOLD
