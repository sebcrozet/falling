#ifndef FALLING_METAL_LIST
# define FALLING_METAL_LIST

namespace Falling
{
  namespace Metal
  {
    template <typename...>
      struct List
      { };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_LIST
