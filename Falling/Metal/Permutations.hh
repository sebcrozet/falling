#ifndef FALLING_METAL_PERMUTATIONS
# define FALLING_METAL_PERMUTATIONS

# include <Falling/Metal/FConcatMap.hh>
# include <Falling/Metal/Interleave.hh>
# include <Falling/Metal/Fold.hh>

namespace Falling
{
  namespace Metal
  {
    /**
     * Template implementation of the Haskell function:
     *
     * permutations :: [a] -> [[a]]
     * permutations []     = []
     * permutations (e:[]) = [[e]]
     * permutations (e:es) = concat $ map (interleave e) $ permutations es
     */

    template <typename>
      struct Permutations;

    template <template <typename...> class F
              , typename E>
      struct Permutations<F<E>>
      {
        using type = F<F<E>>;
      };

    template <template <typename...> class F
              , typename    E
              , typename... L>
      struct Permutations<F<E, L...>>
      {
        private:
        // definition of partially applied interleave: (interleave e)
        template <typename _L>
          struct _InterleaveE;

        template <typename... _L>
          struct _InterleaveE<F<_L...>>
          {
            using type = typename Interleave<E, F<_L...>>::type;
          };

        public:
        // permutations (e:es) = foldr1 $ map (interleave e) $ permutations es
        using type =
          typename FConcatMap<
            _InterleaveE
            , typename Permutations<F<L...>>::type
          >::type;
      };

  } // end Permutations
} // end Falling

#endif // FALLING_METAL_PERMUTATIONS
