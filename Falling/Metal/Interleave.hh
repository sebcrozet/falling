#ifndef FALLING_METAL_INTERLEAVE
# define FALLING_METAL_INTERLEAVE

# include <Falling/Metal/Cons.hh>
# include <Falling/Metal/FMap.hh>

namespace Falling
{
  namespace Metal
  {
    /**
     * Template implementation of the Haskell function:
     *
     *  interleave :: a -> [a] -> [[a]]
     *  interleave a []     = [[a]]
     *  interleave a (e:[]) = [[a, e], [e, a]]
     *  interleave a (e:es) = (a:e:es) : map (e:) (interleave a es)
     */
    template <typename E, typename L>
      struct Interleave;

    template <typename A
              , template <typename...> class F
              , typename E
              >
      struct Interleave<A, F<E>>
      {
        // interleave a (e:[]) = [[a, e], [e, a]]
        using type =
          F<
             F<A, E>
             , F<E, A>
           >;
      };

    template <typename A
              , template <typename...> class F
              , typename    E
              , typename... ES
              >
      struct Interleave<A, F<E, ES...>>
      {
        // definition of partialy applied cons: (e:)
        private:
        template <typename _F>
          struct _ACons
          {
            using type = typename Cons<A, _F>::type;
          };

        public:
        // intereave a (e:es) = (a:e:es) : map (e:) (interleave a es)
        using type =
          typename Cons<
            F<A, E, ES...>
            , typename FMap<_ACons, typename Interleave<A, F<ES...>>::type>::type
          >::type;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_INTERLEAVE
