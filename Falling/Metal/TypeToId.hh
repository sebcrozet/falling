#ifndef FALLING_METAL_TYPETOID
# define FALLING_METAL_TYPETOID

# include <Falling/Metal/Ord.hh>

namespace Falling
{
  namespace Metal
  {
    // good idea comming from http://stackoverflow.com/questions/3741682/type-to-int-mapping
    template<int N>
      struct marker_id {
        static int const  value = N;
        static bool const found = true;
      };

    template<typename T>
      struct marker_type { using type = T; };

    template<typename T, int N>
      struct register_id : marker_id<N>, marker_type<T> {
        private:
          friend marker_type<T> marked_id(marker_id<N>) {
            return marker_type<T>();
          }
      };

    template<typename T>
      struct TypeToId
      {
        static const bool found = false;
      };

    // then register types using:
    //
    // template<>
    // struct TypeToId<int> : register_id<int, 0> { };

    template <typename A
              , typename B>
      struct Ord<A, B, typename std::enable_if<TypeToId<A>::found && TypeToId<B>::found, void>::type>
      {
        static const bool greater        = TypeToId<A>::value >  TypeToId<B>::value;
        static const bool smaller        = TypeToId<A>::value <  TypeToId<B>::value;
        static const bool equal          = TypeToId<A>::value == TypeToId<B>::value;
        static const bool greaterOrEqual = TypeToId<A>::value >= TypeToId<B>::value;
        static const bool smallerOrEqual = TypeToId<A>::value <= TypeToId<B>::value;
      };

  } // end Metal
} // end Falling

#endif // FALLING_METAL_TYPETOID
