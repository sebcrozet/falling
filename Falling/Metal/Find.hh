#ifndef FALLING_METAL_FIND
# define FALLING_METAL_FIND

# include <type_traits>

namespace Falling
{
  namespace Metal
  {
    template <typename T, typename F>
      struct Find
      {
        template <int, typename, typename>
          struct _FindImpl;

        template <int                            curr
                  , typename                     _T
                  , template <typename...> class _F>
          struct _FindImpl<curr, _T, _F<>>
          {
            static const int value = -1;
          };

        template <int                            curr
                  , typename                     _T
                  , template <typename...> class _F
                  , typename                     _E
                  , typename...                  _L>
          struct _FindImpl<curr, _T, _F<_E, _L...>>
          {
            static const int value =
              std::is_same<_T, _E>::value ? curr : _FindImpl<curr + 1, _T, _F<_L...>>::value;
          };

        static const int value = _FindImpl<0, T, F>::value;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_FIND
