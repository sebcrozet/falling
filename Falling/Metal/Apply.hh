#ifndef FALLING_METAL_APPLY
# define FALLING_METAL_APPLY

namespace Falling
{
  namespace Metal
  {
    template <template <typename> class T, typename E>
      struct Apply
      {
        using type = T<E>;
      };
  } // end Metal
} // end Falling

#endif // end FALLING_METAL_APPLY
