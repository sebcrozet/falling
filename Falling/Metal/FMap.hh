#ifndef FALLING_METAL_FMAP
# define FALLING_METAL_FMAP

# include <Falling/Metal/Cons.hh>

namespace Falling
{
  namespace Metal
  {
    template <template <typename> class Transformer
              , typename                Transformee>
      struct FMap;

    template <template   <typename>    class Transformer
              , template <typename...> class Transformee>
      struct FMap<Transformer, Transformee<>>
      {
        using type = Transformee<>;
      };

    template <template   <typename>    class Transformer
              , template <typename...> class Transformee
              , typename    E
              , typename... ES>
      struct FMap<Transformer, Transformee<E, ES...>>
      {
        using type =
          typename Cons<
            typename Transformer<E>::type
            , typename FMap<Transformer, Transformee<ES...>>::type
          >::type;
      };
  };
}

#endif // FALLING_METAL_FMAP
