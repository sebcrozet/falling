#ifndef FALLING_METAL_FCONVERT
# define FALLING_METAL_FCONVERT

namespace Falling
{
  namespace Metal
  {

    template <typename E
              , template <typename ...> class A>
      struct FConvert;
    
    template <template   <typename ...> class E
              , template <typename ...> class A
              , typename... L>
      struct FConvert<E<L...>, A>
      {
        using type = A<L...>;
      };
  } // end Metal
} // end Falling

#endif // FALLING_METAL_FCONVERT

