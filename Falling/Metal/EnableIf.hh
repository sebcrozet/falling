#ifndef FALLING_METAL_ENABLEIF
# define FALLING_METAL_ENABLEIF

namespace Falling
{
  namespace Metal
  {
    template <typename T>
      struct _LiftParenthesis;

    template <typename T, typename A>
      struct _LiftParenthesis<T(A)>
      {
        using type = A;
      };
  } // end Metal
} // end Falling

# define RMPAR(A)                    Falling::Metal::_LiftParenthesis<void A>::type
# define ENABLE_EXPR_IF(A)           typename std::enable_if<A, int>::type = 0
# define DECL_IF(Decl, Cond, Type)   typename Decl = typename std::enable_if<Cond, Type>::type

// FIXME: this is uggly to have both ENABLE_IF and CHECK_ENABLER…
// This is because CHECK_ENABLER cannot check several conditions at once.
# define ENABLE_IF(Cond)             typename std::enable_if<Cond, void>::type

# define CHECK_ENABLER(Enabler)      ENABLE_IF(RMPAR(Enabler)::value)
# define CHECK_CONCEPT(Concept)      ENABLE_EXPR_IF(RMPAR(Concept)::value)
# define BIND_CONCEPT(Decl, Concept) \
  DECL_IF(Decl, RMPAR(Concept)::value, typename RMPAR(Concept))

#endif // FALLING_METAL_ENABLEIF
