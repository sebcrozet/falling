#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_MINKOWSKISAMPLING
# define FALLING_COLLISION_DETECTION_NARROWPHASE_MINKOWSKISAMPLING

# include <limits>
# include <Falling/Shape/TranslatedShape/TranslatedShape.hh>
# include <Falling/Shape/TranslatedShape/ImplicitShape.hh>
# include <Falling/Shape/TranslatedShape/HasCenterOfMass.hh>

# include <Falling/Shape/MinkowskiSum/MinkowskiSum.hh>
# include <Falling/Shape/MinkowskiSum/ImplicitShape.hh>
# include <Falling/Shape/MinkowskiSum/HasCenterOfMass.hh>

# include <Falling/Shape/CSO/CSO.hh>
# include <Falling/Shape/CSO/ImplicitShape.hh>
# include <Falling/Shape/CSO/HasCenterOfMass.hh>

# include <Falling/Shape/Ball/Ball.hh>
# include <Falling/Shape/Ball/ImplicitShape.hh>
# include <Falling/Shape/Ball/HasCenterOfMass.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        // FIXME: at this point, we dont know that we are working with a Minkowski sum! Thus, the
        // name «MinkowskiSampling» is not general enough.
        struct MinkowskiSampling
        {
          template <typename       Splx
                    , typename     SA
                    , typename     SB
                    , typename     V
                    , typename     Err
                    , unsigned int numSamples>
            static std::pair<V, V> projectOrigin(const SA& sa, const SB& sb)
            {
              static_assert_UnitSphereSamples((V), numSamples);
              static_assert_DotProd((V));
              static_assert_AbelianGroup((V));

              // we enlarge the shapes with a small sphere
              //
              // FIXME: using minkowskiSum(CSO(...)) could be more
              // efficient than the current approach (CSO(minkowskiSum(...), minkowskiSum(..)))
              auto cso = makeCSO(makeMinkowskiSum(sa, Ball<V>(Err::margin / 2.0))
                                 , makeMinkowskiSum(sb, Ball<V>(Err::margin / 2.0)));

              const V* bestDir;
              double   minDist1 = std::numeric_limits<double>::max();

              for (const V& dir : UnitSphereSamples<V, numSamples>::samples())
              {
                double dist = dot(dir, supportPoint(cso, dir));

                if (dist < minDist1)
                {
                  bestDir = &dir;
                  minDist1 = dist;
                }
              }

              assert(minDist1 >= 0.0 && "The Minkovski Sampling algorithm must be used"
                  " only when the origin is inside of the cso.");

              std::pair<V, V> projection;

              V shift = *bestDir * minDist1;

              bool success = GJK::closestPoints<Splx, SA, TranslatedShape<SB, V>, V, Err>
                (sa, TranslatedShape<SB, V>(sb, shift), projection);

              assert(success && "The origin was inside of the Simplex during phase 1.");

              V correctedNormal = normalized(std::get<1>(projection)
                                             - std::get<0>(projection));

              double minDist2 = dot(correctedNormal, supportPoint(cso, correctedNormal));

              assert(minDist2 >= 0.0 && "The Minkowski Sampling algorithm must be used"
                  " only when the origin is inside of the cso.");

              assert(minDist1 >= minDist2);

              shift = correctedNormal * minDist2;

              success = GJK::closestPoints<Splx, SA, TranslatedShape<SB, V>, V, Err>
                (sa, TranslatedShape<SB, V>(sb, shift), projection);

              assert(success && "The origin was inside of the Simplex during phase 2.");

              return std::make_pair(std::get<0>(projection)
                                    , std::get<1>(projection) - shift);
            }
        };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling


#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_MINKOWSKISAMPLING
