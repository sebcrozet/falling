#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_IMPLICITSHAPEIMPLICITSHAPECOLLISIONDETECTOR
# define FALLING_COLLISION_DETECTION_NARROWPHASE_IMPLICITSHAPEIMPLICITSHAPECOLLISIONDETECTOR

# include <Falling/Metal/Inherit.hh>
# include <Falling/Metal/Identifiable.hh>
# include <Falling/Collision/Detection/NarrowPhase/AbstractNarrowPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/ImplicitShapeImplicitShapeCollisionDetection.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        template <typename Splx, typename SA, typename SB, typename V, typename Err
                  , unsigned int numSamples>
          class ImplicitShapeImplicitShapeCollisionDetector
                : public AbstractNarrowPhase<Contact<V>
                                             , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>
        {
          private:
            Contact<V> _coll;
            bool       _ok;

          public:
            virtual void update(const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sa
                                , const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sb)
            {
              _ok = collideImplicitShapeImplicitShape<Splx, SA, SB, V, Err, numSamples>(
                  *sa.constValue().valueAs<SA>()
                  , *sb.constValue().valueAs<SB>()
                  , _coll);

              if (_ok)
                _coll.impulse = 0.0;
            }

            virtual unsigned int numContacts() const
            {
              return _ok ? 1 : 0;
            }

            virtual void collisions(std::vector<Contact<V>*>& out)
            {
              if (_ok)
                out.push_back(&_coll);
            }

            virtual ImplicitShapeImplicitShapeCollisionDetector* clone() const
            { return new ImplicitShapeImplicitShapeCollisionDetector(); }
        };
      } // end narrowPhase
    } // end Detection
  } // end Collision
} // end Falling


#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_IMPLICITSHAPEIMPLICITSHAPECOLLISIONDETECTOR
