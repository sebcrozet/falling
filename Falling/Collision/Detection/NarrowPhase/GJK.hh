#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_GJK
# define FALLING_COLLISION_DETECTION_NARROWPHASE_GJK

# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/Shape/CSO/CSO.hh>
# include <Falling/Shape/MinkowskiSum/AnnotatedSupportPoint.hh>
# include <Falling/Collision/Detection/NarrowPhase/Simplex.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
          struct GJK
          {
            template <typename Splx, typename S, typename V, typename Err>
              static bool projectOrigin(const S& shape, Splx& simplex, V& proj)
              {
                static_assert_DotProd((V));
                static_assert_AbelianGroup((V));
                static_assert_ImplicitShape((S), (V));
                static_assert_Dimension((Splx));

                using SubAlgorithm = Simplex<Splx, V>;

                proj            = SubAlgorithm::projectOrigin(simplex);
                double sqlenDir = sqlen(proj);

                while (true)
                {
                  auto csoPoint = supportPoint(shape, neg(proj));

                  if (SubAlgorithm::containsPoint(simplex, csoPoint)
                      || sqlenDir - dot(proj, csoPoint) <= Err::sqEpsRel * sqlenDir)
                  {
                    return true; // the distance found has a good enough precision
                  }

                  SubAlgorithm::addPoint(simplex, csoPoint);
                  proj               = SubAlgorithm::projectOrigin(simplex);
                  double oldSqlenDir = sqlenDir;
                  sqlenDir           = sqlen(proj);

                  if (SubAlgorithm::dimension(simplex) == DimensionTrait<Splx>::dim
                      || sqlenDir <= Err::epsTol * SubAlgorithm::maxSqLen(simplex))
                    return false; // point inside of the cso

                  if (sqlenDir >= oldSqlenDir)
                    return true;
                }
              }

            template <typename Splx, typename S, typename V, typename Err>
              static bool projectOrigin(const S& shape, V& projection)
              {
                static_assert_ImplicitShape((S), (V));

                using namespace Shape;

                auto simplex = Splx();

                Simplex<Splx, V>::addPoint(simplex, supportPoint(shape, -centerOfMass(shape)));

                return projectOrigin<Splx, S, V, Err>(shape, simplex, projection);
              }

            template <typename Splx, typename SA, typename SB, typename V, typename Err>
              static double distanceBetween(const SA& a, const SB& b)
              {
                using namespace Shape;

                static_assert_HasCenterOfMass((CSO<SA, SB>));
                static_assert_ImplicitShape((CSO<SA, SB>), (V));

                V projection;

                auto simplex = Splx();

                CSO<SA, SB> cso(a, b);

                Simplex<Splx, V>::addPoint(simplex, supportPoint(cso, -centerOfMass(cso)));

                bool success = projectOrigin<Splx, CSO<SA, SB>, V, Err>(
                    cso
                    , simplex
                    , projection
                );

                return success ? len(projection) : 0.0;
              }

            template <typename Splx, typename SA, typename SB, typename V, typename Err>
              static bool closestPoints(const SA& a, const SB& b, std::pair<V, V>& out)
              {
                using namespace Shape;

                static_assert_HasCenterOfMass((CSO<SA, SB>));
                static_assert_ImplicitShape((CSO<SA, SB>), (V));

                auto        simplex = Splx();
                CSO<SA, SB> cso(a, b);

                Simplex<Splx, AnnotatedSupportPoint<V>>::addPoint(
                    simplex
                    , supportPoint(cso, AnnotatedSupportPoint<V>(-centerOfMass(cso))));

                AnnotatedSupportPoint<V> projection;

                bool success = projectOrigin<Splx, CSO<SA, SB>, AnnotatedSupportPoint<V>, Err>(
                    cso
                    , simplex
                    , projection
                );

                std::get<0>(out) = projection.sourceA;
                std::get<1>(out) = -projection.sourceB;

                return success;
              }
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_GJK
