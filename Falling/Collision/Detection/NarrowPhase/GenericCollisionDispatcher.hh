#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_GENERICCOLLISIONDISPATCHER
# define FALLING_COLLISION_DETECTION_NARROWPHASE_GENERICCOLLISIONDISPATCHER

# include <utility>
# include <unordered_map>

namespace std
{
  template<>
    struct hash<std::pair<int, int>>
    {
      public:
        size_t operator()(const pair<int, int>& s) const 
        {
          size_t h1 = hash<int>()(get<0>(s));
          size_t h2 = hash<int>()(get<1>(s));
          return h1 ^ (h2 << 1); // FIXME: dont know if this is a good hash function…
        }
    };
}; // end std

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        template <typename NF>
          class GenericCollisionDispatcher
          {
            private:
              std::unordered_map<std::pair<int, int>, NF*> _dispatchMap;

            public:
              NF* operator()(int a, int b)
              {
                auto found = _dispatchMap.find(std::make_pair(a, b));

                if (found == _dispatchMap.end())
                  return nullptr;

                return std::get<1>(*found);
              }

              void registerDispatchPairSymetric(int a, int b, NF* nf)
              {
                registerDispatchPairAsymetric(a, b, nf);
                registerDispatchPairAsymetric(b, a, nf);
              }

              void registerDispatchPairAsymetric(int a, int b, NF* nf)
              {
                _dispatchMap[std::make_pair(a, b)] = nf;
              }
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling
#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_GENERICCOLLISIONDISPATCHER
