#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_IMPLICITSHAPEIMPLICITSHAPECOLLISIONDETECTION
# define FALLING_COLLISION_DETECTION_NARROWPHASE_IMPLICITSHAPEIMPLICITSHAPECOLLISIONDETECTION

# include <Falling/Collision/Detection/NarrowPhase/GJK.hh>
# include <Falling/Collision/Detection/NarrowPhase/MinkowskiSampling.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        // FIXME: parametrize by the backup algorithm?
        template <typename Splx, typename SA, typename SB, typename V, typename Err,
                  unsigned int numSamples>
          bool collideImplicitShapeImplicitShape(const SA& sa, const SB& sb, Contact<V>& out)
          {
            static_assert_ImplicitShape((SA), (V));
            static_assert_ImplicitShape((SB), (V));
            static_assert_DotProd((V));
            static_assert_AbelianGroup((V));

            // FIXME: keep the simplex, or the last CSO point in memory
            std::pair<V, V> closestPoints;

            bool success = GJK::closestPoints<Splx, SA, SB, V, Err>(sa, sb, closestPoints);

            double dist = 0.0;
            V      normal;

            if (success)
            {
              normal      = std::get<1>(closestPoints) - std::get<0>(closestPoints);;
              double dist = sqlen(normal);

              if (dist >= Err::margin * Err::margin || dist == 0.0)
              {
                //- std::cout << "failed " << dist << std::endl;
                return false;
              }

              dist = sqrt(dist);
              // we dont use normal /= dist since we cannot assume that
              // normalize(normal) <=> normal /= dist
              normalize(normal);

              std::get<0>(closestPoints) += normal * Err::margin / 2.0;
              std::get<1>(closestPoints) -= normal * Err::margin / 2.0;
              dist = Err::margin - dist;
            }
            else
            {
              // we have a deep penetration, use the alternative algorithm
              closestPoints =
                MinkowskiSampling::projectOrigin<
                  Splx, SA, SB, V, Err, numSamples
                >(sa, sb);

              normal = std::get<0>(closestPoints) - std::get<1>(closestPoints);
              dist   = normalize(normal);

              //- std::cout << "found dist: " << dist << std::endl;
              //- std::cout << "found normal: " << normal << std::endl;
            }

            out.globalContact1   = std::get<0>(closestPoints);
            out.globalContact2   = std::get<1>(closestPoints);
            out.penetrationDepth = dist;
            out.contactNormal    = normal;

            //- std::cout << "Found collision: " << out << std::endl;
            return true;
          }

        template <typename Splx, typename SA, typename SB, typename V, typename Err,
                  typename numSamples>
          Contact<V>* collideImplicitShapeImplicitShape(const SA& sa, const SB& sb)
          {
            auto res = new Contact<V>();

            if (collideImplicitShapeImplicitShape<Splx, SA, SB, V, Err, numSamples>(sa, sb, *res))
              return res;

            delete res;

            return nullptr;
          }
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_IMPLICITSHAPEIMPLICITSHAPECOLLISIONDETECTION
