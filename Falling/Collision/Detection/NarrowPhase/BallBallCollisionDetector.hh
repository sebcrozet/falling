#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_BALLBALLCOLLISIONDETECTOR
# define FALLING_COLLISION_DETECTION_NARROWPHASE_BALLBALLCOLLISIONDETECTOR

# include <Falling/Metal/Inherit.hh>
# include <Falling/Collision/Contact.hh>
# include <Falling/Collision/Detection/NarrowPhase/AbstractNarrowPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/BallBallCollisionDetection.hh>
# include <Falling/Metal/Identifiable.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        using namespace Falling::Shape;

        template <typename V>
          class BallBallCollisionDetector
          : public AbstractNarrowPhase<
              Contact<V>
              , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>

          {
            private:
              Contact<V> _coll;
              bool         _ok;

            public:
              virtual void update(
                  const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type&   sa
                  , const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sb)
              {
                _ok = collideBallBall(*sa.constValue().valueAs<Ball<V>>()
                                      , *sb.constValue().valueAs<Ball<V>>()
                                      , _coll);

                if (!_ok)
                  _coll.clearCache();
              }

              virtual unsigned int numContacts() const
              { return _ok ? 1 : 0; }

              virtual void collisions(std::vector<Contact<V>*>& out)
              {
                if (_ok)
                  out.push_back(&_coll);
              }

              virtual BallBallCollisionDetector<V>* clone() const
              { return new BallBallCollisionDetector(); }
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_BALLBALLCOLLISIONDETECTOR
