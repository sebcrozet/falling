#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_JOHNSONSIMPLEX
# define FALLING_COLLISION_DETECTION_NARROWPHASE_JOHNSONSIMPLEX

# include <limits>
# include <cassert>
# include <tuple>
# include <Falling/Algebra/Transform.hh>
# include <Falling/Algebra/Dimension.hh>
# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/ScalarOp.hh>
# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/Eq.hh>
# include <Falling/Collision/Detection/NarrowPhase/Simplex.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        using namespace Falling::Algebra;

        template <typename V>
          class JohnsonSimplex
          {
            static_assert_Dimension((V));
            static_assert_DotProd((V));
            static_assert_AbelianGroup((V));
            static_assert_ScalarOp((V));
            static_assert_Eq((V));

            public:
            static const int maxDim = DimensionTrait<V>::dim;

            private:
            struct CofactorParams
            {
              double       determinant;
              unsigned int idx[maxDim + 1];       // points indices
              double       barCoords[maxDim + 1]; // barycentric coordinates
              unsigned int numPoints = 0;         // dimension FIXME: or numPoints?

              void invariant()
              {
                assert(numPoints <= maxDim);
                for (unsigned int i = 0; i < numPoints; ++i)
                  assert(idx[i] <= maxDim);
              }
            };

            unsigned _pid[maxDim + 1];
            V        _points[maxDim + 1];
            int      _numPoints;
            unsigned _pidBeforeProjection[maxDim + 1];
            int      _lastNumPoints = 0;
            double   _projectionBarycentricCoordinates[maxDim + 1];

            // NOTE: does not copy the determinant
            static void _copyCofactorParamsTo(const CofactorParams& a, CofactorParams& b)
            {
              b.numPoints = a.numPoints;

              const double*       ba = a.barCoords;
              double*             bb = b.barCoords;
              const unsigned int* ia = a.idx;
              unsigned int*       ib = b.idx;

              for (unsigned i = 0; i <= maxDim; ++i) // FIXME: we could use something smaller than maxDim
              {
                bb[i] = ba[i];
                ib[i] = ia[i];
              }
            }

            unsigned _emptySlot()
            {
              bool used[maxDim + 1];

              for (unsigned i = 0; i <= maxDim; ++i)
                used[i] = false;

              for (unsigned i = 0; i < _numPoints; ++i)
                used[_pid[i]] = true;

              for (unsigned i = 0; i <= maxDim; ++i)
                if (!used[i])
                  return i;

              assert(false && "This simplex has no empty slots.");

              return 0;
            }

            public:
            JohnsonSimplex()
              : _numPoints(0)
            { }

            unsigned int dimension() const
            { 
              assert(_numPoints >= 1
                     && "Cannot evaluate the dimension of a simplex without point.");
              return _numPoints - 1;
            }

            bool containedBeforeProjection(const V& v) const
            {
              for (unsigned i = 0; i < _lastNumPoints; ++i)
              {
                if (_points[_pidBeforeProjection[i]] == v)
                  return true;
              }

              return false;
            }

            void addPoint(const V& v)
            {
              assert(_numPoints <= maxDim);

              unsigned id = _emptySlot();

              _pid[_numPoints++]      = id;
              _points[id]             = v;

              //- std::cout << "Added " << v << " at slot " << id << std::endl;
            }

            double maxSqLen() const
            {
              assert(_numPoints >= 1);

              double max = std::numeric_limits<double>::lowest();

              for (unsigned int i = 0; i < _numPoints; ++i)
              {
                double lsqr = sqlen(_points[_pid[i]]);

                if (max < lsqr)
                  max = lsqr;
              }

              return max;
            }

            double projectionCofactorLength(double* cofactors, const V* pts, unsigned numPts)
            {
              return sqlen(_projectionFromCofactors(cofactors, pts, numPts));
            }

            V projectOrigin()
            {
              assert(_numPoints != 0 && "Cannot compute the cofactor of an empty simplex.");

               //- std::cout << "#### starting projection on a " << dimension() << " simplex." << std::endl;

              _lastNumPoints = _numPoints;

              for (unsigned i = 0; i < _lastNumPoints; ++i)
                _pidBeforeProjection[i] = _pid[i];

              CofactorParams subCofactors[maxDim + 1];

              if (_numPoints == 1)
              {
                _projectionBarycentricCoordinates[0] = 1.0;

                return _points[_pid[0]];
              }

              // compute sub-cofactors
              for (unsigned int i = 0; i < _numPoints; ++i)
              {
                 //- std::cout << "### Creating new subsimplex." << std::endl;
                bool shift = 0;

                for (unsigned int j = 0; j < _numPoints; ++j)
                {
                  if (i != j)
                  {
                     //- std::cout << "PID: " << _pid[j] << std::endl;
                    subCofactors[i].idx[j - (shift ? 1 : 0)] = _pid[j];
                  }
                  else
                    shift = true;
                }

                subCofactors[i].numPoints = _numPoints - 1;

                _cofactor(_pid[i], subCofactors[i], 0);
              }
               //- std::cout << "### End of subsimplices creation." << std::endl;

              // check if the origin projects on this sub-simplex
              bool allPositive = true;
              for (unsigned i = 0; i < _numPoints; ++i)
              {
                if (subCofactors[i].determinant <= 0.0)
                {
                  allPositive = false;
                  break;
                }
              }

              if (allPositive)
              {
                // Projection found on this simplex
                double determinant = 0.0;

                for (unsigned i = 0; i < _numPoints; ++i)
                  determinant += subCofactors[i].determinant;

                for (unsigned i = 0; i < _numPoints; ++i)
                {
                  _projectionBarycentricCoordinates[i] = subCofactors[i].determinant / determinant;
                   //- std::cout << "[1] Copying bar coord: " << _projectionBarycentricCoordinates[i] << std::endl;
                }
                auto res = _projectionFromBarCoords(_projectionBarycentricCoordinates
                                                    , _pid
                                                    , _numPoints);
                 //- std::cout << "Projection without dimension reduction found: " << res << std::endl;

                return res;
              }
              else
              {
                // there is no projection of this sub-simplex
                // find which sub-simplex was best
                // FIXME: this is not the right(ly efficient) way to implement johnson’s
                // sub-algorithm.
                unsigned int best       = 0;
                double       bestLength = std::numeric_limits<double>::max();

                for (unsigned int i = 0; i < _numPoints; ++i)
                {
                  double candidate = len(_projectionFromCofactors(subCofactors[i].barCoords
                                                                  , subCofactors[i].idx
                                                                  , subCofactors[i].numPoints));

                  if (candidate < bestLength)
                  {
                    best       = i;
                    bestLength = candidate;
                  }
                }

                // we found the best, copy back infos
                // FIXME: save datas from subCofactor[best] ?
                _numPoints = subCofactors[best].numPoints;

                double bestDeterminant = 0.0;

                for (unsigned i = 0; i < _numPoints; ++i)
                  bestDeterminant += subCofactors[best].barCoords[i];

                for (unsigned i = 0; i < _numPoints; ++i)
                {
                  _pid[i] = subCofactors[best].idx[i];
                  _projectionBarycentricCoordinates[i] =
                    subCofactors[best].barCoords[i] / bestDeterminant;
                   //- std::cout << "[2] Copying bar coord: " << _projectionBarycentricCoordinates[i] << " " << subCofactors[best].determinant << " |-> " << bestDeterminant << std::endl;
                }

                // FIXME: use _projectionFromBarCoords with _projectionBarycentricCoordinates?
                auto res = _projectionFromCofactors(subCofactors[best].barCoords
                                                    , subCofactors[best].idx
                                                    , subCofactors[best].numPoints);
                 //- std::cout << "Projection with dimension reduciton found: " << res << std::endl;

                return res;
              }
            }

            void _printCallDepthPrefix(unsigned int depth) const
            {
               //- for (unsigned int i = 0; i <= depth; ++i)
               //-   std::cout << "----";
               //- std::cout << "> ";
            }

            /*
             * Returns < cofactor value, sub-simplex dimension >
             */
            void _cofactor(unsigned int      idrm
                           , CofactorParams& thisCofactorParams
                           , unsigned int    callDepth) const
            {
              thisCofactorParams.invariant();

              _printCallDepthPrefix(callDepth);
               //- std::cout << "Entering cofactor of dimension "
               //-           << thisCofactorParams.numPoints
               //-           << std::endl;

              if (thisCofactorParams.numPoints == 0)
              {
                thisCofactorParams.barCoords[0] = 1.0;
                thisCofactorParams.numPoints    = 1;
                thisCofactorParams.determinant  = 1.0;
                thisCofactorParams.idx[0]       = idrm;

                _printCallDepthPrefix(callDepth);
                //- std::cout << "Trivial case returned: " << _points[thisCofactorParams.idx[0]] 
                //-           << " at id " << thisCofactorParams.idx[0] << std::endl;
              }
              else
              {
                unsigned int   currNumPoints = thisCofactorParams.numPoints;
                CofactorParams subCofactors[maxDim + 1];

                V rmp1 = _points[thisCofactorParams.idx[0]] - _points[idrm];

                // compute sub-cofactors
                for (unsigned i = 0; i < currNumPoints; ++i)
                {
                  bool shift = 0;

                  for (unsigned j = 0; j < currNumPoints; ++j)
                  {
                    if (i != j)
                      subCofactors[i].idx[j - (shift ? 1 : 0)] = thisCofactorParams.idx[j];
                    else
                      shift = true;
                  }

                  subCofactors[i].numPoints = currNumPoints - 1;
                  _cofactor(thisCofactorParams.idx[i], subCofactors[i], callDepth + 1);
                }

                double currCofactor = 0.0;

                // compute this cofactor
                for (unsigned i = 0; i < currNumPoints; ++i)
                {
                  _printCallDepthPrefix(callDepth);
                  //- std::cout << subCofactors[i].determinant << " * " << "dot " << rmp1 << " " << _points[thisCofactorParams.idx[i]] << std::endl;

                  currCofactor += subCofactors[i].determinant
                                  * dot(rmp1, _points[thisCofactorParams.idx[i]]);
                }

                // check if the origin projects on this sub-simplex
                bool allPositive = true;

                _printCallDepthPrefix(callDepth);
                 //- std::cout << "Projecting on: " << std::endl;
                for (unsigned i = 0; i < currNumPoints; ++i)
                {
                  _printCallDepthPrefix(callDepth);
                   //- std::cout << "   " << _points[thisCofactorParams.idx[i]] << std::endl;
                }
                _printCallDepthPrefix(callDepth);
                 //- std::cout << "Found cofactor: " << currCofactor << std::endl;

                for (unsigned i = 0; i < currNumPoints; ++i)
                {
                  if (subCofactors[i].determinant <= 0.0)
                  {
                    allPositive = false;
                    break;
                  }
                }

                // store this cofactor
                thisCofactorParams.determinant = currCofactor;

                if (allPositive)
                {
                  _printCallDepthPrefix(callDepth);
                   //- std::cout << "Projection on this subsimplex found." << std::endl;
                  for (unsigned i = 0; i < currNumPoints; ++i)
                    thisCofactorParams.barCoords[i] = subCofactors[i].determinant;
                  // no need to change thisCofactorParams.idx
                  // no need to change thisCofactorParams.numPoints
                }
                else
                {
                  // there is no projection of this sub-simplex
                  // find which sub-simplex was best
                  // FIXME: this is not the right(ly efficient) way to implement johnson’s
                  // sub-algorithm.
                  unsigned int best       = 0;
                  double       bestLength = std::numeric_limits<double>::max();

                  for (unsigned int i = 0; i < currNumPoints; ++i)
                  {
                    double candidate = len(
                        _projectionFromCofactors(
                          subCofactors[i].barCoords
                          , subCofactors[i].idx
                          , subCofactors[i].numPoints));

                    if (candidate < bestLength)
                    {
                      best = i;
                      bestLength = candidate;
                    }
                  }

                  _printCallDepthPrefix(callDepth);
                  //- std::cout << "No projection on this subsimplex found. Finding a smaller one." << std::endl;
                   //- std::cout << "Projection on subsimplex " << subCofactors[best].numPoints << " found" << std::endl;
                  // we found the best, copy back infos
                  _copyCofactorParamsTo(subCofactors[best], thisCofactorParams);
                  thisCofactorParams.determinant = -42.0; // prevent any projections on the parent
                }
              }
            }

            private:
            V _projectionFromCofactors(const double*         cofactors
                                       , const unsigned int* pts
                                       , int                 numpts) const
            {
              V      res;
              double determinant = 0.0;

              for (unsigned i = 0; i < numpts; ++i)
                determinant += cofactors[i];

              for (unsigned i = 0; i < numpts; ++i)
                res += _points[pts[i]] * cofactors[i] / determinant;

              return res;
            }

            V _projectionFromBarCoords(const double*         barCoords
                                       , const unsigned int* pts
                                       , int                 numpts) const
            {
              V res;

              for (unsigned i = 0; i < numpts; ++i)
                res += barCoords[i] * _points[pts[i]];

              return res;
            }

            public:
            std::ostream& operator<<(std::ostream& stream) const
            {
              stream << "JohnsonSimplex { "
                << " dimension = " << dimension()
                << ", points   = [";

              for (unsigned int i = 0; i < _numPoints; ++i)
                stream << " " << _points[_pid[i]] << ",";

              stream << "]" << "}" << std::endl;

              return stream;
            }
          };

        template <typename V>
          struct Simplex<JohnsonSimplex<V>, V>
          {
            static const bool value = true;

            static unsigned int dimension(const JohnsonSimplex<V>& simplex)
            { return simplex.dimension(); }

            static bool containsPoint(const JohnsonSimplex<V>& simplex, const V& pt)
            { return simplex.containedBeforeProjection(pt); }

            static void addPoint(JohnsonSimplex<V>& simplex, const V& point)
            { simplex.addPoint(point); }

            static V projectOrigin(JohnsonSimplex<V>& simplex)
            { return simplex.projectOrigin(); }

            static double maxSqLen(const JohnsonSimplex<V>& simplex)
            { return simplex.maxSqLen(); }
          };

        template <typename V>
          std::ostream& operator<<(std::ostream& stream, const JohnsonSimplex<V>& simplex)
          { return simplex.operator<<(stream); }
      } // end NarrowPhase
    } // end Detection
  } // end Collision

  namespace Algebra
  {
    template <typename V>
    struct DimensionTrait<Collision::Detection::NarrowPhase::JohnsonSimplex<V>>
    {
      static const bool value = true;

      static const unsigned int dim = Collision::Detection::NarrowPhase::JohnsonSimplex<V>::maxDim;
    };
  };
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_JOHNSONSIMPLEX
