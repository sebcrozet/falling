#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_DEFAULTCOLLISIONDISPATCHER
# define FALLING_COLLISION_DETECTION_NARROWPHASE_DEFAULTCOLLISIONDISPATCHER

// FIXME: should this file be on Falling/Shape/. ?

# include <Falling/Shape/Box/Box.hh>
# include <Falling/Shape/TransformedShape/TransformedShape.hh>
# include <Falling/Collision/Detection/NarrowPhase/GenericCollisionDispatcher.hh>
# include <Falling/Collision/Detection/NarrowPhase/EmptyCollisionDetector.hh>
# include <Falling/Collision/Detection/NarrowPhase/BallBallCollisionDetector.hh>
# include <Falling/Collision/Detection/NarrowPhase/PlaneImplicitShapeCollisionDetector.hh>
# include <Falling/Collision/Detection/NarrowPhase/ImplicitShapeImplicitShapeCollisionDetector.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        using namespace Falling::Shape;
        using namespace Falling::Algebra;

        // FIXME: is inheritence the right choice?
        template <typename V, typename T, typename Splx, typename Err
                  , unsigned int numSamples>
          class DefaultCollisionDispatcher
          : public GenericCollisionDispatcher<
              AbstractNarrowPhase<Contact<V>
                                  , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>>
          {
            public:
              using NarrowPhaseType = AbstractNarrowPhase<
                                        Contact<V>
                                        , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>;

            private:
              using Parent = GenericCollisionDispatcher<NarrowPhaseType>;

              NarrowPhaseType* _defaultDetector = nullptr;

            public:

              DefaultCollisionDispatcher()
              {
                using namespace Metal::Static;

                Parent::registerDispatchPairSymetric(IdentifiableTrait<Ball<V>>::identifier(),
                                                     IdentifiableTrait<Ball<V>>::identifier(),
                                                     new BallBallCollisionDetector<V>());

                Parent::registerDispatchPairSymetric(IdentifiableTrait<Plane<V>>::identifier(),
                                                     IdentifiableTrait<Ball<V>>::identifier(),
                                                     new PlaneImplicitShapeCollisionDetector<Ball<V>, V>());

                using TBox = TransformedShape<Box<V>, T>;
                Parent::registerDispatchPairSymetric(IdentifiableTrait<Plane<V>>::identifier(),
                                                     IdentifiableTrait<TBox>::identifier(),
                                                     new PlaneImplicitShapeCollisionDetector<TBox, V>());

                Parent::registerDispatchPairAsymetric(
                    IdentifiableTrait<TBox>::identifier(),
                    IdentifiableTrait<TBox>::identifier(),
                    new ImplicitShapeImplicitShapeCollisionDetector<
                          Splx, TBox, TBox, V, Err, numSamples>());
              }

              NarrowPhaseType* operator()(
                  const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type*   a
                  , const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type* b)
              {
                auto res = GenericCollisionDispatcher<NarrowPhaseType>::operator()(
                    a->identifier(),
                    b->identifier());

                if (res)
                  return res->clone();

                return _defaultDetector;
              }
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling
#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_DEFAULTCOLLISIONDISPATCHER
