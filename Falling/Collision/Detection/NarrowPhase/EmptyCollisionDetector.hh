#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_EMPTYCOLLISIONDETECTOR
# define FALLING_COLLISION_DETECTION_NARROWPHASE_EMPTYCOLLISIONDETECTOR

# include <Falling/Metal/Inherit.hh>
# include <Falling/Collision/Detection/NarrowPhase/AbstractNarrowPhase.hh>
# include <Falling/Metal/Identifiable.hh>
# include <Falling/Collision/Contact.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        using namespace Falling::Shape;

        template <typename V>
          class EmptyCollisionDetector
          : public AbstractNarrowPhase<
              Contact<V>
              , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>

          {
            public:
              virtual void update(
                  const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sa
                  , const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sb)
              {
                assert(std::cout << "[Falling] Warning: updating an empty narrow phase." << std::endl
                       || true);
              }

              virtual unsigned int numContacts() const
              { return 0; }

              virtual void collisions(std::vector<Contact<V>*>& out)
              { }

              virtual EmptyCollisionDetector<V>* clone() const
              { return new EmptyCollisionDetector(); }
          };
        ;
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling


#endif // end FALLING_COLLISION_DETECTION_NARROWPHASE_EMPTYCOLLISIONDETECTOR
