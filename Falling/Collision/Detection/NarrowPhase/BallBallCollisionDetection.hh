#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_BALLBALLCOLLISIONDETECTION
# define FALLING_COLLISION_DETECTION_NARROWPHASE_BALLBALLCOLLISIONDETECTION

# include <Falling/Shape/Ball/Ball.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        using namespace Falling::Shape;
        using namespace Falling::Collision;
        using namespace Falling::Algebra;

        template <typename V>
          bool collideBallBall(const Ball<V>&   ball1
                               , const Ball<V>& ball2
                               , Contact<V>&    out)
          {
            static_assert_AbelianGroup((V));;
            static_assert_DotProd((V));
            static_assert_ScalarOp((V));

            auto c1 = ball1.center();
            auto c2 = ball2.center();
            auto r1 = ball1.radius();
            auto r2 = ball2.radius();

            auto deltaPos  = c2 - c1;
            auto sqDist    = sqlen(deltaPos);
            auto sumRadius = r1 + r2;

            if (sqDist < sumRadius * sumRadius)
            {
              double dist = sqrt(sqDist);

              out.penetrationDepth = sumRadius - dist;
              out.contactNormal    = deltaPos / dist;
              out.globalContact1   = c1 + r1 * out.contactNormal;
              out.globalContact2   = c2 - r2 * out.contactNormal;

              return true;
            }

            return false;
          }

        template <typename V>
          Contact<V>* collideBallBall(const Ball<V>& ball1, const Ball<V>& ball2)
          {
            auto res = new Contact<V>();

            if (collideBallBall(ball1, ball2, *res))
              return res;

            delete res;

            return nullptr;
          }
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_BALLBALLCOLLISIONDETECTION
