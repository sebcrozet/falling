#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_PLANEIMPLICITSHAPECOLLISIONDETECTION
# define FALLING_COLLISION_DETECTION_NARROWPHASE_PLANEIMPLICITSHAPECOLLISIONDETECTION

# include <Falling/Algebra/DotProd.hh>
# include <Falling/Algebra/ScalarOp.hh>
# include <Falling/Shape/ImplicitShape.hh>
# include <Falling/Shape/Plane/Plane.hh>
# include <Falling/Collision/Contact.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {

        template <typename G, typename V>
          bool collidePlaneImplicitShape(const Shape::Plane<V>& plane
                                         , const G&             other
                                         , Contact<V>&          out)
          {
            using namespace Falling::Shape;
            using namespace Falling::Collision;
            using namespace Falling::Algebra;

            // FIXME: improve error messages
            static_assert_ImplicitShape((G), (V));
            static_assert_AbelianGroup((V));
            static_assert_DotProd((V));
            static_assert_ScalarOp((V));

            V deepestPoint = supportPoint(other, -plane.normal());

            double dist = dot(plane.center() - deepestPoint, plane.normal());

            if (dist > 0.0)
            {
              out.globalContact1   = deepestPoint + dist * plane.normal();
              out.globalContact2   = deepestPoint;
              out.contactNormal    = plane.normal();
              out.penetrationDepth = dist;

              return true;
            }

            return false;
          }

        template <typename G, typename V>
          Contact<V>* collidePlaneImplicitShape(const Shape::Plane<V>& plane, const G& other)
          {
            using namespace Falling::Shape;
            using namespace Falling::Collision;
            using namespace Falling::Algebra;

            auto res = new Contact<V>();

            if (collidePlaneImplicitShape(plane, other, *res))
              return res;

            delete res;

            return nullptr;
          }
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_PLANEIMPLICITSHAPECOLLISIONDETECTION
