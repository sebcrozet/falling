#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_NARROWPHASE
# define FALLING_COLLISION_DETECTION_NARROWPHASE_NARROWPHASE

# include <type_traits>
# include <vector>
# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
# define static_assert_NarrowPhase(TYPENF, TYPECD, TYPES)               \
         static_assert(Falling::Collision::Detection::NarrowPhaseTrait< \
             typename RMPAR(TYPENF)                                     \
             , typename RMPAR(TYPECD)                                   \
             , typename RMPAR(TYPES)>::value                            \
             , "Type parameter " #TYPENF " must be a narrow phase with collision datas " #TYPECD \
               " and running on shape with type " #TYPES )
        template <typename NF, typename CD, typename S, typename Enabler = void> // FIXME: make CD and S internal typedefs!
        struct NarrowPhaseTrait
        {
          static const bool value = false;

          static void update(NF*, const S&, const S&);
          static unsigned int numCollisions(const NF&);
          static void collisions(NF*, std::vector<CD*>&);
        };

        // FIXME: all those functions will not type-check unless we make CD and S internal typedefs
        // (fundepts).
        template <typename NF, typename CD, typename S
                  , BIND_CONCEPT(NFT, (NarrowPhaseTrait<NF, CD, S>))>
          void update(NF* nf, const S& s1, const S& s2)
          {
            NFT::update(nf, s1, s2);
          }

        template <typename NF, typename CD, typename S
                  , BIND_CONCEPT(NFT, (NarrowPhaseTrait<NF, CD, S>))>
          unsigned int numCollisions(const NF& nf)
          {
            return NFT::numCollisions(nf);
          }

        template <typename NF, typename CD, typename S
                  , BIND_CONCEPT(NFT, (NarrowPhaseTrait<NF, CD, S>))>
          void collisions(NF* nf, std::vector<CD*>& out)
          {
            NFT::collisions(nf, out);
          }

      }; // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_NARROWPHASE
