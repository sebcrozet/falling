#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_PLANEIMPLICITSHAPECOLLISIONDETECTOR
# define FALLING_COLLISION_DETECTION_NARROWPHASE_PLANEIMPLICITSHAPECOLLISIONDETECTOR

# include <Falling/Metal/Inherit.hh>
# include <Falling/Collision/Detection/NarrowPhase/AbstractNarrowPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/PlaneImplicitShapeCollisionDetection.hh>
# include <Falling/Metal/Identifiable.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        template <typename G, typename V>
          class PlaneImplicitShapeCollisionDetector
                : public AbstractNarrowPhase<Contact<V>
                                             , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>
          {
            private:
              Contact<V> _coll;
              bool         _ok;

            public:
            virtual void update(const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sa
                                , const Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type& sb)
            {
              if (sa.identifier() == Metal::Static::IdentifiableTrait<Plane<V>>::identifier())
              {
                _ok = collidePlaneImplicitShape(*sa.constValue().valueAs<Plane<V>>()
                                                , *sb.constValue().valueAs<G>()
                                                , _coll);
              }
              else
              {
                _ok = collidePlaneImplicitShape(*sb.constValue().valueAs<Plane<V>>()
                                                , *sa.constValue().valueAs<G>()
                                                , _coll);

                if (_ok)
                  _coll.revert();
              }

              if (!_ok)
                _coll.impulse = 0.0;
            }

            virtual unsigned int numContacts() const
            {
              return _ok ? 1 : 0;
            }

            virtual void collisions(std::vector<Contact<V>*>& out)
            {
              if (_ok)
                out.push_back(&_coll);
            }

            virtual PlaneImplicitShapeCollisionDetector* clone() const
            { return new PlaneImplicitShapeCollisionDetector(); }
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_PLANEIMPLICITSHAPECOLLISIONDETECTOR
