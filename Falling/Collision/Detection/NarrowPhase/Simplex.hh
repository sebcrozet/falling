#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_SIMPLEX
# define FALLING_COLLISION_DETECTION_NARROWPHASE_SIMPLEX

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        template <typename S, typename V, typename Enabler = void>
          struct Simplex
          {
            static const bool value = false;

            static bool     containsPoint(const S&, const V&);
            static void     addPoint(S&, const V&);
            static V        projectOrigin(S&);
            static double   maxSqLen(const S&);
            static unsigned dimension(const S&);
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_SIMPLEX
