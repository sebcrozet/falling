#ifndef FALLING_COLLISION_DETECTION_NARROWPHASE_ABSTRACTNARROWPHASE
# define FALLING_COLLISION_DETECTION_NARROWPHASE_ABSTRACTNARROWPHASE

# include <Falling/Collision/Detection/NarrowPhase/NarrowPhase.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace NarrowPhase
      {
        template <typename CD, typename S>
        class AbstractNarrowPhase
        {
          public:
            virtual void                 update(const S& sa, const S& sb)        = 0;
            virtual unsigned int         numContacts()                     const = 0;
            virtual void                 collisions(std::vector<CD*>& out)       = 0;
            virtual AbstractNarrowPhase* clone()                           const = 0;
            virtual ~AbstractNarrowPhase()
            { }
        };

        template <typename CD, typename S>
          struct NarrowPhaseTrait<AbstractNarrowPhase<CD, S>, CD, S>
          {
            static const bool value = true;

            static void update(AbstractNarrowPhase<CD, S>* nf, const S& sa, const S& sb)
            { nf->update(sa, sb); }

            static unsigned int numContacts(const AbstractNarrowPhase<CD, S>& nf)
            { return nf.numContacts(); }

            static void collisions(AbstractNarrowPhase<CD, S>* nf, std::vector<CD*>& out)
            { nf->collisions(out); }
          };
      } // end NarrowPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_NARROWPHASE_ABSTRACTNARROWPHASE
