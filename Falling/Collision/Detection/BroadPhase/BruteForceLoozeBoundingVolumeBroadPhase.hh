#ifndef FALLING_COLLISION_DETECTION_BROADPHASE_BRUTEFORCELOOZEBOUNDINGVOLUMEBROADPHASE
# define FALLING_COLLISION_DETECTION_BROADPHASE_BRUTEFORCELOOZEBOUNDINGVOLUMEBROADPHASE

# include <algorithm>
# include <unordered_map>

# include <Falling/Metal/HasProxy.hh>
# include <Falling/Metal/Empty.hh>
# include <Falling/Collision/Detection/BroadPhase/BroadPhase.hh>
# include <Falling/BoundingVolume/HasBoundingVolume.hh>
# include <Falling/BoundingVolume/LoozeBoundingVolume.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace BroadPhase
      {
        template <typename RB, typename BV>
          struct BoundingVolumeMap
          {
            protected:
              std::unordered_map<const RB*, BV> _map;
          };

        template <typename BV>
          struct LoozeBoundingVolumeProxy
          {
            static_assert_LoozeBoundingVolume((BV));

            BV loozeBoundingVolume;
          };

        using namespace BoundingVolume::Static;
        using namespace BoundingVolume;

        template <typename RB, typename BV>
          class BruteForceLoozeBoundingVolumeBroadPhase
            : std::conditional<
                Metal::HasProxyTrait<LoozeBoundingVolumeProxy<BV>, RB>::value
                , Metal::Empty                     // there is a proxy, nothing to add
                , BoundingVolumeMap<RB, BV>>::type // there is no proxy, add a map to keep track of the bounding volumes
          {
            static_assert_HasBoundingVolume((RB), (BV));
            static_assert_BoundingVolume((BV));
            static_assert_LoozeBoundingVolume((BV));

            public:
            friend struct BroadPhaseTrait<BruteForceLoozeBoundingVolumeBroadPhase<RB, BV>, RB>;

            private:
              using Proxy = LoozeBoundingVolumeProxy<BV>;

              std::vector<RB*> _bodies;
              std::vector<RB*> _panding;
              double           _margin;

              // version with constant-time lookup
              template <typename T>
                typename std::enable_if<Metal::HasProxyTrait<Proxy, T>::value, BV>::type&
                _boundingVolumeOf(RB& b)
                { return Metal::proxy<LoozeBoundingVolumeProxy<BV>>(b).loozeBoundingVolume; }

              // version with logarithmic-time lookup. Used when the proxy is not available
              template <typename T>
                typename std::enable_if<!Metal::HasProxyTrait<Proxy, T>::value, BV>::type&
                _boundingVolumeOf(RB& b)
                { return BoundingVolumeMap<RB, BV>::_map[&b]; }

            public:

              explicit BruteForceLoozeBoundingVolumeBroadPhase(double margin)
                : _margin(margin)
              { }

              double margin() const
              { return _margin; }

              const std::vector<RB*>& bodies() const
              { return _bodies; }

              BV& boundingVolumeOf(RB& b)
              { return _boundingVolumeOf<RB>(b); }
          };

        template <typename RB, typename BV>
          struct BroadPhaseTrait<BruteForceLoozeBoundingVolumeBroadPhase<RB, BV>, RB>
          {
            static const bool value = true;

            using BF = BruteForceLoozeBoundingVolumeBroadPhase<RB, BV>;

            static void addBody(BF& bf, RB* rb)
            {
              assert(std::find(bf._bodies.begin(), bf._bodies.end(), rb) == bf._bodies.end());

              bf._bodies.push_back(rb);
              bf._panding.push_back(rb);

              // update the bounding volume
              loozen(bf.margin()
                     , HasBoundingVolumeTrait<RB, BV>::boundingVolume(*rb)
                     , bf.boundingVolumeOf(*rb));
            }

            static void removeBody(BF& bf, RB* rb)
            {
              assert(false && "Not yet implemented.");
            }

            static std::vector<std::pair<RB*, RB*>> getCollisionsPairs(
                BF&                       bf
                , const std::vector<RB*>& bodies)
            {
              std::vector<std::pair<RB*, RB*>> res;
              std::vector<RB*>                 updated;

              for (auto body : bodies)
              {
                const BV& bv  = HasBoundingVolumeTrait<RB, BV>::boundingVolume(*body);
                BV&       lbv = bf.boundingVolumeOf(*body);

                if (!contain(lbv, bv)) // need update!
                {
                  loozen(bf.margin(), bv, lbv);
                  updated.push_back(body);
                }
              }

              for (auto panding : bf._panding)
              {
                if (std::find(updated.begin(), updated.end(), panding) == updated.end())
                  updated.push_back(panding);
              }

              for (auto body : updated)
              {
                BV& bv1 = bf.boundingVolumeOf(*body);

                for (auto body2 : bf._bodies)
                {
                  if (body != body2)
                  {
                    BV& bv2 = bf.boundingVolumeOf(*body2);

                    if (intersect(bv1, bv2))
                      res.push_back(std::make_pair(body, body2));
                  }
                }
              }

              bf._panding.clear();

              return res;
            }
          };
      } // end BroadPhase
    } // end Detection
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_DETECTION_BROADPHASE_BRUTEFORCELOOZEBOUNDINGVOLUMEBROADPHASE
