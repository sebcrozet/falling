#ifndef FALLING_COLLISION_DETECTION_BROADPHASE_BROADPHASE
# define FALLING_COLLISION_DETECTION_BROADPHASE_BROADPHASE

# include <utility>
# include <vector>
# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace BroadPhase
      {
# define static_assert_BroadPhase(TYPEBF, TYPERB)                                    \
         static_assert(Falling::Collision::Detection::BroadPhase::BroadPhaseTrait<   \
             typename RMPAR(TYPEBF), typename RMPAR(TYPERB)>::value                  \
                       , "Type parameter " #TYPEBF " must be a broad phase with bodies of type " #TYPERB ".")
        template <typename BF, typename RB, typename Enabler = void>
          struct BroadPhaseTrait
          {
            static const bool value = false;

            static void                             addBody(BF&, RB*);
            static void                             removeBody(BF&, RB*);
            static std::vector<std::pair<RB*, RB*>> getCollisionsPairs(BF&,
                                                                       const std::vector<RB*>);
          };

        template <typename BF, typename RB, BIND_CONCEPT(BPT, (BroadPhaseTrait<BF, RB>))>
          void addBody(BF& bf, RB* rb)
          {
            BPT::addBody(bf, rb);
          }

        template <typename BF, typename RB, BIND_CONCEPT(BPT, (BroadPhaseTrait<BF, RB>))>
          void removeBody(BF& bf, RB* rb)
          {
            BPT::removeBody(bf, rb);
          }

        template <typename BF, typename RB, BIND_CONCEPT(BPT, (BroadPhaseTrait<BF, RB>))>
            std::vector<std::pair<RB*, RB*>> getCollisionsPairs(BF&                     bf,
                                                                const std::vector<RB*>& bs)
            {
              return BPT::getCollisionsPairs(bf, bs);
            }
      } // end BroadPhase
    } // end Detection
  } // end Collision
} // end Falling

# endif // FALLING_COLLISION_DETECTION_BROADPHASE_BROADPHASE
