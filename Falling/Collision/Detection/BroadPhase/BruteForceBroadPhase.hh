#ifndef FALLING_COLLISION_DETECTION_BROADPHASE_BRUTEFORCEBROADPHASE
# define FALLING_COLLISION_DETECTION_BROADPHASE_BRUTEFORCEBROADPHASE

# include <Falling/Collision/Detection/BroadPhase/BroadPhase.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace BroadPhase
      {
        template <typename RB>
          class BruteForceBroadPhase
          {
            friend BroadPhaseTrait<BruteForceBroadPhase<RB>, RB>;

            private:
              std::vector<RB*> _bodies;
              std::vector<RB*> _pandingBodies;
          };

        template <typename RB>
          struct BroadPhaseTrait<BruteForceBroadPhase<RB>, RB>
          {
            static const bool value = true;

            static void addBody(BruteForceBroadPhase<RB>& bf, RB* body)
            {
              bf._pandingBodies.push_back(body);
            }

            std::vector<std::pair<RB*, RB*>>
              static getCollisionsPairs(BruteForceBroadPhase<RB>& bf, const std::vector<RB*>)
              {
                std::vector<std::pair<RB*, RB*>> pairs;

                for (auto body1 : bf._pandingBodies)
                {
                  for (auto body2 : bf._bodies)
                    pairs.push_back(std::make_pair(body1, body2));
                  for (auto body2 : bf._pandingBodies)
                    if (body1 != body2)
                      pairs.push_back(std::make_pair(body1, body2));
                }

                for (auto b : bf._pandingBodies)
                  bf._bodies.push_back(b);

                bf._pandingBodies.clear();

                return pairs;
              }

            static void removeBody(BruteForceBroadPhase<RB>& bf, RB* body)
            {
              auto pos = std::find(bf._bodies, body);

              if (pos != bf._bodies.end())
                bf._bodies.erase(pos);
              else
                bf._pandingBodies.erase(std::find(bf._pandingBodies, body));
            }
          };
      } // end BroadPhase
    } // end Detection
  } // end Collision
} // end Falling


#endif // FALLING_COLLISION_DETECTION_BROADPHASE_BRUTEFORCEBROADPHASE
