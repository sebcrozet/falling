#ifndef FALLING_COLLISION_DETECTION_BROADPHASE_DYNAMICLOOZEBOUNDINGVOLUMEHIERARCHYBROADPHASE
# define FALLING_COLLISION_DETECTION_BROADPHASE_DYNAMICLOOZEBOUNDINGVOLUMEHIERARCHYBROADPHASE

# include <cassert>
# include <Falling/Allocator/Recycler.hh>
# include <Falling/Shape/HasCenterOfMass.hh>

namespace Falling
{
  namespace Collision
  {
    namespace Detection
    {
      namespace BroadPhase
      {
        using namespace BoundingVolume::Static;
        using namespace BoundingVolume;

        template <typename BV, typename RB>
          struct BVHTreeNode; // fwd

        template <typename RB, typename BV>
          struct BVHTreeNodeMap
          {
            protected:
              std::unordered_map<const RB*, BVHTreeNode<BV, RB>*> _map;
          };

        template <typename BV>
          struct BVHTreeNodeProxy
          {
            private:
              static_assert_BoundingVolume((BV));
              static_assert_LoozeBoundingVolume((BV));

              // use a generic type checked at runtime
              Metal::Empty* _treeNode; // the exact type of this  is BVHTreeNode<BV, RB>*

            public:
              BVHTreeNodeProxy()
                : _treeNode(nullptr)
              { }

              template <typename RB>
                explicit BVHTreeNodeProxy(BVHTreeNode<BV, RB>* treeNode)
                { setTreeNode(treeNode); }

              // we do that to avoid circular dependencies between BVHTreeNodeProxy and the
              // RigidBody type
              template <typename RB>
                BVHTreeNode<BV, RB>* treeNode() const
                {
                  assert((!_treeNode || dynamic_cast<BVHTreeNode<BV, RB>*>(_treeNode)));

                  return static_cast<BVHTreeNode<BV, RB>*>(_treeNode);
                }

              template <typename RB>
                void setTreeNode(BVHTreeNode<BV, RB>* treeNode)
                { _treeNode = treeNode; }
          };

        template <typename BV, typename RB>
        struct BVHTreeNode : public Metal::Empty // to be able to do a dynamic Cast
        {
          enum UpdateState {
            NeedsShrink
            , NeedsEnlarge
            , UpToDate
          };

          static_assert_HasCenterOfMass((BV));

          BV                                           boundingVolume;
          typename Shape::HasCenterOfMass<BV>::CenterOfMassType center;
          RB*                                          body        = nullptr;
          BVHTreeNode<BV, RB>*                         left        = nullptr;
          BVHTreeNode<BV, RB>*                         right       = nullptr;
          BVHTreeNode<BV, RB>*                         parent      = nullptr;
          UpdateState                                  updateState = UpToDate;

          BVHTreeNode<BV, RB>* unlink(
              Allocator::Recycler<BVHTreeNode<BV, RB>>& allocator
              , BVHTreeNode<BV, RB>*                    currentRoot
              )
          {
            // leafy variant
            assert(left == nullptr && right == nullptr);

            if (parent)
            {
              auto other = this == parent->right ? parent->left : parent->right;

              other->parent = parent->parent;

              if (parent->parent)
              {
                if (parent->parent->right == parent)
                  parent->parent->right = other;
                else
                {
                  assert(parent->parent->left == parent);
                  parent->parent->left = other;
                }

                other->parent->updateState = NeedsShrink;
              }
              else
                currentRoot = other; // new root

              other->invariant();

              parent->left   = nullptr; // FIXME: useful?
              parent->right  = nullptr; // FIXME: useful?
              parent->body   = nullptr; // FIXME: useful?
              parent->parent = nullptr; // FIXME: useful?
              allocator.free(parent);

              if (parent == currentRoot)
                return other;
            }
            else // the resulting tree is the null tree
              currentRoot = nullptr;

            return currentRoot;
          }

          void invariant() const
          {
            assert(!parent || parent->right == this || parent->left == this);
            assert((!body && right && left) || (body && !right && !left));
            assert(this != parent && this != right && this != left);
          }

          void deepInvariant() const
          {
            invariant();

            if (right)
            {
              assert(contain(boundingVolume, right->boundingVolume));
              assert(contain(boundingVolume, left->boundingVolume));
              right->deepInvariant();
              left->deepInvariant();
            }
          }

          ~BVHTreeNode()
          {
            if (right)
              delete right;

            if (left)
              delete left;
          }

          int depth() const
          {
            if (!right)
              return 1;

            return std::max(1 + right->depth(), 1 + left->depth());
          }

          int numLeaves() const
          {
            if (!right)
              return 1;

            return right->numLeaves() + left->numLeaves();
          }

          static void updateNode(BVHTreeNode<BV, RB>* node)
          {
            if (!node->body)
            {
              // std::cout << "Merging: " << node->left << " " << node->right << " " << node->left->boundingVolume << " and " << node->right->boundingVolume << std::endl;
              merge(node->left->boundingVolume
                    , node->right->boundingVolume
                    , node->boundingVolume);
            }
            node->center      = Shape::centerOfMass(node->boundingVolume);
            // std::cout << node << " " << node->center << " " << node->boundingVolume << std::endl;
            node->updateState = UpToDate;
          }
        };

        template <typename RB, typename BV>
          class DynamicLoozeBoundingVolumeHierarchyBroadPhase
            : std::conditional<
                Metal::HasProxyTrait<BVHTreeNodeProxy<BV>, RB>::value
                , Metal::Empty                  // there is a proxy, nothing to add
                , BVHTreeNodeMap<RB, BV>>::type // there is no proxy, add a map to keep track of the bounding volumes
          {
            static_assert_HasBoundingVolume((RB), (BV));
            static_assert_BoundingVolume((BV));
            static_assert_LoozeBoundingVolume((BV));


            public:
            friend struct BroadPhaseTrait<
              DynamicLoozeBoundingVolumeHierarchyBroadPhase<RB, BV>, RB>;

            private:
              using Proxy   = BVHTreeNodeProxy<BV>;
              using Node    = BVHTreeNode<BV, RB>;
              using NodePtr = Node*; 

              std::vector<NodePtr>     _panding;
              double                   _margin;
              Allocator::Recycler<Node>_allocator;
              NodePtr                  _root = nullptr;

              // version with constant-time lookup
              template <typename _RB>
                typename std::enable_if<Metal::HasProxyTrait<Proxy, _RB>::value, NodePtr>::type
                _treeNodeOf(_RB& b)
                { return Metal::proxy<Proxy>(b).template treeNode<_RB>(); }

              // version with logarithmic-time lookup. Used when the proxy is not available
              template <typename _RB>
                typename std::enable_if<!Metal::HasProxyTrait<Proxy, _RB>::value, NodePtr>::type
                _treeNodeOf(_RB& b)
                { return BVHTreeNodeMap<_RB, BV>::_map[&b]; }

              // version with constant-time lookup
              template <typename _RB>
                typename std::enable_if<Metal::HasProxyTrait<Proxy, _RB>::value, void>::type
                _setTreeNodeOf(RB& b, NodePtr n)
                { Metal::proxy<Proxy>(b).template setTreeNode<RB>(n); }

              // version with logarithmic-time lookup. Used when the proxy is not available
              template <typename _RB>
                typename std::enable_if<!Metal::HasProxyTrait<Proxy, _RB>::value, void>::type
                _setTreeNodeOf(RB& b, NodePtr n)
                { BVHTreeNodeMap<RB, BV>::_map[&b] = n; }

            public:
              DynamicLoozeBoundingVolumeHierarchyBroadPhase(double margin)
                : _margin(margin)
              { }

              double margin() const
              { return _margin; }

              NodePtr treeNodeOf(RB& b)
              { return _treeNodeOf<RB>(b); }

              void setTreeNodeOf(RB& b, NodePtr n)
              { _setTreeNodeOf<RB>(b, n); }
          };

        template <typename RB, typename BV>
          struct BroadPhaseTrait<DynamicLoozeBoundingVolumeHierarchyBroadPhase<RB, BV>, RB>
          {
            static const bool value = true;

            // find objects to update
            using BF      = DynamicLoozeBoundingVolumeHierarchyBroadPhase<RB, BV>;
            using Node    = BVHTreeNode<BV, RB>;
            using NodePtr = Node*; 

            private:
            static void _updateShrink(NodePtr node)
            {
              if (node->updateState == Node::NeedsShrink)
              {
                Node::updateNode(node);

                if (node->parent) // there was a shrink, propagate to the parent
                  node->parent->updateState = Node::NeedsShrink;
              }
            }

            static void _checkWithChild(NodePtr                             toTest
                                        , NodePtr                           curr
                                        , std::vector<std::pair<RB*, RB*>>& pairs)
            {
              assert(toTest != curr);

              // we assume we already have an intersection with the curr
              if (curr->body) // repport the collision
                pairs.push_back(std::make_pair(toTest->body, curr->body));
              else
              {
                if (intersect(toTest->boundingVolume, curr->left->boundingVolume))
                  _checkWithChild(toTest, curr->left, pairs);

                if (intersect(toTest->boundingVolume, curr->right->boundingVolume))
                  _checkWithChild(toTest, curr->right, pairs);

                _updateShrink(curr);
              }
            }

            static NodePtr _insert(BF&                                 bf
                                   , NodePtr                           toInsert
                                   , NodePtr                           curr
                                   , NodePtr                           root
                                   , std::vector<std::pair<RB*, RB*>>& pairs)
            {
              assert(curr != toInsert);

              if (!root) // the tree is empty: nothing to do but creating a singleton
                return toInsert;

              if (curr->body) // this is a leaf
              {
                // create a new node (will be the common parent)
                auto newParent = bf._allocator.alloc();

                // play with pointers
                if (curr->parent)
                {
                  assert(curr->parent->right == curr || curr->parent->left == curr);

                  if (curr->parent->right == curr)
                    curr->parent->right = newParent;
                  else
                    curr->parent->left = newParent;
                }
                else
                  root = newParent;

                newParent->parent = curr->parent;
                newParent->right  = curr;
                newParent->left   = toInsert;
                curr->parent      = newParent;
                toInsert->parent  = newParent;

                // check if we have a potential collision pair
                if (intersect(toInsert->boundingVolume, curr->boundingVolume))
                  pairs.push_back(std::make_pair(toInsert->body, curr->body));

                Node::updateNode(newParent);

                newParent->invariant();
                curr->invariant();
                root->invariant();
                toInsert->invariant();
              }
              else // internal node
              {
                if (sqlen(curr->right->center - toInsert->center)
                    < sqlen(curr->left->center - toInsert->center))
                {
                  // closer to the right child
                  _insert(bf, toInsert, curr->right, root, pairs);

                  if (intersect(toInsert->boundingVolume, curr->left->boundingVolume))
                    _checkWithChild(toInsert, curr->left, pairs);
                }
                else
                {
                  // closer to the left child
                  _insert(bf, toInsert, curr->left, root, pairs);

                  if (intersect(toInsert->boundingVolume, curr->right->boundingVolume))
                    _checkWithChild(toInsert, curr->right, pairs);
                }

                // FIXME: we could avoid to update every nodes on the branch
                Node::updateNode(curr);
              }

              // std::cout << "checking" << std::endl;
              assert(!curr->right || contain(curr->boundingVolume, curr->right->boundingVolume));
              assert(!curr->left  || contain(curr->boundingVolume, curr->left->boundingVolume));

              return root;
            }

            public:
            static void addBody(BF& bf, RB* rb)
            {
              assert(bf.treeNodeOf(*rb) == nullptr && "Cannot insert the same object twice.");

              // create a new node
              auto newNode = bf._allocator.alloc();

              newNode->body = rb;
              bf._panding.push_back(newNode);
              bf.setTreeNodeOf(*rb, newNode);

              // update the bounding volume
              loozen(bf.margin()
                     , HasBoundingVolumeTrait<RB, BV>::boundingVolume(*rb)
                     , newNode->boundingVolume);

              Node::updateNode(newNode);
            }

            static void removeBody(BF& bf, RB* rb)
            {
              assert(false && "Not yet implemented.");
            }

            static std::vector<std::pair<RB*, RB*>> getCollisionsPairs(
                BF&                       bf
                , const std::vector<RB*>& bodies)
            {
              std::vector<std::pair<RB*, RB*>> res;
              std::vector<NodePtr>             updated;
              NodePtr                          currentRoot = bf._root;

              for (auto body : bodies)
              {
                const BV& bv   = HasBoundingVolumeTrait<RB, BV>::boundingVolume(*body);
                auto      node = bf.treeNodeOf(*body);
                BV&       lbv  = node->boundingVolume;

                if (!contain(lbv, bv)) // need update!
                {
                  loozen(bf.margin(), bv, lbv);
                  currentRoot = node->unlink(bf._allocator, currentRoot); // unlink the node to update
                  updated.push_back(node);
                }
              }

              for (auto panding : bf._panding)
              {
                // FIXME: not very efficient
                if (std::find(updated.begin(), updated.end(), panding) == updated.end())
                  updated.push_back(panding);
              }

              std::random_shuffle(updated.begin(), updated.end());

              // now perform the insersions
              for (auto node : updated)
              {
                currentRoot = _insert(bf, node, currentRoot, currentRoot, res);

                currentRoot->invariant();
              }

              // std::cout << "depth: " << currentRoot->depth() << std::endl;
              // std::cout << "num leaves: " << currentRoot->numLeaves() << std::endl;
              // std::cout << "num colls: " << res.size() - 2 * updated.size() << std::endl;

              bf._root = currentRoot;
              bf._panding.clear();

              currentRoot->deepInvariant();

              return res;
            }
          };
      }
    }
  } // end Collision
} // end Falling


#endif // FALLING_COLLISION_DETECTION_BROADPHASE_DYNAMICLOOZEBOUNDINGVOLUMEHIERARCHYBROADPHASE
