#ifndef FALLING_COLLISION_GRAPH_ALLNARROWPHASEACCUMULATOR
# define FALLING_COLLISION_GRAPH_ALLNARROWPHASEACCUMULATOR

// FIXME: wierd to include something from RigidBody here. Has Shape should be on the Shape folder?
# include <Falling/Body/HasShape.hh>
# include <Falling/Collision/Graph/Graph.hh>
# include <Falling/Collision/Detection/NarrowPhase/NarrowPhase.hh>

namespace Falling
{
  namespace Collision
  {
    template <typename RB, typename NF, typename CD, typename S>
      class AllNarrowPhaseAccumulator
      {
        // FIXME: is HasShapeTrait useful? Would be be better to use the dispatcher here to
        // encapsulate the shape extractor?
        static_assert_HasShape((RB));

        private:
        std::vector<std::tuple<RB*, RB*, CD*>> _contacts;
        std::vector<CD*>                       _contactsCollector;

        public:
        using AccumulationResultType = std::vector<std::tuple<RB*, RB*, CD*>>;

        void reinit()
        { _contacts.clear(); }

        AccumulationResultType result()
        { return _contacts; }

        bool accumulate(Node<RB, NF>& node)
        { return true; }

        bool accumulate(Edge<NF, RB>& edge)
        {
          using namespace Falling::Collision::Detection::NarrowPhase;

          auto nf = edge.value();
          auto n1 = edge.pred();
          auto n2 = edge.succ();
          RB*  r1 = n1->value();
          RB*  r2 = n2->value();

          using NPT = NarrowPhaseTrait<NF, CD, S>;

          NPT::update(nf, *shape(*n1->value()), *shape(*n2->value()));
          NPT::collisions(nf, _contactsCollector);

          for (auto contact : _contactsCollector)
            _contacts.push_back(std::make_tuple(r1, r2, contact));

          _contactsCollector.clear();

          // we accept this edge, no matter the number of contacts (we dont create islands)
          return true;
        }
      };
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_GRAPH_ALLNARROWPHASEACCUMULATOR
