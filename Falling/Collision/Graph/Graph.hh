#ifndef FALLING_COLLISION_GRAPH_GRAPH
# define FALLING_COLLISION_GRAPH_GRAPH

# include <iostream>
# include <cassert>
# include <vector>
# include <unordered_map>

namespace Falling
{
  namespace Collision
  {
    template <typename N, typename E>
      class Node;
    template <typename E, typename N>
      class Edge;
    template <typename N, typename E>
      class Graph;

    template <typename N, typename E>
      class Node
      {
        friend Edge<E, N>;
        friend Graph<N, E>;

        private:
          unsigned int                                 _timestamp;
          std::unordered_map<Node<N, E>*, Edge<E, N>*> _existingPairs;
          std::vector<Edge<E, N>*>                     _edges;
          N*                                           _value;
          bool                                         _updated;

        public:
          Node(N* value)
            : _timestamp(0), _value(value), _updated(false)
          { }

          const std::vector<Edge<E,N>*>& edges() const
          { return _edges; }

          const std::unordered_map<Node<N, E>*, Edge<E, N>*>& existingPairs() const
          { return _existingPairs; }

          bool updated() const
          { return _updated; }

          void setUpdated(bool up)
          { _updated = up; }

          const N* value() const
          { return _value; }

          N* value()
          { return _value; }

          unsigned int timestamp() const
          { return _timestamp; }

          void setTimestamp(unsigned int t)
          { _timestamp = t; }

          unsigned int addEdge(Edge<E, N>* edge)
          {
            _existingPairs[edge->other(this)] = edge;
            _edges.push_back(edge);

            return _edges.size() - 1;
          }

          template <typename V>
            void accumulate(V& accumulator)
            {
              if (accumulator.accumulate(*this)) // the accumulator can short-cut the accumulation
              {
                for (auto edge : _edges)
                {
                  if (edge->timestamp() != _timestamp)
                  {
                    edge->setTimestamp(_timestamp);
                    edge->accumulate(accumulator);
                  }
                }
              }
            }
      };

    template <typename E, typename N>
      class Edge
      {
        friend Graph<N, E>;

        private:
          E*           _value;
          unsigned int _timestamp;
          Node<N, E>*  _pred;
          Node<N, E>*  _succ;
          unsigned int _predId;
          unsigned int _succId;
          unsigned int _cleanupTimestamp;

        public:
          Edge(E* value)
            : _value(value), _timestamp(0), _cleanupTimestamp(0)
          { }

          E* value()
          { return _value; }

          const E* value() const
          { return _value; }

          void init(Node<N, E>*    pred
                    , Node<N, E>*  succ
                    , unsigned int predId
                    , unsigned int succId)
          {
            _pred   = pred;
            _succ   = succ;
            _predId = predId;
            _succId = succId;
          }

          unsigned int timestamp() const
          { return _timestamp; }

          void setTimestamp(unsigned int t)
          { _timestamp = t; }

          unsigned int cleanupTimestamp() const
          { return _cleanupTimestamp; }

          void setCleanupTimestamp(unsigned int t)
          { _cleanupTimestamp = t; }

          Node<N, E>* other(const Node<N, E>* o) const
          { return o == _pred ? _succ : _pred; }

          const Node<N, E>* pred() const
          { return _pred; }

          Node<N, E>* pred()
          { return _pred; }

          const Node<N, E>* succ() const
          { return _succ; }

          Node<N, E>* succ()
          { return _succ; }

          template <typename V>
            void accumulate(V& accumulator)
            {
              if (accumulator.accumulate(*this)) // the accumulator can short-cut the accumulation
              {
                if (_pred->_timestamp != _timestamp)
                {
                  _pred->_timestamp = _timestamp;
                  _pred->accumulate(accumulator);
                }
                if (_succ->_timestamp != _timestamp)
                {
                  _succ->_timestamp = _timestamp;
                  _succ->accumulate(accumulator);
                }
              }
            }

          void verifyLinks() // for debug only
          {
            assert(_pred->edges()[_predId] == this);
            assert(_succ->edges()[_succId] == this);
          }

          void unlink()
          {
            // pred
            Edge<E, N>* lastPred = _pred->_edges.back();

            _pred->_edges.pop_back();

            if (lastPred != this)
            {
              _pred->_edges[_predId] = lastPred;

              if (lastPred->_pred == _pred)
                lastPred->_predId = _predId;
              else
              {
                assert(lastPred->_succ == _pred);
                lastPred->_succId = _predId;
              }

              lastPred->verifyLinks();
            }

            // succ
            Edge<E, N>* lastSucc = _succ->_edges.back();
            _succ->_edges.pop_back();

            if (lastSucc != this)
            {
              _succ->_edges[_succId] = lastSucc;

              if (lastSucc->_pred == _succ)
                lastSucc->_predId = _succId;
              else
              {
                assert(lastSucc->_succ == _succ);
                lastSucc->_succId = _succId;
              }

              lastSucc->verifyLinks();
            }

            _succ->_existingPairs.erase(_pred);
            _pred->_existingPairs.erase(_succ);

            // for safety
            _pred = nullptr;
            _succ = nullptr;
          }

          ~Edge()
          { delete _value; }
      };

    template <typename N, typename E>
      class Graph
      {
        private:
          unsigned int                        _timestamp;
          unsigned int                        _cleanupTimestamp;
          std::unordered_map<N*, Node<N, E>*> _nodes;

        public:
          Graph()
            : _timestamp(0)
          { }

          void addNode(N* nodeValue)
          {
            assert(_nodes.find(nodeValue) == _nodes.end()); // the node must not exist already

            auto node = new Node<N, E>(nodeValue);
            node->_timestamp = _timestamp - 1;

            _nodes[nodeValue] = node;
          }

          void removeNode(N* nodeValue)
          {
            auto node = _nodes[nodeValue];

            for (auto edge : node->_edges())
            {
              edge->unlink();
              delete edge;
            }

            _nodes.erase[nodeValue];
            delete node;
          }

          bool prepareInsersion(N* n1, N* n2)
          {
            auto node1 = _nodes[n1];
            auto node2 = _nodes[n2];

            auto edge = node1->existingPairs().find(node2);

            node1->setUpdated(true);
            
            if (edge != node1->existingPairs().end())
            {
              std::get<1>(*edge)->setCleanupTimestamp(_cleanupTimestamp);
              return true;
            }

            return false;
          }

          template <typename A>
            std::vector<typename A::AccumulationResultType>
              accumulate(const std::vector<N*>& nodes)
            {
              A accumulator;
              std::vector<typename A::AccumulationResultType> results;

              ++_timestamp;

              for (auto node : nodes)
              {
                accumulator.reinit();

                auto graphNode = _nodes[node];

                if (graphNode->_timestamp != _timestamp)
                {
                  graphNode->_timestamp = _timestamp;
                  graphNode->accumulate(accumulator);
                  results.push_back(accumulator.result());
                }
              }

              return results;
            }

          void addEdge(E* edgeValue, N* n1, N* n2)
          {
            if (edgeValue == nullptr)
              return;

            auto node1    = _nodes[n1];
            auto node2    = _nodes[n2];
            auto existing = node1->existingPairs().find(node2);

            node1->setUpdated(true);

            if (existing == node1->existingPairs().end())
            {
              // the edge does not exist yet
              auto newEdge = new Edge<E, N>(edgeValue);

              newEdge->init(node1, node2, 0, 0);

              auto id1 = node1->addEdge(newEdge);
              auto id2 = node2->addEdge(newEdge);

              newEdge->init(node1, node2, id1, id2);
              newEdge->_timestamp = _timestamp - 1;

              newEdge->setCleanupTimestamp(_cleanupTimestamp);
            }
          }

          void cleanup()
          {
            std::vector<Edge<E, N>*> toRemove;

            for (auto nodeWithKey : _nodes)
            {
              auto node = std::get<1>(nodeWithKey);

              if (node->updated())
              {
                node->setUpdated(false);

                for (auto edge : node->_edges)
                {
                  if (edge->cleanupTimestamp() < _cleanupTimestamp)
                    toRemove.push_back(edge);
                }

                for (auto edge : toRemove)
                {
                  edge->unlink();
                  delete edge;
                }

                toRemove.clear();
              }
            }

            ++_cleanupTimestamp;
          }
      };
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_GRAPH_GRAPH
