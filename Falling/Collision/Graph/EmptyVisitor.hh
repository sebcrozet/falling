#ifndef FALLING_COLLISION_GRAPH_EMPTYVISITOR
# define FALLING_COLLISION_GRAPH_EMPTYVISITOR

# include <Falling/Collision/Graph/Graph.hh>

namespace Falling
{
  namespace Collision
  {
    template <typename RB, typename NF>
      class EmptyVisitor
      {
        public:
          bool visit(Node<RB, NF>& node)
          {
            return true;
          }

          bool visit(Edge<NF, RB>& edge)
          {
            return true;
          }
      };
  } // end Collision
} // end Falling

#endif // FALLING_COLLISION_GRAPH_EMPTYVISITOR
