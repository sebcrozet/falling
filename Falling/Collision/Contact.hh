#ifndef FALLING_COLLISION_CONTACT
# define FALLING_COLLISION_CONTACT

# include <Falling/Algebra/AbelianGroup.hh>
# include <Falling/Algebra/Transform.hh>

namespace Falling
{
  namespace Collision
  {
    using namespace Falling::Algebra;

    template <typename V>
      struct Contact
      {
        V      globalContact1;
        V      globalContact2;
        V      contactNormal;
        double penetrationDepth;
        double impulse; // FIXME: its a bit uggly to have the impulse on the collision (geometry)

        Contact()
          : penetrationDepth(0.0), impulse(0.0)
        { }

        void clearCache()
        { impulse = 0.0; }

        void revert()
        {
          static_assert_AbelianGroup((V));

          std::swap(globalContact1, globalContact2);
          negate(contactNormal);
        }
      };

    template <typename T>
      std::ostream& operator<<(std::ostream& stream, const Contact<T>& contact)
      {
        stream << "Contact { "
               << " globalContact1 = "    << contact.globalContact1
               << ", globalContact2 = "   << contact.globalContact2
               << ", contactNormal = "    << contact.contactNormal
               << ", penetrationDepth = " << contact.penetrationDepth
               << ", impulse = "          << contact.impulse
               << " }"
               << std::endl;

        return stream;
      }

    template <typename V>
      struct UpdatableContact
      {
        Contact<V>* collision;
        V             localContact1;
        V             localContact2;
      };

    template <typename T>
    void collisionToUpdatableContact(
        const T&                                               it1
        , const T&                                             it2
        , const Contact<typename TransformSystemTrait<T>
                        ::AsTranslatable::TranslationType>&    coll
        , UpdatableContact<typename TransformSystemTrait<T>
                           ::AsTranslatable::TranslationType>& ucoll)
    {
      static_assert_TransformSystem((T));

      ucoll.collision     = coll;
      ucoll.localContact1 = transform(it1, coll.globalContact1);
      ucoll.localContact2 = transform(it2, coll.globalContact2);
    }
  } // end Collision
} // end Falling
#endif // FALLING_COLLISION_CONTACT
