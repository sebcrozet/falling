#ifndef FALLING_WORLD_GENERICWORLD
# define FALLING_WORLD_GENERICWORLD

# include <Falling/Integrator/Integrator.hh>
# include <Falling/Collision/Detection/BroadPhase/BroadPhase.hh>
# include <Falling/Collision/Graph/Graph.hh>

namespace Falling
{
  namespace World
  {
    using namespace Falling::Integrator;
    using namespace Falling::Collision;
    using namespace Falling::Collision::Detection::BroadPhase;

    template <typename RB   // RigidBody
              , typename I  // Integrator
              , typename NF // Narrow Phase
              , typename BP // Broad Phase
              , typename CD // Collision Dispatcher
              , typename CA // Constaint Accumulator
              , typename CS> // Constraint Solver
    class GenericWorld
    {
      static_assert_Integrator((I), (RB));
      static_assert_BroadPhase((BP), (RB));

      using IT  = IntegratorTrait<I, RB>;
      using BPT = BroadPhaseTrait<BP, RB>;

      private:
        std::vector<RB*> _bodies;
        I*               _integrator;
        Graph<RB, NF>    _collisionGraph;
        BP*              _broadPhase;
        CD               _collisionDispatcher;
        CS               _constraintsSolver;

      public:
        GenericWorld(I* integrator, BP* broadPhase)
          : _integrator(integrator), _broadPhase(broadPhase)
        { }

        ~GenericWorld()
        {
          delete _broadPhase;
          delete _integrator;
          // FIXME: also remove _bodies?
        }

        // FIXME: it should be great to make this const. This requires a const visitor function of
        // the graph.
        Graph<RB, NF>* collisionGraph()
        { return &_collisionGraph; }

        CD* collisionDispatcher()
        { return &_collisionDispatcher; }

        const std::vector<RB*> bodies() const
        { return _bodies; }

        void addBody(RB* body)
        {
          assert(body != nullptr);

          _bodies.push_back(body);
          BPT::addBody(*_broadPhase, body);
          _collisionGraph.addNode(body);
        }

        void removeBody(RB* body)
        {
          assert(std::find(_bodies.begin(), _bodies.end(), body) != _bodies.end());

          _bodies.erase(std::find(_bodies.begin(), _bodies.end(), body));
          removeBody(*_broadPhase, body);
          _collisionGraph.removeNode(body);
        }

        void step(double dt)
        {
          for (auto body : _bodies)
            IT::integrate(*_integrator, dt, *body);

          auto pairs = getCollisionsPairs(*_broadPhase, _bodies);

          for (auto pair : pairs)
          {
            auto b1 = std::get<0>(pair);
            auto b2 = std::get<1>(pair);

            // FIXME: this test will be done twice (once here, and once on the collision graph)…
            if (!_collisionGraph.prepareInsersion(b1, b2))
              _collisionGraph.addEdge(_collisionDispatcher(b1, b2), b1, b2);
          }

          _collisionGraph.cleanup(); // remove unused collisions

          auto islands = _collisionGraph.template accumulate<CA>(_bodies);

          for (auto island : islands)
            _constraintsSolver(dt, island, _bodies); // FIXME: do not give every bodies…
        }
    };
  } // end World
} // end Falling

#endif // FALLING_WORLD_GENERICWORLD
