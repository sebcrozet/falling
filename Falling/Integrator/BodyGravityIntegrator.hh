#ifndef FALLING_BODY_BODYGRAVITYINTEGRATOR
# define FALLING_BODY_BODYGRAVITYINTEGRATOR

# include <Falling/Integrator/GenericEulerIntegrator.hh>
# include <Falling/Integrator/Integrator.hh>

namespace Falling
{
  namespace Integrator
  {
    template <typename RB, typename LV, typename AV>
      class BodyGravityIntegrator
      {
        private:
        LV _linearGravity;
        AV _angularGravity;

        public:
        LV& linearGravity()
        { return _linearGravity; }

        AV& angularGravity()
        { return _angularGravity; }

        const LV& linearGravity() const
        { return _linearGravity; }

        const AV& angularGravity() const
        { return _angularGravity; }

        BodyGravityIntegrator(const LV& linearGravity, const AV angularGravity)
          : _linearGravity(linearGravity), _angularGravity(angularGravity)
        { }
      };

    template <typename RB, typename LV, typename AV>
      struct IntegratorTrait<BodyGravityIntegrator<RB, LV, AV>, RB>
      {
        private:
          using IT = BodyGravityIntegrator<RB, LV, AV>;

        public:
          static const bool value = true;

          static void integrate(IT& integrator, double dt, RB& body)
          {
            setExternalLinearForce(body, integrator.linearGravity());
            setExternalAngularForce(body, integrator.angularGravity());
            integrateBodyVelocity(dt, body);
            integrateBodyPosition(dt, body);
          }
      };
  } // end Body
} // end Falling

#endif // FALLING_BODY_BODYGRAVITYINTEGRATOR
