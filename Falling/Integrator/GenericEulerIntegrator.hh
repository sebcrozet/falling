#ifndef FALLING_INTEGRATOR_GENERICEULERINTEGRATOR
# define FALLING_INTEGRATOR_GENERICEULERINTEGRATOR

# include <Falling/Metal/EnableIf.hh>
# include <Falling/Algebra/TransformSystem.hh>
# include <Falling/Body/Transformable.hh>
# include <Falling/Body/Dynamic.hh>

namespace Falling
{
  namespace Integrator
  {
    using namespace Falling::Algebra;
    using namespace Falling::Body;

    template <typename V, CHECK_CONCEPT((ScalarOpTrait<V>))>
      void dampVector(V& v)
      { v *= 0.9998; }

    template <typename V
              , CHECK_CONCEPT((ScalarOpTrait<V>))
              , CHECK_CONCEPT((AbelianGroupTrait<V>))>
      void integrate(double dt, const V& f, V& v)
      {
        v += dt * f;
      }

    template <typename T, BIND_CONCEPT(TST, (TransformSystemTrait<T>))>
    void displacement(double                                                 dt
                      , const T&                                             ref
                      , const typename TST::AsTranslatable::TranslationType& linVel
                      , const typename TST::AsRotatable::RotationType&       angVel
                      , T&                                                   outDeltaTranform)
    {
      outDeltaTranform = TST::AsMultInversibleSemiGroup::one;
      translate(dt * linVel
                , rotateWrtPoint(dt * angVel, translation(ref), outDeltaTranform));
    }

    template <typename LV, typename AV
              , CHECK_CONCEPT((ScalarOpTrait<LV>))
              , CHECK_CONCEPT((AbelianGroupTrait<LV>))
              , CHECK_CONCEPT((ScalarOpTrait<AV>))
              , CHECK_CONCEPT((AbelianGroupTrait<AV>))>
    void integrateVelocities(double dt
                             , const LV& fextLin
                             , const AV& fextAng
                             , LV& linVel
                             , AV& angVel)
    {
      integrate(dt, fextAng, angVel);
      integrate(dt, fextLin, linVel);
      dampVector(linVel);
      dampVector(angVel);
    }

    template <class RB
              , CHECK_CONCEPT((DynamicTrait<RB>))
              , CHECK_CONCEPT((TransformableTrait<RB>))>
    void integrateBodyPosition(double dt, RB& body)
    {
      // FIXME: could a T&& be useful here to make displacement non-void?
      typename RB::TransformType displacementMatrix;
      displacement(dt
                   , localToWorld(body)
                   , linearVelocity(body)
                   , angularVelocity(body)
                   , displacementMatrix);
      append(body, displacementMatrix);
    }


    template <class RB
              , CHECK_CONCEPT((DynamicTrait<RB>))>
    void integrateBodyVelocity(double dt, RB& body)
    {
      typename RB::LinearVelocityType  lvel = linearVelocity(body);
      typename RB::AngularVelocityType avel = angularVelocity(body);
      integrateVelocities(dt
                          , externalLinearForce(body)
                          , externalAngularForce(body)
                          , lvel
                          , avel);
      setLinearVelocity(body, lvel);
      setAngularVelocity(body, avel);
    }
  } // end Integrator
} // end Falling

#endif // FALLING_INTEGRATOR_GENERICEULERINTEGRATOR
