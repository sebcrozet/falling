#ifndef FALLING_INTEGRATOR_INTEGRABLE
# define FALLING_INTEGRATOR_INTEGRABLE

# include <Falling/Metal/EnableIf.hh>

namespace Falling
{
  namespace Integrator
  {
# define static_assert_Integrator(TYPEI, TYPERB)                                           \
         static_assert(Falling::Integrator::IntegratorTrait<                               \
             typename RMPAR(TYPEI)                                                         \
             , typename RMPAR(TYPERB)>::value                                              \
             , "Type parameter " #TYPEI " must be an integrator acting of bodies of type " \
               #TYPERB".")
    template <class I, class RB, typename Enabler = void>
      struct IntegratorTrait
      {
        static const bool value = false;

        static void integrate(I&, double, RB&);
      };

    template <typename RB, typename I, BIND_CONCEPT(IT, (IntegratorTrait<I, RB>))>
      static void integrate(I& integrator, double, RB& body)
      { IT::integrate(integrator, body); }
  }
}

#endif // FALLING_INTEGRATOR_INTEGRABLE
