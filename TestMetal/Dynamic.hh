#include <Falling/Metal/Inherit.hh>
#include <Falling/Metal/FMap.hh>
#include <Falling/Metal/Permutations.hh>
#include <Falling/Metal/Sort.hh>

using namespace Falling::Metal;

template <typename I>
  struct ToDynamicBridge
{ using type = typename I::DynamicBridge; };

template <typename...>
  struct Dictionary;

template <typename... DynamicTraits>
  struct Dictionary : public Inherit<DynamicTraits...>
                      , public FMap<ToDynamicBridge, VirtualInheritLattice<DynamicTraits...>>::type::type
  { };
